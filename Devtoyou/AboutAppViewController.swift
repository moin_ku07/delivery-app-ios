//
//  AboutAppViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 11/14/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import MessageUI

class AboutAppViewController: UIViewController, MFMailComposeViewControllerDelegate, UIWebViewDelegate, UIScrollViewDelegate {

    @IBOutlet var webview: UIWebView!
    
    var activityView: UICustomActivityView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        self.webview.delegate = self
        //self.webview.scrollView.delegate = self
        
        webview.backgroundColor = UIColor(rgba: "EFECD9")
        webview.scrollView.backgroundColor = UIColor(rgba: "EFECD9")
        
        loadLink()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadLink(){
        let link: NSURL = NSURL(string: DataManager.domain().root + "apphtml/contactus.html")!
        //let link: NSURL = NSURL(string: "https://google.com")!
        self.webview.loadRequest(NSURLRequest(URL: link))
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityView.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(webview: UIWebView) {
        activityView.hideActivityIndicator()
        self.webview.scrollView.contentOffset = CGPointMake(0,0)
    }

    @IBAction func onSendMessageTap(sender: AnyObject) {
        let mailComposer: MFMailComposeViewController = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        
        mailComposer.setToRecipients(["info@devtoyou.com"])
        mailComposer.setSubject("Contact Us")
        
        /*let infoDictionary: NSDictionary = NSBundle.mainBundle().infoDictionary!
        let applicationVersion: String = (infoDictionary.objectForKey("CFBundleShortVersionString") as! String) + ", build: " + (infoDictionary.objectForKey("CFBundleVersion") as! String)
        
        let device: UIDevice = UIDevice.currentDevice()
        let deviceName: String = device.name
        let deviceOS: String = device.systemName + " " + device.systemVersion
        
        let bodyHtml: String = "<html><style type=\"text/css\">hr{background-color:black;color:black;height: 1px;margin: 0;}</style><body>Hi,<br><br>Here's my feedback:<br><br><br><br><br><br><hr><p style=\"text-align:center;margin: 0;\">Type feedback above this line</p><br><p style=\"text-align:left;margin: 0;font-size: 12px\">Discussion app Version: \(applicationVersion)</p><p style=\"text-align:left;margin: 0;font-size: 12px\">Device: \(deviceName)</p><p style=\"text-align:left;margin: 0;font-size: 12px\">Device OS Version: \(deviceOS)</p></body></html>"
        mailComposer.setMessageBody(bodyHtml, isHTML: true)*/
        
        self.presentViewController(mailComposer, animated: true, completion: nil)
    }
    
    // MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true){
            dispatch_async(dispatch_get_main_queue()){
                switch result{
                case MFMailComposeResultSent:
                    AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                default:
                    print("result = \(result)")
                    //AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "Try again"), buttonNames: nil, completion: nil)
                }
            }
        }
    }
}
