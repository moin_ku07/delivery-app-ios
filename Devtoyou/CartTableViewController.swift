//
//  CartTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/3/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

class CartTableViewController: UITableViewController, CartTableViewCellDelegate, CartTextTableViewCellDelegate {
    
    @IBOutlet var footerView: UIView!
    @IBOutlet var deliveryCostView: UIView!
    @IBOutlet var deliveryCostLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var totalView: UIView!
    @IBOutlet var placeOrderButton: UIButton!
    
    var tableData: [Carts] = [Carts]()
    
    var noDataMessageLabel: UILabel?
    var selectedQuantityField: UITextField?
    var activityView: UICustomActivityView!
    let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("Devtoyou")
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var user_id: String?
    var user_type: String?
    var hasLocations: Bool = false
    var addresses: NSMutableArray = NSMutableArray()
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()
    
    var hasCustomOrder: Bool = false
    var customOrderText: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        //numberFormatter.minimumFractionDigits = 2
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let id: NSNumber = prefs.objectForKey("id") as? NSNumber{
            user_id = id.stringValue
        }
        if let user_type: String = prefs.objectForKey("user_type") as? String{
            self.user_type = user_type
        }
        
        var url: String = "api/"
        if user_type == "customer"{
            url += "customers/" + user_id! + "/"
        }
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    if let locations: NSArray = json.objectForKey("locations") as? NSArray{
                        if locations.count > 0{
                            self.addresses = NSMutableArray(array: locations)
                            self.hasLocations = true
                        }else{
                            self.addresses.removeAllObjects()
                            self.hasLocations = false
                        }
                    }else{
                        self.addresses.removeAllObjects()
                        self.hasLocations = false
                    }
                }catch{
                    self.addresses.removeAllObjects()
                    self.hasLocations = false
                    print("Address json decode error")
                }
            }
        }
        
        //print("will appear")
        
        loadTableData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        /*if tableData.count > 0{
            self.noDataMessageLabel?.hidden = true
            footerView.hidden = false
        }else{
            if self.noDataMessageLabel == nil{
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                    self.tableView.bounds.size.height))
                
                self.noDataMessageLabel!.text = NSLocalizedString("Label_No_Item_Checkout", comment: "No Item")
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.tableView.backgroundView = self.noDataMessageLabel!
                //print("label added")
            }else{
                footerView.hidden = true
                self.noDataMessageLabel?.hidden = false
            }
        }*/

        if tableData.count > 0 && !hasCustomOrder{
            totalView.hidden = false
            deliveryCostView.hidden = false
            placeOrderButton.setTitle(NSLocalizedString("Label_Place_Order", comment: "Place Order"), forState: UIControlState.Normal)
        }else{
            totalView.hidden = true
            deliveryCostView.hidden = true
            placeOrderButton.setTitle(NSLocalizedString("Label_Place_Custom_Order", comment: "Place Custom Order"), forState: UIControlState.Normal)
        }
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasCustomOrder{
            return 1
        }
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if hasCustomOrder{
            let cell: CartTextTableViewCell = tableView.dequeueReusableCellWithIdentifier("textCell", forIndexPath: indexPath) as! CartTextTableViewCell
            cell.delegate = self
            if let textView: UITextView = cell.contentView.subviews.first?.viewWithTag(1) as? UITextView{
                textView.text = customOrderText
            }
            cell.selectionStyle = .None
            return cell
        }
        
        let cell: CartTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CartTableViewCell
        cell.selectionStyle = .None
        cell.delegate = self

        let cellData: Carts = tableData[indexPath.row]
        
        cell.title.text = cellData.name
        cell.storeName.text = cellData.store_name
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            cell.storeName.textAlignment = .Left
        }
        cell.details.text = cellData.details
        cell.details.textColor = UIColor(rgba: "6f6f6e")
        cell.details.contentOffset = CGPointZero
        cell.quantity.text = numberFormatter.stringFromNumber(cellData.quantity!)
        cell.prevQuantity = cellData.quantity
        if let price: NSDecimalNumber = cellData.price{
            cell.price.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
            if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                cell.price.text = numberFormatter.stringFromNumber(price)! + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
            }
        }
        
        if let urlString: String = cellData.photo{
            cell.thumbUrl = NSURL(string: urlString)!
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath){
            if cell.reuseIdentifier == "textCell"{
                placeOrderButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
            }
        }
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        selectedQuantityField?.resignFirstResponder()
        return indexPath
    }
    
    // MARK: - CartTableViewCellDelegate
    func onCellDeleteTap(cell: CartTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: Carts = tableData[indexPath.row]
            moc.deleteObject(cellData)
            do{
                try moc.save()
                tableData.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Middle)
                if tableData.count > 0{
                    self.tabBarItem.badgeValue = "\(tableData.count)"
                }else{
                    self.tabBarItem.badgeValue = nil
                    self.loadTableData()
                }
                calculateTotal()
            }catch{
                print("item was not deleted. reason: \(error)")
            }
        }
    }
    
    // MARK: - CartTextTableViewCellDelegate
    func onTextCellDeleteTap(cell: CartTextTableViewCell){
        print("onTextCellDeleteTap")
        self.prefs.setObject(nil, forKey: "customOrder")
        self.prefs.removeObjectForKey("customOrder")
        self.prefs.synchronize()
        hasCustomOrder = false
        self.loadTableData()
    }
    
    func onQuantityFocus(textField: UITextField) {
        selectedQuantityField = textField
    }
    
    func onQuantityUpdate(quantity: NSNumber, cell: CartTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: Carts = tableData[indexPath.row]
            cellData.quantity = quantity
            do{
                try moc.save()
                calculateTotal()
            }catch{
                print("item was not deleted. reason: \(error)")
            }
        }
    }

    // MARK: - loadTableData
    func loadTableData(){
        let cartArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc) as! [Carts]
        //print(cartArray)
        if cartArray.count > 0{
            hasCustomOrder = false
            tableData.removeAll()
            for cart in cartArray{
                tableData.append(cart)
            }
            self.tabBarItem.badgeValue = "\(cartArray.count)"
            calculateTotal()
        }else{
            self.tabBarItem.badgeValue = nil
            tableData.removeAll()
            
            if let customOrder: NSDictionary = prefs.objectForKey("customOrder") as? NSDictionary{
                if let additional_notes: String = customOrder.objectForKey("additional_notes") as? String{
                    hasCustomOrder = true
                    customOrderText = additional_notes
                }
            }else{
                hasCustomOrder = false
            }
        }
        tableView.reloadData()
    }
    
    // MARK: - calculateTotal
    func calculateTotal(){
        var deliveryCost: Float = 0
        
        var storeIDs: [Int] = [Int]()
        var total: Float = 0
        for cart in tableData{
            if !storeIDs.contains(cart.store_id!.integerValue){
                if let dcharge: NSNumber = cart.delivery_charge{
                    deliveryCost += dcharge.floatValue
                    storeIDs.append(cart.store_id!.integerValue)
                }
            }
            total += cart.quantity!.floatValue * cart.price!.floatValue
        }
        total += deliveryCost
        
        deliveryCostLabel.text = NSLocalizedString("Label_Currency_Code", comment: "Currency") + " \(deliveryCost)"
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            deliveryCostLabel.text = numberFormatter.stringFromNumber(NSNumber(float: deliveryCost))! + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
        }
        
        totalLabel.text = NSLocalizedString("Label_Currency_Code", comment: "Currency") + " \(total)"
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            totalLabel.text = numberFormatter.stringFromNumber(NSNumber(float: total))! + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
        }
    }

    @IBAction func onPlaceOrderTap(sender: UIButton) {
        if hasLocations || prefs.objectForKey("user_type") as? String == "store_owner"{
                let vc: PlaceOrderViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PlaceOrderViewController") as! PlaceOrderViewController
            vc.user_id = self.user_id
            vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            vc.addresses  = self.addresses
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_Location", comment: "Add location"), buttonNames: nil, completion: { (index) -> Void in
                let vc: AddAddressMapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAddressMapViewController") as! AddAddressMapViewController
                vc.toViewController = self.tabBarController
                vc.user_id = self.user_id
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
    }
    
}
