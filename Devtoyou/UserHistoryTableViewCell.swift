//
//  UserHistoryTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/8/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RateView

protocol UserHistoryTableViewCellDelegate{
    func onLeaveFeedbackTap(cell: UserHistoryTableViewCell)
    func onStatusButtonTap(cell: UserHistoryTableViewCell)
    func onUploadImageTap(cell: UserHistoryTableViewCell)
}

class UserHistoryTableViewCell: UITableViewCell {
    
    var delegate: UserHistoryTableViewCellDelegate?

    
    @IBOutlet var statusButton: PaddedButton!
    @IBOutlet var rateView: RateView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var feedbackText: UITextView!
    @IBOutlet var feedbackButton: PaddedButton!
    @IBOutlet var uploadButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rateView.starSize = 15
        rateView.starFillColor = UIColor(rgba: "de3929")
        rateView.starBorderColor = UIColor(rgba: "de3929")
        rateView.starNormalColor = UIColor(rgba: "efecd9")
        rateView.canRate = false
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onLeaveFeedbackTap(sender: PaddedButton) {
        delegate?.onLeaveFeedbackTap(self)
    }

    @IBAction func onStatusButtonTap(sender: PaddedButton) {
        delegate?.onStatusButtonTap(self)
    }
    
    @IBAction func onUploadImageTap(sender: UIButton) {
        delegate?.onUploadImageTap(self)
    }
    
    
}
