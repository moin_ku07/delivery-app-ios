//
//  CartTextTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 5/29/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

protocol CartTextTableViewCellDelegate{
    func onTextCellDeleteTap(cell: CartTextTableViewCell)
}

class CartTextTableViewCell: UITableViewCell {
    
    var delegate: CartTextTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func onDeleteTap(sender: UIButton) {
        print("onDeleteTap \(#line)")
        delegate?.onTextCellDeleteTap(self)
    }

}
