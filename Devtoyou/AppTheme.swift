//
//  AppTheme.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/28/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class AppTheme: NSObject {
    static func apply() {
        applyToUIButton()
        // ...
    }
    
    // It can either theme a specific UIButton instance, or defaults to the appearance proxy (prototype object) by default
    static func applyToUIButton(a: UIButton = UIButton.appearance()) {
        a.titleLabelFont = UIFont.systemFontOfSize(12)
        // other UIButton customizations
    }
}


extension UIButton {
    var titleLabelFont: UIFont! {
        get { return self.titleLabel?.font }
        set { self.titleLabel?.font = newValue }
    }
}