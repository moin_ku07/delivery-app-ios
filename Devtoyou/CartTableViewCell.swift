//
//  CartTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/3/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import SDWebImage

protocol CartTableViewCellDelegate{
    func onCellDeleteTap(cell: CartTableViewCell)
    func onQuantityFocus(textField: UITextField)
    func onQuantityUpdate(quanity: NSNumber, cell: CartTableViewCell)
}

class CartTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    var delegate: CartTableViewCellDelegate?
    var previousScrollOffset: CGPoint?
    var prevQuantity: NSNumber!

    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var storeName: UILabel!
    @IBOutlet var details: ZeroPaddingTextView!
    @IBOutlet var quantity: UITextField!
    @IBOutlet var price: UILabel!
    @IBOutlet var thumbActivity: UIActivityIndicatorView!
    @IBOutlet var upButton: UIButton!
    @IBOutlet var downButton: UIButton!
    
    var fontSize: CGFloat = 5
    
    var thumbUrl: NSURL?{
        didSet{
            /*let delay = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(delay){
                do{
                    let imageData: NSData = try NSData(contentsOfURL: self.thumbUrl!, options: NSDataReadingOptions())
                    dispatch_async(dispatch_get_main_queue()){
                        self.thumbActivity.stopAnimating()
                        self.thumbnail.image = UIImage(data: imageData)
                    }
                }catch{
                    print("Thumb fetch error")
                }
            }*/
            thumbnail.sd_setImageWithURL(thumbUrl) { (image:UIImage!, error: NSError!, cache: SDImageCacheType, url: NSURL!) -> Void in
                self.thumbActivity.stopAnimating()
            }
        }
    }
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        title.text = ""
        storeName.text = ""
        details.text = ""
        price.text = ""
        
        //quantity.delegate = self
        quantity.enabled = false
        quantity.layer.borderColor = UIColor(rgba: "cecece").CGColor
        quantity.layer.borderWidth = 1
        
        upButton.setTitle("\u{002B}", forState: UIControlState.Normal)
        upButton.titleLabel?.font = UIFont.boldSystemFontOfSize(18)
        upButton.titleLabel?.textAlignment = .Center
        downButton.setTitle("\u{2013}", forState: UIControlState.Normal)
        downButton.titleLabel?.font = UIFont.boldSystemFontOfSize(18)
        downButton.titleLabel?.textAlignment = .Center
        
        upButton.layer.borderColor = UIColor.whiteColor().CGColor
        downButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onDeleteButtonTap(sender: UIButton) {
        delegate?.onCellDeleteTap(self)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if let scrollView: UITableView = self.superview?.superview as? UITableView{
            previousScrollOffset = scrollView.contentOffset
            let textFieldPos: CGPoint = textField.convertPoint(textField.frame.origin, fromView: scrollView)
            //println(abs(textFieldPos.y))
            if scrollView.superview!.bounds.height - abs(textFieldPos.y) < 290{
                let yPost: CGFloat = abs(textFieldPos.y) - (scrollView.superview!.bounds.height - 290 - textField.frame.origin.y)
                let scrollPoint: CGPoint = CGPointMake(0, yPost)
                //println(scrollPoint)
                scrollView.setContentOffset(scrollPoint, animated: true)
            }
        }
        
        delegate?.onQuantityFocus(textField)
        
        if textField.keyboardType == UIKeyboardType.DecimalPad{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            item = UIBarButtonItem(title: NSLocalizedString("Label_Save", comment: "Save"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onSaveTap) )
            
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        
        prevQuantity = (textField.text! as NSString).integerValue
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        let newQuantity: NSNumber = (quantity.text! as NSString).integerValue as NSNumber
        quantity.text = newQuantity.stringValue
        if prevQuantity != newQuantity{
            delegate?.onQuantityUpdate(newQuantity, cell: self)
        }
        
    }
    
    @IBAction func onUpTap(sender: UIButton) {
        //prevQuantity = (quantity.text! as NSString).integerValue
        let newQuantity: NSNumber = prevQuantity.integerValue + 1
        prevQuantity = newQuantity
        quantity.text = numberFormatter.stringFromNumber(newQuantity)!
        delegate?.onQuantityUpdate(newQuantity, cell: self)
    }
    
    @IBAction func onDownTap(sender: UIButton) {
        //prevQuantity = (quantity.text! as NSString).integerValue
        if prevQuantity.integerValue - 1 > 1{
            let newQuantity: NSNumber = prevQuantity.integerValue - 1
            prevQuantity = newQuantity
            quantity.text = numberFormatter.stringFromNumber(newQuantity)!
            delegate?.onQuantityUpdate(newQuantity, cell: self)
        }else{
            prevQuantity = 1
            quantity.text = numberFormatter.stringFromNumber(1)!
            delegate?.onQuantityUpdate(1, cell: self)
        }
    }
    
    
    func onSaveTap(){
        quantity.resignFirstResponder()
    }
}
