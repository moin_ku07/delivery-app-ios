//
//  HomeCollectionViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/24/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class HomeCollectionViewController: UICollectionViewController {
    
    var tableData: NSMutableArray = NSMutableArray()
    
    var activityView: UICustomActivityView!
    
    var isPopularStore: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)

        // Register cell classes
        self.collectionView!.registerNib(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        collectionView!.addSubview(refreshControl)
        self.collectionView!.alwaysBounceVertical = true

        dispatch_async(dispatch_get_main_queue()){ () -> Void in
            self.doLocalisation()
        }
    }
    
    func doLocalisation(){
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            let scalingTransform : CGAffineTransform = CGAffineTransformMakeScale(-1, 1)
            collectionView!.transform = scalingTransform
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //(self.tabBarController as! TabBarController).setSearchAsRightBarButton()
        self.loadTableData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return tableData.count
    }
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath)->CGSize{
        let ratio: CGFloat = 1.155
        var width: CGFloat = (view.frame.size.width - 12*3)/2
        var height: CGFloat = (width / ratio) + 25
        if view.frame.size.width > 414{
            width = (view.frame.size.width - 12*4)/3
            height = (width / ratio) + 25
            return CGSizeMake(width, height)
        }
        
        return CGSizeMake(width, height)
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: HomeCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! HomeCollectionViewCell
    
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        cell.title.text = cellData.objectForKey("name") as? String
        cell.title.font = UIFont.systemFontOfSize(14)
        cell.title.textAlignment = .Center
        
        if let photourl: String = cellData.objectForKey("thumb") as? String{
            let url: NSURL = NSURL(string: photourl)!
            cell.imageUrl = url
        }
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            let scalingTransform : CGAffineTransform = CGAffineTransformMakeScale(-1, 1)
            cell.transform = scalingTransform
        }
    
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        let cell: HomeCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! HomeCollectionViewCell
        
        let itemvc: StoreItemsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsViewController") as! StoreItemsViewController
        itemvc.rootTabBarController = self.tabBarController as? TabBarController
        itemvc.storeData = cellData
        itemvc.titleText = cell.title.text
        itemvc.thumbImage = cell.imageView.image
        itemvc.store_id = cellData.objectForKey("id") as! NSNumber
        
        self.navigationController?.presentViewController(itemvc, animated: true, completion: nil)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
    // MARK: - loadTableData
    func loadTableData(){
        if tableData.count == 0{
            activityView.showActivityIndicator()
        }
        let url: String = isPopularStore ? "api/stores/?favorite=true" : "api/stores/"
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    self.tableData = NSMutableArray(array: json)
                    self.collectionView?.reloadData()
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
    }
    
    // MARK: - refresh
    func refresh(refreshControl: UIRefreshControl) {
        loadTableData()
        refreshControl.endRefreshing()
    }

}
