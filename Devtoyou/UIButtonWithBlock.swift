//
//  UIButtonWithBlock.swift
//  Devtoyou
//
//  Created by Moin Uddin on 2/23/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

var _buttonActionBlock: (()-> Void)?

class UIButtonWithBlock: UIButton {

    func handleControlEvent(event: UIControlEvents, action: (()-> Void)){
        _buttonActionBlock = action
        self.addTarget(self, action: #selector(callActionBlock(_:)), forControlEvents: event)
    }
    
    func callActionBlock(sender: AnyObject){
        _buttonActionBlock?()
    }

}
