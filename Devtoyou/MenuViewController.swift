//
//  MenuViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/24/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class CenteredTable: UITableView {
    override func reloadData() {
        super.reloadData()
        centerTableContentsIfNeeded()
    }
    
    override func  layoutSubviews() {
        super.layoutSubviews()
        centerTableContentsIfNeeded()
    }
    
    func centerTableContentsIfNeeded() {
        let totalHeight = CGRectGetHeight(bounds)
        let contentHeight = contentSize.height
        let contentCanBeCentered = contentHeight < totalHeight
        if (contentCanBeCentered) {
            contentInset = UIEdgeInsets(top: ceil(totalHeight/2 - contentHeight/2), left: 0, bottom: 0, right: 0);
        } else {
            contentInset = UIEdgeInsetsZero;
        }
    }
}

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: CenteredTable!
    
    var selectedIndexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    var tableData: [String] = ["All Stores","Popular Stores", "About App", "Help Centre", "Rate this App"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(rgba: "df3929").colorWithAlphaComponent(0.97)
        
        let closeButton: UIButton = UIButton(frame: CGRectMake(0, 0, 22, 22))
        //closeButton.backgroundColor = UIColor.yellowColor()
        closeButton.setBackgroundImage(UIImage(named: "icon-close"), forState: UIControlState.Normal)
        closeButton.addTarget(self, action: #selector(onCloseTap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(closeButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 22))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 22))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 15))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: -12))
        
        
        tableView.delegate = self
        tableView.dataSource = self

        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.selectionStyle = .None
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        
        cell.textLabel?.text = tableData[indexPath.row]
        cell.textLabel?.textAlignment = .Center
        cell.textLabel?.font = UIFont.systemFontOfSize(18)
        
        if indexPath.compare(selectedIndexPath) == NSComparisonResult.OrderedSame{
            cell.textLabel?.textColor = UIColor.whiteColor()
        }else{
            cell.textLabel?.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        }
        
        return cell
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - onCloseTap
    func onCloseTap(sender: UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
