#import "UIFont+SystemFontOverride.h"

@implementation UIFont (SystemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
    //NSLog(@"boldFont: %f", fontSize);
    return [UIFont fontWithName:@"BahijTheSansArabicExtraBold" size:fontSize];
}

+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
    //NSLog(@"plainFont: %f", fontSize);
    return [UIFont fontWithName:@"BahijTheSansArabicPlain" size:fontSize];
}

#pragma clang diagnostic pop

@end