//
//  NavigationController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/24/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prefersStatusBarHidden() -> Bool {
        if let lastVC = viewControllers.last{
            return lastVC.prefersStatusBarHidden()
        }
        return false
    }
    
    override func shouldAutorotate() -> Bool {
        if let lastVC = viewControllers.last{
            return lastVC.shouldAutorotate()
        }
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if let lastVC = viewControllers.last{
            return lastVC.supportedInterfaceOrientations()
        }
        
        return UIInterfaceOrientationMask.AllButUpsideDown
    }

}
