//
//  IntroVC3.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/31/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol IntroVC3Delegate{
    func onGotItTap()
}

class IntroVC3: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UITextView!
    @IBOutlet var gotitButton: UIButton!
    var delegate: IntroVC3Delegate?
    var pageIndex: Int = 2

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            titleLabel.text = "لا تشيل هم"
            descriptionLabel.text = "نوصل لك في أي مكان تريد وحسب الوقت الذي يناسبك،  نتشرف بخدمتك"
            descriptionLabel.font = UIFont(name: "BahijTheSansArabicPlain", size: 12.0)
            descriptionLabel.textAlignment = .Center
        }
        
        descriptionLabel.setContentOffset(CGPointZero, animated: false)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.layoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        descriptionLabel.setContentOffset(CGPointZero, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onGotItTap(sender: AnyObject) {
        print("onGotItTap")
        self.delegate?.onGotItTap()
    }

}
