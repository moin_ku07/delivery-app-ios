//
//  StoreImageCollectionViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 11/11/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import SDWebImage

class StoreImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var thumbActivity: UIActivityIndicatorView!
    
    var thumbUrl: NSURL?{
        didSet{
            imageView.sd_setImageWithURL(thumbUrl) { (image:UIImage!, error: NSError!, cache: SDImageCacheType, url: NSURL!) -> Void in
                self.thumbActivity.stopAnimating()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.userInteractionEnabled = true
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onImageTap))
        tapGesture.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapGesture)
    }
    
    func onImageTap(){
        if imageView.image != nil{
            DVImagePopup(image: imageView.image!, sourceView: (UIApplication.sharedApplication().delegate as! AppDelegate).window!)
        }
    }
    
}
