//
//  HistoryDetailTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/15/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RateView

class HistoryDetailTableViewController: UITableViewController, HistoryDetailTableViewCellDelegate, FeedbackPopupViewControllerDelegate {
    
    @IBOutlet var driverLabel: UILabel!
    @IBOutlet var driverRateView: RateView!
    @IBOutlet var customerLabel: UILabel!
    @IBOutlet var customerRateView: RateView!
    @IBOutlet var gCustomerLabel: UILabel!
    @IBOutlet var gCustomerRateView: RateView!
    @IBOutlet var gStoreLabel: UILabel!
    @IBOutlet var gStoreRateView: RateView!
    @IBOutlet var gDriverLabel: UILabel!
    @IBOutlet var gDriverRateView: RateView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var feedbackByYouView: UIView!
    @IBOutlet var userRatingView: UIView!
    @IBOutlet var feedbackByYouLabel: UILabel!
    @IBOutlet var storeItemsHeaderFeedbackLabel: UILabel!
    @IBOutlet var feedbackProvidedByView: UIView!
    @IBOutlet var feedbackProvidedByRateView: UIView!
    
    var tableSection: [NSMutableDictionary] = [NSMutableDictionary]()
    var tableRow: [[NSMutableDictionary]] = [[NSMutableDictionary]]()
    
    var tableData: NSArray!
    var orderID: NSNumber!
    var orderData: NSDictionary!
    
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    var user_type: String!
    
    var noDataMessageLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        user_type = prefs.objectForKey("user_type") as! String
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let rateViewArray: [RateView] = [driverRateView, customerRateView, gCustomerRateView, gStoreRateView, gDriverRateView]
        for rv in rateViewArray{
            rv.starSize = 15
            rv.starFillColor = UIColor(rgba: "de3929")
            rv.starBorderColor = UIColor(rgba: "de3929")
            rv.starNormalColor = UIColor(rgba: "efecd9")
            
            if [gCustomerRateView, gStoreRateView, gDriverRateView].contains(rv){
                let rTapG: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onGRateTap(_:)))
                rTapG.numberOfTapsRequired = 1
                rv.userInteractionEnabled = true
                rv.addGestureRecognizer(rTapG)
            }
        }
        
        let driverRateTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onDriverRateTap(_:)))
        driverRateTap.numberOfTapsRequired = 1
        driverRateView.userInteractionEnabled = true
        driverRateView.addGestureRecognizer(driverRateTap)
        
        let customerRateTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onCustomerRateTap(_:)))
        customerRateTap.numberOfTapsRequired = 1
        customerRateView.userInteractionEnabled = true
        customerRateView.addGestureRecognizer(customerRateTap)
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        tableData = orderData.objectForKey("items") as! NSArray
        
        loadTableData()
    }
    
    // MARK: - 
    func filterTableData(){
        tableSection.removeAll()
        tableRow.removeAll()
        
        if let driver_feedback: [NSDictionary] = orderData.objectForKey("driver_feedback") as? [NSDictionary]{
            for df in driver_feedback{
                if let rating: NSNumber = df.objectForKey("rating") as? NSNumber{
                    if (df.objectForKey("given_by") as? String == "customer" && user_type == "customer") || (df.objectForKey("given_by") as? String == "store_owner" && user_type == "store_owner"){
                        driverRateView.rating = rating.floatValue
                    }else if user_type == "driver" && df.objectForKey("given_by") as? String == "customer"{
                        gCustomerRateView.rating = rating.floatValue
                        gCustomerRateView.feedbackText = df.objectForKey("text") as? String
                    }else if user_type == "driver" && df.objectForKey("given_by") as? String == "store_owner"{
                        gStoreRateView.rating = rating.floatValue
                        gCustomerRateView.feedbackText = df.objectForKey("text") as? String
                    }
                }
                
            }
        }
        
        if let customers_feedback: [NSDictionary] = orderData.objectForKey("customers_feedback") as? [NSDictionary]{
            for df in customers_feedback{
                if let rating: NSNumber = df.objectForKey("rating") as? NSNumber{
                    print("customers_feedback rating: \(rating)")
                    
                    if user_type == "driver" && df.objectForKey("given_by") as? String == "driver"{
                        // show rating in givenbyyou customerrateview by driver
                        customerRateView.rating = rating.floatValue
                    }else if user_type == "store_owner" && df.objectForKey("given_by") as? String == "store_owner"{
                        // show rating in givenbyyou customerrateview by driver
                        customerRateView.rating = rating.floatValue
                    }else if user_type == "customer" && df.objectForKey("given_by") as? String == "store_owner"{
                        // show feedback provided to customer by store_owner
                        gStoreRateView.rating = rating.floatValue
                        gStoreRateView.feedbackText = df.objectForKey("text") as? String
                    }else if user_type == "customer" && df.objectForKey("given_by") as? String == "driver"{
                        // feedback provided to customer by driver
                        gDriverRateView.rating = rating.floatValue
                        gDriverRateView.feedbackText = df.objectForKey("text") as? String
                    }
                }
                
            }
        }
        
        if orderData.objectForKey("driver") as? NSNumber == nil || user_type == "driver" && (orderData.objectForKey("driver") as? NSNumber != nil){
            //tableView.tableHeaderView = nil
            //storeItemsHeaderFeedbackLabel.text = feedbackByYouLabel.text
            //feedbackByYouView.removeFromSuperview()
            
            driverLabel.removeFromSuperview()
            driverRateView.removeFromSuperview()
            tableView.layoutIfNeeded()
        }
        
        if user_type == "customer"{
            customerLabel.removeFromSuperview()
            customerRateView.removeFromSuperview()
            
            gCustomerLabel.removeFromSuperview()
            gCustomerRateView.removeFromSuperview()
        }else if user_type == "store_owner"{
            gStoreLabel.removeFromSuperview()
            gStoreRateView.removeFromSuperview()
            
            gCustomerLabel.removeFromSuperview()
            gCustomerRateView.removeFromSuperview()
            
            if prefs.objectForKey("id") as? NSNumber == orderData.objectForKey("user") as? NSNumber{
                print("here")
                customerLabel.removeFromSuperview()
                customerRateView.removeFromSuperview()
            }
            
            // until store feedback by driver
            //headerView.removeFromSuperview()
            //feedbackProvidedByView.removeFromSuperview()
            //feedbackProvidedByRateView.removeFromSuperview()
        }else if user_type == "driver"{
            gDriverLabel.removeFromSuperview()
            gDriverRateView.removeFromSuperview()
        }
        
        
        if self.noDataMessageLabel != nil{
            self.noDataMessageLabel!.removeFromSuperview()
        }
        
        if tableData.count > 0{
            var sectionText: String = ""
            var itemsArray: [NSMutableDictionary] = [NSMutableDictionary]()
            var storeFeedbackObj: NSDictionary? = nil
            
            for (index, item) in (tableData as! [NSDictionary]).enumerate(){
                storeFeedbackObj = nil
                if tableSection.count == 0{
                    sectionText = item.objectForKey("store_name") as! String
                    
                    if let feedbackArray: [NSDictionary] = item.objectForKey("stores_feedback") as? [NSDictionary]{
                        for fbobj in feedbackArray{
                            if fbobj.objectForKey("given_by") as? String == user_type{
                                storeFeedbackObj = fbobj
                            }
                            if user_type == "store_owner" && fbobj.objectForKey("given_by") as? String == "driver" && fbobj.objectForKey("store") as? NSNumber == prefs.objectForKey("store_id") as? NSNumber{
                                gDriverRateView.rating = (fbobj.objectForKey("rating") as! NSNumber).floatValue
                                gDriverRateView.feedbackText = fbobj.objectForKey("text") as? String
                            }
                        }
                    }
                    
                    tableSection.append(["name": sectionText, "id": item.objectForKey("store_id") as! NSNumber, "stores_feedback": storeFeedbackObj != nil ? storeFeedbackObj! : NSNull()])
                    
                    itemsArray.removeAll()
                    itemsArray.append(NSMutableDictionary(dictionary: item))
                }else if sectionText != item.objectForKey("store_name") as! String{
                    sectionText = item.objectForKey("store_name") as! String
                    
                    if let feedbackArray: [NSDictionary] = item.objectForKey("stores_feedback") as? [NSDictionary]{
                        for fbobj in feedbackArray{
                            if fbobj.objectForKey("given_by") as? String == user_type{
                                storeFeedbackObj = fbobj
                            }
                            if user_type == "store_owner" && fbobj.objectForKey("given_by") as? String == "driver" && fbobj.objectForKey("store") as? NSNumber == prefs.objectForKey("store_id") as? NSNumber{
                                gDriverRateView.rating = (fbobj.objectForKey("rating") as! NSNumber).floatValue
                                gDriverRateView.feedbackText = fbobj.objectForKey("text") as? String
                            }
                        }
                    }
                    
                    tableSection.append(["name": sectionText, "id": item.objectForKey("store_id") as! NSNumber, "stores_feedback": storeFeedbackObj != nil ? storeFeedbackObj! : NSNull()])
                    tableRow.append(itemsArray)
                    
                    itemsArray.removeAll()
                    itemsArray.append(NSMutableDictionary(dictionary: item))
                }else{
                    itemsArray.append(NSMutableDictionary(dictionary: item))
                }
                
                if index == tableData.count - 1{
                    tableRow.append(itemsArray)
                }
            }
            
            //print(tableSection)
            //print(tableRow)
        }else{
            tableView.tableHeaderView = nil
            if orderData.objectForKey("driver") as? NSNumber == nil{
                feedbackByYouView.removeFromSuperview()
                userRatingView.removeFromSuperview()
                feedbackProvidedByView.removeFromSuperview()
                feedbackProvidedByRateView.removeFromSuperview()
                
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width,
                    self.view.bounds.size.height))
                
                self.noDataMessageLabel!.text = NSLocalizedString("Label_Review_Not_Applicable", comment: "No review")
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                //self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.view.addSubview(self.noDataMessageLabel!)
            }else{
                customerLabel.removeFromSuperview()
                customerRateView.removeFromSuperview()
                gCustomerLabel.removeFromSuperview()
                gCustomerRateView.removeFromSuperview()
                gStoreLabel.removeFromSuperview()
                gStoreRateView.removeFromSuperview()
            }
        }
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        if user_type == "driver"{
//            return 0
//        }
        return tableSection.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if user_type == "driver"{
            return 0
        }
        return tableRow[section].count
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 44))
        header.backgroundColor = UIColor(rgba: "ddd9c0")
        if user_type == "driver"{
            header.backgroundColor = UIColor(rgba: "EFECD9")
        }
        
        let title: UILabel = UILabel(frame: CGRectMake(0, 0, 200, 25))
        title.font = UIFont.boldSystemFontOfSize(15)
        title.text = tableSection[section].objectForKey("name") as? String
        title.textColor = UIColor(rgba: "8B837B")
        title.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(title)
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            title.textAlignment = .Right
        }
        
        let rateView: RateView = RateView(frame: CGRectMake(0, 0, 75, 15))
        rateView.starSize = 15
        rateView.starFillColor = UIColor(rgba: "de3929")
        rateView.starBorderColor = UIColor(rgba: "de3929")
        rateView.starNormalColor = UIColor(rgba: "efecd9")
        rateView.canRate = false
        rateView.tag = section
        
        rateView.translatesAutoresizingMaskIntoConstraints = false
        header.addSubview(rateView)
        
        let viewDict: [String: AnyObject] = ["view": header, "rateView": rateView, "title": title]
        
        header.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-16-[title]-8-[rateView(75)]-8-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewDict))
        header.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[rateView(15)]", options: NSLayoutFormatOptions(), metrics: nil, views: viewDict))
        
        header.addConstraint(NSLayoutConstraint(item: title, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: header, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
        header.addConstraint(NSLayoutConstraint(item: rateView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: header, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
        
        let rateTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onHeaderRateTap(_:)))
        rateTap.numberOfTapsRequired = 1
        rateView.userInteractionEnabled = true
        rateView.addGestureRecognizer(rateTap)
        
        if let stores_feedback: NSDictionary = tableSection[section].objectForKey("stores_feedback") as? NSDictionary, let rating: NSNumber = stores_feedback.objectForKey("rating") as? NSNumber{
            rateView.rating = rating.floatValue
        }
        
        
        /*if section > 0{
            addBorderLayer(header, edge: UIRectEdge.Top, color: UIColor.grayColor(), thickness: 1.0)
        }
        addBorderLayer(header, edge: UIRectEdge.Bottom, color: UIColor.grayColor(), thickness: 1.0)*/
        
        return header
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: HistoryDetailTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HistoryDetailTableViewCell
        cell.selectionStyle = .None
        cell.delegate = self
        
        let cellData: NSDictionary = tableRow[indexPath.section][indexPath.row]
        
        cell.name.text = cellData.objectForKey("item_name") as? String
        
        if let stores_feedback: NSDictionary = cellData.objectForKey("items_feedback") as? NSDictionary, let rating: NSNumber = stores_feedback.objectForKey("rating") as? NSNumber{
            cell.rateView.rating = rating.floatValue
        }
        
        cell.rateView.canRate = false

        return cell
    }
    
    // MARK: - HistoryDetailTableViewCellDelegate
    func onRateTap(cell: HistoryDetailTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableRow[indexPath.section][indexPath.row]
            //print(cellData)
            let vc: FeedbackPopupViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackPopupViewController") as! FeedbackPopupViewController
            vc.delegate = self
            if let items_feedback: NSDictionary = cellData.objectForKey("items_feedback") as? NSDictionary{
                vc.shouldSubmit = false
                vc.ratingValue = (items_feedback.objectForKey("rating") as? NSNumber)?.floatValue
                vc.feedbackString = items_feedback.objectForKey("text") as? String
            }else{
                if user_type == "store_owner"{
                    return
                }
                vc.shouldSubmit = true
            }
        
            vc.orderID = self.orderID
            vc.itemID = cellData.objectForKey("store_item") as? NSNumber
            vc.apiURL = "api/item-evaluations/"
            
            vc.titleString = NSLocalizedString("Label_Feedback_Item_Title", comment: "Item Rating")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    func onHeaderRateTap(recognizer: UITapGestureRecognizer){
        let vc: FeedbackPopupViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackPopupViewController") as! FeedbackPopupViewController
        vc.delegate = self
        if let stores_feedback: NSDictionary = tableSection[recognizer.view!.tag].objectForKey("stores_feedback") as? NSDictionary{
            vc.shouldSubmit = false
            vc.ratingValue = (stores_feedback.objectForKey("rating") as? NSNumber)?.floatValue
            vc.feedbackString = stores_feedback.objectForKey("text") as? String
        }else{
            if user_type == "store_owner"{
                return
            }
            vc.shouldSubmit = true
        }
        
        vc.orderID = orderID
        vc.storeID = tableSection[recognizer.view!.tag].objectForKey("id") as? NSNumber
        vc.apiURL = "api/store-evaluations/"
        
        vc.titleString = NSLocalizedString("Label_Feedback_Store_Title", comment: "Store Rating")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func onDriverRateTap(recognizer: UITapGestureRecognizer){
        let vc: FeedbackPopupViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackPopupViewController") as! FeedbackPopupViewController
        vc.delegate = self
        
        if let rating: Float = (recognizer.view as? RateView)?.rating where rating > 0{
            vc.shouldSubmit = false
            vc.ratingValue = rating
            if let driver_feedback: [NSDictionary] = orderData.objectForKey("driver_feedback") as? [NSDictionary]{
                for df in driver_feedback{
                    if let feedbackText: String = df.objectForKey("text") as? String{
                        if (df.objectForKey("given_by") as? String == "customer" && user_type == "customer") || (df.objectForKey("given_by") as? String == "store_owner" && user_type == "store_owner"){
                            vc.feedbackString = feedbackText
                        }
                    }
                    
                }
            }
        }else{
            vc.shouldSubmit = true
        }
        
        vc.orderID = orderID
        vc.driverID = orderData.objectForKey("driver") as? NSNumber
        vc.apiURL = "api/driver-evaluations/"
        
        vc.titleString = NSLocalizedString("Label_Feedback_Driver_Title", comment: "Driver Feedback")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func onCustomerRateTap(recognizer: UITapGestureRecognizer){
        let vc: FeedbackPopupViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackPopupViewController") as! FeedbackPopupViewController
        vc.delegate = self
        
        if let rating: Float = (recognizer.view as? RateView)?.rating where rating > 0{
            vc.shouldSubmit = false
            vc.ratingValue = rating
            if let customers_feedback: [NSDictionary] = orderData.objectForKey("customers_feedback") as? [NSDictionary]{
                for df in customers_feedback{
                    if let feedbackText: String = df.objectForKey("text") as? String{
                        vc.feedbackString = feedbackText
                    }
                    
                }
            }
        }else{
            vc.shouldSubmit = true
        }
        
        vc.orderID = orderID
        vc.customerID = orderData.objectForKey("user") as? NSNumber
        vc.apiURL = "api/customer-evaluations/"
        
        vc.titleString = NSLocalizedString("Label_Feedback_Customer_Title", comment: "Customer Feedback")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func onGRateTap(recognizer: UITapGestureRecognizer){
        print("todo gRateTap")
        if let rating: Float = (recognizer.view as? RateView)?.rating where rating > 0{
            let vc: FeedbackPopupViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackPopupViewController") as! FeedbackPopupViewController
            vc.delegate = self
            vc.shouldSubmit = false
            
            if let rating: Float = (recognizer.view as? RateView)?.rating where rating > 0{
                vc.shouldSubmit = false
                vc.ratingValue = rating
            }
            
            if user_type == "customer"{
                if let customers_feedback: [NSDictionary] = orderData.objectForKey("customers_feedback") as? [NSDictionary]{
                    for df in customers_feedback{
                        if let feedbackText: String = df.objectForKey("text") as? String{
                            if df.objectForKey("given_by") as? String == "store_owner" && recognizer.view == gStoreRateView{
                                vc.feedbackString = feedbackText
                                vc.titleString = NSLocalizedString("Label_Feedback_Store_Title", comment: "Store Rating")
                            }else if df.objectForKey("given_by") as? String == "driver" && recognizer.view == gDriverRateView{
                                vc.feedbackString = feedbackText
                                vc.titleString = NSLocalizedString("Label_Feedback_Driver_Title", comment: "Driver Feedback")
                            }
                        }
                        
                    }
                }
            }else if user_type == "driver"{
                if let driver_feedback: [NSDictionary] = orderData.objectForKey("driver_feedback") as? [NSDictionary]{
                    for df in driver_feedback{
                        if let feedbackText: String = df.objectForKey("text") as? String{
                            if df.objectForKey("given_by") as? String == "customer" && recognizer.view == gCustomerRateView{
                                vc.feedbackString = feedbackText
                                vc.titleString = NSLocalizedString("Label_Feedback_Customer_Title", comment: "Customer Feedback")
                            }else if df.objectForKey("given_by") as? String == "store_owner" && recognizer.view == gStoreRateView{
                                vc.feedbackString = feedbackText
                                vc.titleString = NSLocalizedString("Label_Feedback_Store_Title", comment: "Store Rating")
                            }
                        }
                    }
                }
            }else if user_type == "store_owner"{
                vc.feedbackString = (recognizer.view! as! RateView).feedbackText
                vc.titleString = NSLocalizedString("Label_Feedback_Driver_Title", comment: "Driver Feedback")
            }
            
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    // MARk: - FeedbackPopupViewControllerDelegate
    func onFeedbackSubmitSuccess() {
        loadTableData()
    }
    
    // MARK: - loadTableData
    func loadTableData(){
        var url: String = "api/orders/" + orderID.stringValue + "/?include_feedback=true&"
        
        if user_type == "store_owner"{
            url = url + "store=" + (prefs.objectForKey("store_id") as! NSNumber).stringValue
        }else if user_type == "driver"{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "driver=" + user_id
        }else{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "user=" + user_id
        }
        
        view.bringSubviewToFront(activityView)
        
        activityView.showActivityIndicator()
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    //print(json)
                    self.orderData = json
                    self.tableData = []
                    self.tableData = json.objectForKey("items") as? NSArray
                    self.filterTableData()
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
    }
    
}

extension RateView{
    private struct AssociatedKeys {
        static var FeedbackText = "nsh_FeedbackText"
    }
    
    var feedbackText: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.FeedbackText) as? String
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.FeedbackText,
                    newValue as NSString?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
}

func addBorderLayer(view: UIView, edge: UIRectEdge, color: UIColor, thickness: CGFloat){
    let border: CALayer = CALayer()
    
    switch edge{
        case UIRectEdge.Top: border.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame), thickness)
        case UIRectEdge.Bottom: border.frame = CGRectMake(0, CGRectGetHeight(view.frame) - thickness, CGRectGetWidth(view.frame), thickness)
        case UIRectEdge.Left: border.frame = CGRectMake(0, 0, thickness, CGRectGetHeight(view.frame))
        case UIRectEdge.Right: border.frame = CGRectMake(CGRectGetWidth(view.frame) - thickness, 0, thickness, CGRectGetHeight(view.frame))
        default: break
    }
    
    border.backgroundColor = color.CGColor
    view.layer.addSublayer(border)
}
