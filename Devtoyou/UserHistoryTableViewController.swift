//
//  UserHistoryTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/8/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class UserHistoryTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UserHistoryTableViewCellDelegate {
    
    var tableData: NSMutableArray = NSMutableArray()
    var activityView: UICustomActivityView!
    var noDataMessageLabel: UILabel?
    var navigationTitle: String?
    
    var selectedStoreID: NSNumber?
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle

        //self.navigationController?.navigationBar.topItem!.title = ""
        
        tableView.registerNib(UINib(nibName: "UserHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        loadTableData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationTitle != nil{
            self.navigationItem.title = navigationTitle!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableData.count > 0{
            self.noDataMessageLabel?.hidden = true
        }else{
            if self.noDataMessageLabel == nil{
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                    self.tableView.bounds.size.height))
                
                self.noDataMessageLabel!.text = NSLocalizedString("Label_No_Item_Order", comment: "No Item")
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.tableView.backgroundView = self.noDataMessageLabel!
                self.noDataMessageLabel?.hidden = true
                //print("label added")
            }else{
                self.noDataMessageLabel?.hidden = false
            }
        }
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UserHistoryTableViewCell
        cell.selectionStyle = .None
        cell.delegate = self
        
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        if let statusCode: String = cellData.objectForKey("status") as? String{
            cell.statusButton.setTitle(getStatusTextFromCode(statusCode), forState: UIControlState.Normal)
            if statusCode == "DELIV"{
                cell.feedbackButton.hidden = false
                if let items: NSArray = cellData.objectForKey("items") as? NSArray where items.count > 0{
                    cell.uploadButton.hidden = false
                }
            }else{
                cell.feedbackButton.hidden = true
                cell.uploadButton.hidden = true
            }
        }
        
        if let datetimeString: String = cellData.objectForKey("updated_at") as? String{
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let datetime: NSDate = dateFormatter.dateFromString(datetimeString){
                //print(datetime)
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "hh:mm a"
                dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                cell.timeLabel.text = dateFormatter.stringFromDate(datetime)
                dateFormatter.dateFormat = "dd MMM, yyyy"
                cell.dateLabel.text = dateFormatter.stringFromDate(datetime)
            }
        }
        
        let formattedString: NSMutableAttributedString = NSMutableAttributedString()
        if let orederID: String = cellData.objectForKey("display_id") as? String{
            let newStr = NSAttributedString(string: "#\(orederID)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)])
            formattedString.appendAttributedString(newStr)
        }
        if let pickup: String = cellData.objectForKey("pickup") as? String{
            var pickupStr: String = NSLocalizedString("Label_Pickup_Time", comment: "Delivery time")
            
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let datetime: NSDate = dateFormatter.dateFromString(pickup){
                //print(datetime)
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                }
                pickupStr += ": " + dateFormatter.stringFromDate(datetime)
            }
            
            let newStr = NSAttributedString(string: "\(pickupStr)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            formattedString.appendAttributedString(newStr)
        }
        
        if let items: NSArray = cellData.objectForKey("items") as? NSArray where items.count > 0{
            for item in (items as! [NSDictionary]){
                var itemName: String = String()
                if let item_name: String = item.objectForKey("item_name") as? String{
                    itemName = item_name
                }
                
                var priceStr: String = String()
                if let price: NSNumber = item.objectForKey("price") as? NSNumber{
                    priceStr = NSString(format: "%.2f", price.floatValue) as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        priceStr = numberFormatter.stringFromNumber(price)!
                    }
                }
                
                var quantityStr: String = String()
                if let quantity: NSString = item.objectForKey("quantity") as? NSString{
                    quantityStr = quantity as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity.integerValue)!
                    }
                }
                
                if let quantity: NSNumber = item.objectForKey("quantity") as? NSNumber{
                    quantityStr = quantity.stringValue
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity)!
                    }
                }
                
                var storeName: String = String()
                if let store_name: String = item.objectForKey("store_name") as? String{
                    storeName = store_name
                }
                
                let currencyCode: String = NSLocalizedString("Label_Currency_Code", comment: "SAR")
                let fromStr: String = NSLocalizedString("Label_Order_From_Store", comment: "From")
                
                var newStr = NSAttributedString(string: "\(itemName) (\(priceStr) \(currencyCode)) x \(quantityStr) \(fromStr) \(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    newStr = NSAttributedString(string: "\u{200F}\(itemName) (\(priceStr) \(currencyCode)) \u{200F}x \(quantityStr) \(fromStr) \u{200F}\(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                }
                formattedString.appendAttributedString(newStr)
            }
        }else if let additional_notes: String = cellData.objectForKey("additional_notes") as? String{
            let newStr: NSAttributedString = NSAttributedString(string: "\(additional_notes)\n")
            formattedString.appendAttributedString(newStr)
        }
        
        if let total: NSNumber = cellData.objectForKey("total") as? NSNumber where total.floatValue > 0{
            var newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(total)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(numberFormatter.stringFromNumber(total)!)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            }
            formattedString.appendAttributedString(newStr)
        }
        
        if let paymentMethods: [NSDictionary] = (UIApplication.sharedApplication().delegate as? AppDelegate)?._paymentMethods{
            for dict in paymentMethods{
                if (dict["id"] as? NSNumber)?.integerValue == (cellData["payment_method"] as? NSNumber)?.integerValue{
                    formattedString.appendAttributedString(NSAttributedString(string: NSLocalizedString("Label_Payment_Option", comment: "Payment Option") + ": " + (dict["name"] as! String), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)]))
                    break
                }
            }
        }
        
        var addressText: String = ""
        let addressTextArray: NSMutableArray = NSMutableArray()
        
        if let locationDict: NSDictionary = cellData.objectForKey("user_location") as? NSDictionary{
            if let name: String = locationDict.objectForKey("name") as? String where name != ""{
                addressTextArray.addObject(name)
            }
            if let houseno: String = locationDict.objectForKey("house_no") as? String where houseno != ""{
                addressTextArray.addObject(NSLocalizedString("Label_House_No", comment: "House no.") + " " + houseno)
            }
            if let street: String = locationDict.objectForKey("street") as? String where street != ""{
                addressTextArray.addObject(street)
            }
            
            addressText = (NSArray(array: addressTextArray) as! Array).joinWithSeparator(", ")
            
            if let region: String = locationDict.objectForKey("region") as? String where region != ""{
                //print(region)
                addressText = addressText + "\n" + region
            }
            if let phone: String = locationDict.objectForKey("phone") as? String where phone != ""{
                addressText = addressText + "\n" + NSLocalizedString("Label_Phone", comment: "Phone no") + ": " + phone
            }
            if let note: String = locationDict.objectForKey("note") as? String where note != ""{
                addressText = addressText + "\n" + note
            }
            
            formattedString.appendAttributedString(NSAttributedString(string: "\n" + NSLocalizedString("Label_Order_Delivery_Address", comment: "Order Details") + ":\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)]))
            formattedString.appendAttributedString(NSAttributedString(string: addressText.stringByReplacingOccurrencesOfString("^\\n*", withString: "", options: .RegularExpressionSearch), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)]))
        }
        
        cell.feedbackText.textAlignment = .Natural
        cell.feedbackText.attributedText = formattedString
        cell.feedbackText.textColor = UIColor(rgba: "8B837B")
        dispatch_async(dispatch_get_main_queue(), {
            cell.feedbackText.contentOffset = CGPointZero
        })

        return cell
    }
    
    // MARK: - UserHistoryTableViewCellDelegate
    func onLeaveFeedbackTap(cell: UserHistoryTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            let vc: HistoryDetailTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HistoryDetailTableViewController") as! HistoryDetailTableViewController
            if let orderIDN: String = cellData.objectForKey("display_id") as? String{
                vc.navigationItem.title = "#\(orderIDN)"
            }
            vc.orderID = cellData.objectForKey("id") as! NSNumber
            vc.orderData = cellData
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func onStatusButtonTap(cell: UserHistoryTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            if prefs.objectForKey("user_type") as? String == "driver"{
                let vc: DriverOrderTrackViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DriverOrderTrackViewController") as! DriverOrderTrackViewController
                vc.orderData = cellData as! NSMutableDictionary
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc: TrackOrderViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TrackOrderViewController") as! TrackOrderViewController
                vc.order_id = cellData.objectForKey("id") as! NSNumber
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func onUploadImageTap(cell: UserHistoryTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            self.selectedStoreID = nil
            
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            if let items: NSArray = cellData.objectForKey("items") as? NSArray{
                var storeNames: [String] = [String]()
                var storeIDs: [NSNumber] = [NSNumber]()
                
                for item in (items as! [NSDictionary]){
                    if let store_name: String = item.objectForKey("store_name") as? String, store_id: NSNumber = item.objectForKey("store_id") as? NSNumber{
                        if storeNames.indexOf(store_name) == nil{
                            storeNames.append(store_name)
                            storeIDs.append(store_id)
                        }
                    }
                }
                
                ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Store", comment: "Select store"), rows: storeNames, initialSelection: 0, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
                        self.selectedStoreID = storeIDs[selectedIndex]
                        self.showImagePickerOptions(cell)
                    }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                        print("nothing")
                    }, origin: cell.uploadButton)
            }
        }
    }
    
    func showImagePickerOptions(cell: UserHistoryTableViewCell){
        let imagePicker: UIImagePickerController = UIImagePickerController()
        ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Photo_Option", comment: "Choose Photo"), rows: [NSLocalizedString("Label_Select_Photo_Library", comment: "Photo Gallery"), NSLocalizedString("Label_Select_Photo_Camera", comment: "Camera")], initialSelection: 0, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
            if selectedIndex == 0{
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            }else{
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            }
            
            //imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.presentViewController(imagePicker, animated: true, completion: nil)
            }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                print("nothing")
            }, origin: cell.uploadButton)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let imageInfo = info as NSDictionary
        
        var image: UIImage = imageInfo.objectForKey(UIImagePickerControllerOriginalImage) as! UIImage
        
        let imageSize: CGSize = image.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        if (width != height) {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2;
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0)
            image.drawAtPoint(CGPointMake(-widthOffset, -heightOffset), blendMode: CGBlendMode.Copy, alpha: 1)
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
        }
        
        //scale down image
        let scaledImage = self.scaleImageWith(image, newSize: CGSizeMake(100, 100 / (image.size.width / image.size.height)))
        
        let imageData = UIImagePNGRepresentation(scaledImage)
        
        activityView.showActivityIndicator()
        var imageObj: NSMutableDictionary = NSMutableDictionary()
        
        imageObj = ["filename": "photo.png", "fieldname": "photo", "mimetype": "image/png", "data": imageData!]
        
        let postData: NSDictionary = [
            "store": self.selectedStoreID!
        ]
        
        print(postData)
        
        DataManager.postDataWithImageAsyncWithCallback("api/store-photos/", method: "POST", data: postData, imageObj: imageObj) {  (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                print("success. Nothing to do atm")
                self.selectedStoreID = nil
                AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
            }else{
                print(error?.localizedDescription)
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Try_Again", comment: "Try again"), buttonNames: nil, completion: nil)
            }
        }
    }
    
    func scaleImageWith( image: UIImage, newSize: CGSize) -> UIImage{
        //println(newSize)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

    // MARK: - loadTableData
    func loadTableData(){
        var url: String = "api/orders/?include_feedback=true&include_address=true&"
        
        if prefs.objectForKey("user_type") as? String == "store_owner"{
            url = url + "store=" + (prefs.objectForKey("store_id") as! NSNumber).stringValue
        }else if prefs.objectForKey("user_type") as? String == "driver"{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "driver=" + user_id
        }else{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "user=" + user_id
        }
        if tableData.count == 0{
            activityView.showActivityIndicator()
        }
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    //print(json)
                    self.tableData = NSMutableArray(array: json)
                    self.tableView.reloadData()
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
    }

}

func getStatusTextFromCode(code: String)->String{
    var fullText: String = ""
    switch code{
        case "REQ": fullText = NSLocalizedString("Label_Status_Code_REQ", comment: "Request")
        case "REV": fullText = NSLocalizedString("Label_Status_Code_REV", comment: "Review")
        case "APPR": fullText = NSLocalizedString("Label_Status_Code_APPR", comment: "Approved")
        case "AD": fullText = NSLocalizedString("Label_Status_Code_AD", comment: "Assigned to Driver")
        case "DOTW": fullText = NSLocalizedString("Label_Status_Code_DOTW", comment: "Driver On The Way")
        case "DIS": fullText = NSLocalizedString("Label_Status_Code_DIS", comment: "Driver In The Store")
        case "DLS": fullText = NSLocalizedString("Label_Status_Code_DLS", comment: "Driver Left Store")
        case "DELIV": fullText = NSLocalizedString("Label_Status_Code_DELIV", comment: "Delivered")
        case "MCL": fullText = NSLocalizedString("Label_Status_Code_MCL", comment: "Moderator Closed")
        case "UCAN": fullText = NSLocalizedString("Label_Status_Code_UCAN", comment: "User Cancelled")
        case "MREJ": fullText = NSLocalizedString("Label_Status_Code_MREJ", comment: "Moderator Rejected Order")
        default:   ""
    }
    return fullText
}
