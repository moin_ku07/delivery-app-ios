//
//  PlaceOrderPaymentTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 4/6/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class PlaceOrderPaymentTableViewCell: UITableViewCell {
    
    @IBOutlet var leftImage: UIImageView!
    @IBOutlet var rightImage: UIImageView!
    @IBOutlet var title: UILabel!
    
    var isCellSelected: Bool = false{
        didSet{
            if self.isCellSelected{
                leftImage.image = UIImage(named: "icon-radio-checked")
                rightImage.image = UIImage(named: "icon-radio-checked")
            }else{
                leftImage.image = UIImage(named: "icon-radio-checked")
                rightImage.image = UIImage(named: "icon-radio-unchecked")
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        title.text = ""
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
