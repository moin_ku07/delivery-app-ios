//
//  MyOrderTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/6/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class MyOrderTableViewController: UITableViewController, MyOrderTableViewCellDelegate {
    
    var tableData: NSMutableArray = NSMutableArray()
    var activityView: UICustomActivityView!
    var noDataMessageLabel: UILabel?
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle

        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadTableData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableData.count > 0{
            self.noDataMessageLabel?.hidden = true
        }else{
            if self.noDataMessageLabel == nil{
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                    self.tableView.bounds.size.height))
                
                self.noDataMessageLabel!.text = NSLocalizedString("Label_No_Item_Order", comment: "No Item")
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.tableView.backgroundView = self.noDataMessageLabel!
                self.noDataMessageLabel?.hidden = true
                //print("label added")
            }else{
                self.noDataMessageLabel?.hidden = false
            }
        }
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: MyOrderTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MyOrderTableViewCell
        cell.delegate = self
        cell.selectionStyle = .None

        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        let order_status: String = cellData.objectForKey("status") as! String
        //print(order_status)
        //cell.trackOrderButton.setTitle(getStatusTextFromCode(order_status), forState: UIControlState.Normal)
        if prefs.objectForKey("user_type") as? String == "driver" && ["REQ", "REV", "APPR", "AD"].contains(order_status){
            cell.acceptOrder.hidden = false
        }else{
            cell.acceptOrder.hidden = true
        }
        
        // show acceptOrder button which will act as details button for customer & store_owner
        if prefs.objectForKey("user_type") as? String != "driver"{
            cell.acceptOrder.hidden = false
            cell.acceptOrder.setTitle(NSLocalizedString("Label_Show_Detail", comment: "Show details"), forState: UIControlState.Normal)
        }
        
        if prefs.objectForKey("user_type") as? String == "driver"{
            cell.trackOrderButton.setTitle(NSLocalizedString("Label_Show_Detail", comment: "Show details"), forState: UIControlState.Normal)
        }
        if prefs.objectForKey("user_type") as? String != "driver" && ["REQ", "REV", "APPR"].contains(order_status){
            cell.cancelOrderButton.hidden = false
        }else{
            cell.cancelOrderButton.hidden = true
        }
        
        let formattedString: NSMutableAttributedString = NSMutableAttributedString()
        if let orederID: String = cellData.objectForKey("display_id") as? String{
            let newStr = NSAttributedString(string: "#\(orederID)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)])
            formattedString.appendAttributedString(newStr)
        }
        if let pickup: String = cellData.objectForKey("pickup") as? String{
            var pickupStr: String = NSLocalizedString("Label_Pickup_Time", comment: "Delivery time")
            
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let datetime: NSDate = dateFormatter.dateFromString(pickup){
                //print(datetime)
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                }
                pickupStr += ": " + dateFormatter.stringFromDate(datetime)
                //print(dateFormatter.stringFromDate(datetime))
            }
            
            let newStr = NSAttributedString(string: "\(pickupStr)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            formattedString.appendAttributedString(newStr)
        }
        
        if let items: NSArray = cellData.objectForKey("items") as? NSArray where items.count > 0{
            for item in (items as! [NSDictionary]){
                var itemName: String = String()
                if let item_name: String = item.objectForKey("item_name") as? String{
                    itemName = item_name
                }
                
                var priceStr: String = String()
                if let price: NSNumber = item.objectForKey("price") as? NSNumber{
                    priceStr = NSString(format: "%.2f", price.floatValue) as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        priceStr = numberFormatter.stringFromNumber(price)!
                    }
                }
                
                var quantityStr: String = String()
                if let quantity: NSString = item.objectForKey("quantity") as? NSString{
                    quantityStr = quantity as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity.integerValue)!
                    }
                }
                
                if let quantity: NSNumber = item.objectForKey("quantity") as? NSNumber{
                    quantityStr = quantity.stringValue
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity)!
                    }
                }
                
                var storeName: String = String()
                if let store_name: String = item.objectForKey("store_name") as? String{
                    storeName = store_name
                }
                
                let currencyCode: String = NSLocalizedString("Label_Currency_Code", comment: "SAR")
                let fromStr: String = NSLocalizedString("Label_Order_From_Store", comment: "From")
                
                var newStr = NSAttributedString(string: "\(itemName) (\(priceStr) \(currencyCode)) x \(quantityStr) \(fromStr) \(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    newStr = NSAttributedString(string: "\u{200F}\(itemName) (\(priceStr) \(currencyCode)) \u{200F}x \(quantityStr) \(fromStr) \u{200F}\(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                }
                
                formattedString.appendAttributedString(newStr)
            }
        }else if let additional_notes: String = cellData.objectForKey("additional_notes") as? String{
            let newStr: NSAttributedString = NSAttributedString(string: "\(additional_notes)\n")
            formattedString.appendAttributedString(newStr)
        }
        
        if let total: NSNumber = cellData.objectForKey("total") as? NSNumber where total.floatValue > 0{
            var newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(total)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(numberFormatter.stringFromNumber(total)!)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            }
            formattedString.appendAttributedString(newStr)
        }
        
        if let paymentMethods: [NSDictionary] = (UIApplication.sharedApplication().delegate as? AppDelegate)?._paymentMethods{
            for dict in paymentMethods{
                if (dict["id"] as? NSNumber)?.integerValue == (cellData["payment_method"] as? NSNumber)?.integerValue{
                    formattedString.appendAttributedString(NSAttributedString(string: NSLocalizedString("Label_Payment_Option", comment: "Payment Option") + ": " + (dict["name"] as! String), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)]))
                    break
                }
            }
        }
        
        var addressText: String = ""
        let addressTextArray: NSMutableArray = NSMutableArray()
        
        if let locationDict: NSDictionary = cellData.objectForKey("user_location") as? NSDictionary{
            if let name: String = locationDict.objectForKey("name") as? String where name != ""{
                addressTextArray.addObject(name)
            }
            if let houseno: String = locationDict.objectForKey("house_no") as? String where houseno != ""{
                addressTextArray.addObject(NSLocalizedString("Label_House_No", comment: "House no.") + " " + houseno)
            }
            if let street: String = locationDict.objectForKey("street") as? String where street != ""{
                addressTextArray.addObject(street)
            }
            
            addressText = (NSArray(array: addressTextArray) as! Array).joinWithSeparator(", ")
            
            if let region: String = locationDict.objectForKey("region") as? String where region != ""{
                //print(region)
                addressText = addressText + "\n" + region
            }
            if let phone: String = locationDict.objectForKey("phone") as? String where phone != ""{
                addressText = addressText + "\n" + NSLocalizedString("Label_Phone", comment: "Phone no") + ": " + phone
            }
            if let note: String = locationDict.objectForKey("note") as? String where note != ""{
                addressText = addressText + "\n" + note
            }
            
            formattedString.appendAttributedString(NSAttributedString(string: "\n" + NSLocalizedString("Label_Order_Delivery_Address", comment: "Order Details") + ":\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)]))
            formattedString.appendAttributedString(NSAttributedString(string: addressText.stringByReplacingOccurrencesOfString("^\\n*", withString: "", options: .RegularExpressionSearch), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)]))
        }
        
        cell.textView.textAlignment = .Natural
        cell.textView.attributedText = formattedString
        cell.textView.textColor = UIColor(rgba: "8B837B")
        dispatch_async(dispatch_get_main_queue(), {
            cell.textView.contentOffset = CGPointZero
        })
        
        if prefs.objectForKey("user_type") as? String == "driver" && cellData.objectForKey("accepted") as? Bool == false{
            cell.acceptOrder.hidden = false
        }

        return cell
    }
    
    // MARK: - MyOrderTableViewCellDelegate
    func onTrackTap(cell: MyOrderTableViewCell){
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            if prefs.objectForKey("user_type") as? String == "driver"{
                let vc: DriverOrderTrackViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DriverOrderTrackViewController") as! DriverOrderTrackViewController
                vc.orderData = cellData as! NSMutableDictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc: TrackOrderViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TrackOrderViewController") as! TrackOrderViewController
                vc.order_id = cellData.objectForKey("id") as! NSNumber
                if let orederID: String = cellData.objectForKey("display_id") as? String{
                    vc.navigationItem.title = "#\(orederID)"
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func onCancelOrderTap(cell: MyOrderTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Cancel_Order_Message", comment: "Cancel order"), buttonNames: [NSLocalizedString("Label_Cancel", comment: "Cancel"), NSLocalizedString("AM_Okay", comment: "Okay")], completion: { (index) -> Void in
                if index == 1{
                    let url: String = "api/orders/" + (cellData.objectForKey("id") as! NSNumber).stringValue + "/"
                    let postData: NSDictionary = ["status": "UCAN"]
                    print(postData)
                    self.activityView.showActivityIndicator()
                    DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postData, completion: { (data, error) -> Void in
                        self.activityView.hideActivityIndicator()
                        if data != nil{
                            //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            do{
                                let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                                if json.objectForKey("status") as? Bool == false{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                }else{
                                    self.tableData.removeObjectAtIndex(indexPath.row)
                                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                                }
                            }catch{
                                print(error)
                                print("Unknow error")
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                            }
                        }else{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                        }
                    })
                }
            })
        }
    }
    
    func onAcceptOrderTap(cell: MyOrderTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            
            // acceptOrder button will act as details button for customer & store_owner
            if prefs.objectForKey("user_type") as? String != "driver"{
                let vc: OrderDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetailViewController") as! OrderDetailViewController
                vc.orderData = cellData
                if let orederID: String = cellData.objectForKey("display_id") as? String{
                    vc.navigationItem.title = "#\(orederID)"
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let url: String = "api/orders/" + (cellData.objectForKey("id") as! NSNumber).stringValue + "/"
                let postData: NSDictionary = ["status": "DOTW", "driver": prefs.objectForKey("id") as! NSNumber]
                print(postData)
                activityView.showActivityIndicator()
                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postData, completion: { (data, error) -> Void in
                    self.activityView.hideActivityIndicator()
                    if data != nil{
                        //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                        do{
                            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                            if json.objectForKey("status") as? Bool == false{
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                            }else{
                                cell.acceptOrder.hidden = true
                            }
                        }catch{
                            print(error)
                            print("Unknow error")
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                        }
                    }else{
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                    }
                })
            }
        }
    }
    
    // MARK: - loadTableData
    func loadTableData(){
        let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var url: String = "api/orders/?exclude_delivered=true&exclude_cancelled=true&include_address=true"
        
        if prefs.objectForKey("user_type") as? String == "store_owner"{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "&store=" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "&include_user=" + user_id
        }else if prefs.objectForKey("user_type") as? String == "driver"{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "&driver=" + user_id
        }else{
            let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
            url = url + "&user=" + user_id
        }
        if tableData.count == 0{
            activityView.showActivityIndicator()
        }
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    //print(json)
                    self.tableData = NSMutableArray(array: json)
                    self.tableView.reloadData()
                    
                    //self.tableView.beginUpdates()
                    //self.tableView.reloadRowsAtIndexPaths(self.tableView.indexPathsForVisibleRows!, withRowAnimation: UITableViewRowAnimation.Automatic)
                    //self.tableView.endUpdates()
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
    }
    
    // MARK: - refresh
    func refresh(refreshControl: UIRefreshControl) {
        loadTableData()
        refreshControl.endRefreshing()
    }

}
