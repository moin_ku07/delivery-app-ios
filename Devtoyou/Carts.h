//
//  Carts.h
//  Devtoyou
//
//  Created by Moin Uddin on 10/3/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Carts : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Carts+CoreDataProperties.h"
