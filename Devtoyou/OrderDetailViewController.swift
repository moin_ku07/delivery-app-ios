//
//  OrderDetailViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 2/14/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {
    
    var orderData: NSDictionary!
    var driverData: NSDictionary?

    @IBOutlet var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let driverID: NSNumber = orderData.objectForKey("driver") as? NSNumber{
            let url: String = "api/drivers/\(driverID)/"
            DataManager.loadDataAsyncWithCallback(url, completion: { (data, error) -> Void in
                if data != nil{
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    do{
                        let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        print(json)
                        self.driverData = json
                        self.updateTextView()
                    }catch{
                        print("JSON Parse error \((error as NSError).localizedDescription)")
                    }
                }
            })
        }else{
            updateTextView()
        }
    }
    
    func updateTextView(){
        let formattedString: NSMutableAttributedString = NSMutableAttributedString()
        if let orederID: String = orderData.objectForKey("display_id") as? String{
            let newStr = NSAttributedString(string: "#\(orederID)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)])
            formattedString.appendAttributedString(newStr)
        }
        if let pickup: String = orderData.objectForKey("pickup") as? String{
            var pickupStr: String = NSLocalizedString("Label_Pickup_Time", comment: "Delivery time")
            
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let datetime: NSDate = dateFormatter.dateFromString(pickup){
                //print(datetime)
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                }
                pickupStr += ": " + dateFormatter.stringFromDate(datetime)
            }
            
            let newStr = NSAttributedString(string: "\(pickupStr)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            formattedString.appendAttributedString(newStr)
        }
        
        if let items: NSArray = orderData.objectForKey("items") as? NSArray where items.count > 0{
            let numberFormatter: NSNumberFormatter = NSNumberFormatter()
            numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
            for item in (items as! [NSDictionary]){
                var itemName: String = String()
                if let item_name: String = item.objectForKey("item_name") as? String{
                    itemName = item_name
                }
                
                var priceStr: String = String()
                if let price: NSNumber = item.objectForKey("price") as? NSNumber{
                    priceStr = NSString(format: "%.2f", price.floatValue) as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        priceStr = numberFormatter.stringFromNumber(price)!
                    }
                }
                
                var quantityStr: String = String()
                if let quantity: NSString = item.objectForKey("quantity") as? NSString{
                    quantityStr = quantity as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity.integerValue)!
                    }
                }
                
                if let quantity: NSNumber = item.objectForKey("quantity") as? NSNumber{
                    quantityStr = quantity.stringValue
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity)!
                    }
                }
                
                var storeName: String = String()
                if let store_name: String = item.objectForKey("store_name") as? String{
                    storeName = store_name
                }
                
                let currencyCode: String = NSLocalizedString("Label_Currency_Code", comment: "SAR")
                let fromStr: String = NSLocalizedString("Label_Order_From_Store", comment: "From")
                
                var newStr = NSAttributedString(string: "\(itemName) (\(priceStr) \(currencyCode)) x \(quantityStr) \(fromStr) \(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    newStr = NSAttributedString(string: "\u{202E}\(itemName) (\(priceStr) \(currencyCode)) \u{202E}x \(quantityStr) \(fromStr) \u{202E}\(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                }
                formattedString.appendAttributedString(newStr)
            }
        }else if let additional_notes: String = orderData.objectForKey("additional_notes") as? String{
            let newStr: NSAttributedString = NSAttributedString(string: "\(additional_notes)\n")
            formattedString.appendAttributedString(newStr)
        }
        
        if let total: NSNumber = orderData.objectForKey("total") as? NSNumber where total.floatValue > 0{
            let newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(total)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            formattedString.appendAttributedString(newStr)
        }
        
        if let paymentMethods: [NSDictionary] = (UIApplication.sharedApplication().delegate as? AppDelegate)?._paymentMethods{
            for dict in paymentMethods{
                if (dict["id"] as? NSNumber)?.integerValue == (orderData["payment_method"] as? NSNumber)?.integerValue{
                    formattedString.appendAttributedString(NSAttributedString(string: NSLocalizedString("Label_Payment_Option", comment: "Payment Option") + ": " + (dict["name"] as! String), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)]))
                    break
                }
            }
        }
        
        if driverData != nil{
            var driverAddressText: String = ""
            
            var fullName: String = ""
            if let first_name: String = driverData!.objectForKey("first_name") as? String where first_name != "" || first_name != "N/A"{
                fullName = first_name
            }
            if let last_name: String = driverData!.objectForKey("last_name") as? String where last_name != "" || last_name != "N/A"{
                fullName = fullName != "" ? fullName + " \(last_name)" : last_name
            }
            
            driverAddressText = fullName
            
            if let phone: String = driverData!.objectForKey("username") as? String where phone != ""{
                driverAddressText = driverAddressText + "\n" + NSLocalizedString("Label_Phone", comment: "Phone no") + ": " + phone
            }
            
            print(driverAddressText)
            
            formattedString.appendAttributedString(NSAttributedString(string: "\n" + NSLocalizedString("Label_Driver_Detail", comment: "Driver Details") + ":\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)]))
            formattedString.appendAttributedString(NSAttributedString(string: driverAddressText.stringByReplacingOccurrencesOfString("^\\n*", withString: "", options: .RegularExpressionSearch) + "\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)]))
        }
        
        var addressText: String = ""
        let addressTextArray: NSMutableArray = NSMutableArray()
        
        if let locationDict: NSDictionary = orderData.objectForKey("user_location") as? NSDictionary{
            if let name: String = locationDict.objectForKey("name") as? String where name != ""{
                addressTextArray.addObject(name)
            }
            if let houseno: String = locationDict.objectForKey("house_no") as? String where houseno != ""{
                addressTextArray.addObject(NSLocalizedString("Label_House_No", comment: "House no.") + " " + houseno)
            }
            if let street: String = locationDict.objectForKey("street") as? String where street != ""{
                addressTextArray.addObject(street)
            }
            
            addressText = (NSArray(array: addressTextArray) as! Array).joinWithSeparator(", ")
            
            if let region: String = locationDict.objectForKey("region") as? String where region != ""{
                //print(region)
                addressText = addressText + "\n" + region
            }
            if let phone: String = locationDict.objectForKey("phone") as? String where phone != ""{
                addressText = addressText + "\n" + NSLocalizedString("Label_Phone", comment: "Phone no") + ": " + phone
            }
            if let note: String = locationDict.objectForKey("note") as? String where note != ""{
                addressText = addressText + "\n" + note
            }
            
            formattedString.appendAttributedString(NSAttributedString(string: "\n" + NSLocalizedString("Label_Order_Delivery_Address", comment: "Order Details") + ":\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)]))
            formattedString.appendAttributedString(NSAttributedString(string: addressText.stringByReplacingOccurrencesOfString("^\\n*", withString: "", options: .RegularExpressionSearch), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)]))
        }
        
        textView.textAlignment = .Natural
        textView.attributedText = formattedString
        textView.textColor = UIColor(rgba: "8B837B")
        dispatch_async(dispatch_get_main_queue(), {
            self.textView.contentOffset = CGPointZero
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
