//
//  UIActivityView.swift
//  MyLedger
//
//  Created by Moin Uddin on 11/25/14.
//  Copyright (c) 2014 Moin Uddin. All rights reserved.
//

import Foundation
import UIKit

class UICustomActivityView: UIView {
    
    var container: UIView = UIView()
    var containerColor: UIColor? = nil
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var message: String? = nil
    var messageLabel: UILabel = UILabel()
    var textColor: UIColor? = nil
    var activityColor: UIColor? = nil
    var activityBackground: UIColor? = nil
    
    /*
    Show customized activity indicator,
    actually add activity indicator to passing view
    
    @param uiView - add activity indicator to this view
    */
    
    internal init(activityColor: UIColor? = nil, activityBackground: UIColor? = nil, containerColor: UIColor? = nil, textColor: UIColor? = nil){
        super.init(frame: CGRectMake(0, 0, 0, 0))
        self.activityColor = activityColor
        self.activityBackground = activityBackground
        self.containerColor = containerColor
        self.textColor = textColor
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func initActivityIndicator(uiView: AnyObject, style: UIActivityIndicatorViewStyle? = nil, shouldHaveContainer: Bool? = true, centerPoint: CGPoint? = nil) {
        if centerPoint != nil{
            container.center = centerPoint!
        }
        container.frame = uiView.frame
        container.backgroundColor = UIColorFromHex(0xffffff, alpha: 0.3)
        
        if containerColor != nil{
            container.backgroundColor = containerColor!
        }
        if loadingView.frame == CGRectMake(0, 0, 0, 0){
            loadingView.frame = CGRectMake(0, 0, 80, 80)
            if message != nil{
                loadingView.frame = CGRectMake(0, 0, 150, 150)
            }
        }
        if centerPoint != nil{
            loadingView.center = centerPoint!
        }
        loadingView.backgroundColor = UIColorFromHex(0x000000, alpha: 0.8)
        if activityBackground != nil{
            loadingView.backgroundColor = activityBackground!
        }
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0)
        var activitStyle: UIActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        
        if style != nil{
            activitStyle = style!
        }
        //println(activitStyle.rawValue)
        activityIndicator.activityIndicatorViewStyle = activitStyle
        if activityColor != nil{
            activityIndicator.color = activityColor!
        }
        
        if shouldHaveContainer == true || message != nil{
            if message != nil && style == nil{
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
            }
            if loadingView.frame.size.width == 80 && loadingView.frame.size.height == 80{
                activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2)
            }else{
                activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, (loadingView.frame.size.height - 30) / 2)
            }
            loadingView.addSubview(activityIndicator)
            messageLabel.frame = CGRectMake(0,loadingView.frame.size.height - 30,loadingView.frame.size.width,20)
            messageLabel.textAlignment = .Center
            //messageLabel.font = UIFont.systemFontOfSize(15)
            messageLabel.text = message
            messageLabel.textColor = UIColor.whiteColor()
            if textColor != nil{
                messageLabel.textColor = textColor!
            }
            //label.setTranslatesAutoresizingMaskIntoConstraints(false)
            loadingView.addSubview(messageLabel)
            
            container.addSubview(loadingView)
            if centerPoint == nil{
                loadingView.translatesAutoresizingMaskIntoConstraints = false
                container.addConstraint(NSLayoutConstraint(item: loadingView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: loadingView.frame.width))
                container.addConstraint(NSLayoutConstraint(item: loadingView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: loadingView.frame.height))
                container.addConstraint(NSLayoutConstraint(item: loadingView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: container, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
                
                container.addConstraint(NSLayoutConstraint(item: loadingView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: container, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
            }
        }else{
            activityIndicator.center = container.center
            container.addSubview(activityIndicator)
        }
        container.hidden = true
        uiView.addSubview(container)
        if centerPoint == nil{
            container.translatesAutoresizingMaskIntoConstraints = false
            
            uiView.addConstraint(NSLayoutConstraint(item: container, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: uiView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0))
            uiView.addConstraint(NSLayoutConstraint(item: container, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: uiView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0))
            
            uiView.addConstraint(NSLayoutConstraint(item: container, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: uiView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
            uiView.addConstraint(NSLayoutConstraint(item: container, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: uiView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0))
            
            uiView.addConstraint(NSLayoutConstraint(item: container, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: uiView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
            uiView.addConstraint(NSLayoutConstraint(item: container, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: uiView, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
        }
        activityIndicator.startAnimating()
    }
    
    // MARK: - Show acitivity indicator
    
    func showActivityIndicator() {
        activityIndicator.startAnimating()
        container.hidden = false
    }
    
    // MARK: - Hide activity indicator
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.hidden = true
    }
    
    // MARK: - remove activity indicator from this view
    func removeActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    /*
    Define UIColor from hex value
    
    @param rgbValue - hex color value
    @param alpha - transparency level
    */
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}