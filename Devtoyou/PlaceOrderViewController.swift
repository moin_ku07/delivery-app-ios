//
//  PlaceOrderViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/4/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RadioButton
import ActionSheetPicker_3_0
import CoreLocation

class PlaceOrderViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, AddAddressMapViewControllerDelegate {
    
    var addresses: NSMutableArray = NSMutableArray()
    var user_id: String!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var orderTextView: UITextView!
    @IBOutlet var addressBox: UIView!
    var selectedLocationId: Int?
    @IBOutlet var addressBox2: UIView!
    @IBOutlet var csNameField: UITextField!
    @IBOutlet var csMobileField: UITextField!
    @IBOutlet var csDistrictField: UITextField!
    @IBOutlet var csLocationField: UITextField!
    @IBOutlet var csHouseNo: UITextField!
    @IBOutlet var csDoorColor: UITextField!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var timeBox: UIView!
    @IBOutlet var timeLabel: UILabel!
    var pickupTime: NSDate?
    @IBOutlet var paymentBox: UIView!
    @IBOutlet var radioCash: RadioButton!
    @IBOutlet var radioCredit: RadioButton!
    @IBOutlet var radioPoint: RadioButton!
    @IBOutlet var radioBank: RadioButton!
    @IBOutlet var radioSpan: RadioButton!
    var payment_method: Int = 1
    
    let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext()
    var rightBarButton: UIBarButtonItem!
    var activityView: UICustomActivityView!
    var selectedTextField: UITextField?
    var previousScrollOffset: CGPoint!
    
    var cartArray: [Carts] = [Carts]()
    var total: Float = 0
    var itemsArray: NSMutableArray = NSMutableArray()
    
    @IBOutlet var tableView: UITableView!
    var tableData: [NSDictionary] = [NSDictionary]()
    let tableRowHeight: CGFloat = 30.0
    var selectedIndexPath: NSIndexPath?
    
    var selectedDistrict: Int?
    var districtIDs: [Int] = [Int]()
    var districtNames: [String] = [String]()
    
    var prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        orderTextView.delegate = self
        
        if cartArray.count == 0{
            self.navigationItem.title = NSLocalizedString("Label_Checkout_Title", comment: "Checkout")
        }else{
            self.navigationItem.title = NSLocalizedString("Label_New_Order", comment: "New Order")
        }
        self.navigationController?.navigationBar.topItem!.title = ""
        
        addressBox.layer.borderWidth = 1
        addressBox.layer.borderColor = UIColor(rgba: "bebcae").CGColor
        
        timeBox.layer.borderWidth = 1
        timeBox.layer.borderColor = UIColor(rgba: "bebcae").CGColor
        
        radioCash.selected = true
        radioCash.groupButtons = [radioCredit, radioPoint, radioBank, radioSpan]
        radioCash.addTarget(self, action: #selector(onPaymentOptionChange(_:)), forControlEvents: UIControlEvents.ValueChanged)
        radioCash.tag = 1
        radioCredit.addTarget(self, action: #selector(onPaymentOptionChange(_:)), forControlEvents: UIControlEvents.ValueChanged)
        radioCredit.tag = 2
        radioPoint.addTarget(self, action: #selector(onPaymentOptionChange(_:)), forControlEvents: UIControlEvents.ValueChanged)
        radioPoint.tag = 3
        radioBank.addTarget(self, action: #selector(onPaymentOptionChange(_:)), forControlEvents: UIControlEvents.ValueChanged)
        radioBank.tag = 4
        radioSpan.addTarget(self, action: #selector(onPaymentOptionChange(_:)), forControlEvents: UIControlEvents.ValueChanged)
        radioSpan.tag = 5
        
        if prefs.objectForKey("user_type") as? String == "store_owner"{
            addressBox.removeFromSuperview()
            addressBox2.hidden = false
        }
        
        for view in addressBox2.subviews{
            if let textField: UITextField = view as? UITextField{
                addBorderLayer(textField, edge: UIRectEdge.Bottom, color: UIColor(rgba: "9c9a8f"), thickness: 1.0)
                textField.delegate = self
                textField.returnKeyType = UIReturnKeyType.Next
            }
        }
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onAddressBoxTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        addressBox.userInteractionEnabled = true
        addressBox.addGestureRecognizer(tapGesture)
        
        let timeTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTimeBoxTap(_:)))
        timeTapGesture.numberOfTapsRequired = 1
        timeBox.userInteractionEnabled = true
        timeBox.addGestureRecognizer(timeTapGesture)
        
        let scrollTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onScrollViewTap(_:)))
        scrollTapGesture.numberOfTapsRequired = 1
        //scrollView.addGestureRecognizer(scrollTapGesture)
        
        cartArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc) as! [Carts]
        if cartArray.count > 0{
            total = 0
            for cart in cartArray{
                total += cart.quantity!.floatValue * cart.price!.floatValue
                let dict: NSDictionary = ["store_item": cart.item_id!.integerValue, "quantity": cart.quantity!.integerValue, "price": cart.price!.floatValue]
                itemsArray.addObject(dict)
            }
        }else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Label_Save", comment: "Save"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onSaveButtonTap(_:)))
            AlertManager.showAlert(self, title: nil, message: NSLocalizedString("Label_Type_Order_Details", comment: "Type custom order text"), buttonNames: nil, completion: nil)
        }
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.doLocalisation()
        }
        
        fetchAddressList()
        
        if let paymentMethods: [NSDictionary] = (UIApplication.sharedApplication().delegate as! AppDelegate)._paymentMethods where paymentMethods.count > 0{
            self.tableData = paymentMethods
            for (index, dict) in paymentMethods.enumerate(){
                if (dict["id"] as? NSNumber)?.integerValue == self.payment_method{
                    self.selectedIndexPath = NSIndexPath(forRow: index, inSection: 0)
                    break
                }
            }
            
            for cns in self.tableView.constraints{
                if cns.identifier == "heightCNS"{
                    self.tableView.removeConstraint(cns)
                    break
                }
            }
            let cns: NSLayoutConstraint = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: CGFloat(self.tableData.count) * self.tableRowHeight)
            cns.priority = 1000
            cns.identifier = "heightCNS"
            //print(cns)
            
            self.scrollView.needsUpdateConstraints()
            self.paymentBox.removeFromSuperview()
            
            self.tableView.addConstraint(cns)
            self.scrollView.layoutIfNeeded()
            
            self.tableView.reloadData()
            self.tableView.hidden = false
            //self.tableView.backgroundColor = UIColor.redColor()
            self.scrollView.bringSubviewToFront(self.tableView)
        }
    }
    
    func doLocalisation(){
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            orderTextView.textAlignment = .Right
            let buttons: [RadioButton] = [radioCash, radioCredit, radioPoint, radioBank, radioSpan]
            for btn in buttons{
                btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
                if UIDevice.currentDevice().systemVersion.hasPrefix("8"){
                    btn.titleEdgeInsets = UIEdgeInsetsMake(0, -btn.imageView!.frame.size.width, 0, btn.imageView!.frame.size.width + 10)
                    btn.imageEdgeInsets = UIEdgeInsetsMake(0, (btn.titleLabel!.frame.size.width + 10), 0, -btn.titleLabel!.frame.size.width)
                }else{
                    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
                    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                }
            }
            let textFields: [UITextField] = [csNameField, csMobileField, csDistrictField, csLocationField, csHouseNo, csDoorColor]
            for textField in textFields{
                textField.textAlignment = .Right
            }
            if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
                
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if cartArray.count > 0{
            self.navigationItem.title = NSLocalizedString("Label_Checkout_Title", comment: "Checkout")
        }else{
            self.navigationItem.title = NSLocalizedString("Label_New_Order", comment: "New Order")
            if let customOrder: NSDictionary = prefs.objectForKey("customOrder") as? NSDictionary{
                if let additional_notes: String = customOrder.objectForKey("additional_notes") as? String{
                    orderTextView.text = additional_notes
                }
                if let payment_method: Int = customOrder.objectForKey("additional_notes") as? Int{
                    self.payment_method = payment_method
                }
                if let pickup: NSDate = customOrder.objectForKey("pickup") as? NSDate{
                    pickupTime = pickup
                    let dateFormatter: NSDateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                    }
                    self.timeLabel.text = dateFormatter.stringFromDate(self.pickupTime!)
                }
                if let user_location: Int = customOrder.objectForKey("user_location") as? Int{
                    selectedLocationId = user_location
                    for address in addresses{
                        if let dict: NSDictionary = address as? NSDictionary{
                            if dict.objectForKey("id") as? Int == selectedLocationId{
                                self.addressLabel.text = dict.objectForKey("name") as? String
                                break
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //(self.tabBarController as! TabBarController).setSearchAsRightBarButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func fetchAddressList(){
        DataManager.loadDataAsyncWithCallback("api/districts/") { (data, error) in
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do{
                    let jsonArray: [NSDictionary] = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! [NSDictionary]
                    print(jsonArray)
                    self.districtIDs.removeAll()
                    self.districtNames.removeAll()
                    for dis in jsonArray{
                        self.districtIDs.append((dis.objectForKey("id") as! NSNumber).integerValue)
                        self.districtNames.append(dis.objectForKey("name") as! String)
                    }
                    if self.districtIDs.count > 0{
                        if self.selectedDistrict != nil && self.districtIDs.count > self.selectedDistrict{
                            self.csDistrictField.text = self.districtNames[self.selectedDistrict!]
                        }else{
                            self.csDistrictField.text = self.districtNames[0]
                            self.selectedDistrict = self.districtIDs[0]
                        }
                    }
                }catch{
                    print((error as NSError).localizedDescription)
                }
            }
        }
    }
    
    func onDistrictFieldTap(){
        if districtNames.count > 0{
            let popup: DVAlertViewController = DVAlertViewController(parentController: self, popoverVC: nil, style: DVAlertViewControllerStyle.Popup, contentSize: CGSizeMake(200, 150))
            popup.tableData = districtNames
            popup.shouldRemoveControl = true
            if let district: String = self.csDistrictField.text where district != ""{
                if districtNames.contains(district){
                    if let index: Int = districtNames.indexOf(district){
                        popup.selectedIndex = index
                    }
                }
            }
            popup.doneBlock = {(index: Int?)-> Void in
                if index != nil{
                    self.csDistrictField.text = self.districtNames[index!]
                    self.selectedDistrict = self.districtIDs[index!]
                }
            }
            popup.show()
        }else{
            AlertManager.showAlert(self, title: "Warning", message: "There was no districts. Please contact support to inform", buttonNames: nil, completion: nil)
        }
    }
    
    // MARK: - onTextViewDoneTap
    func onTextFieldDoneTap(){
        if selectedTextField != nil{
            focusNextField(selectedTextField!)
        }
    }
    
    // MARK: focusNextField
    func focusNextField(textField: UITextField){
        let nextTag: Int = textField.tag + 1
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        selectedTextField = textField
        
        if textField === csLocationField{
            onScrollViewTap(UITapGestureRecognizer())
            let vc: AddAddressMapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAddressMapViewController") as! AddAddressMapViewController
            vc.delegate = self
            if csLocationField.text != ""{
                if let locArray: NSArray = csLocationField.text?.componentsSeparatedByString(",") where locArray.count == 2{
                    let lat: Double = (locArray.firstObject as! NSString).doubleValue
                    let lng: Double = (locArray.lastObject as! NSString).doubleValue
                    let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    vc.addressCoordinate = location
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
            return false
        }else if textField == csDistrictField{
            self.onDistrictFieldTap()
            return false
        }
        
        previousScrollOffset = scrollView.contentOffset
        let textFieldPos: CGPoint = textField.convertPoint(textField.frame.origin, fromView: scrollView)
        //println(abs(textFieldPos.y))
        if self.view.bounds.height - abs(textFieldPos.y) < 290{
            let yPost: CGFloat = abs(textFieldPos.y) - (self.view.bounds.height - 290 - textField.frame.origin.y)
            let scrollPoint: CGPoint = CGPointMake(0, yPost)
            //println(scrollPoint)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
        if textField.keyboardType == UIKeyboardType.PhonePad{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            item = UIBarButtonItem(title: NSLocalizedString("Label_Next", comment: "Next"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onTextFieldDoneTap) )
            
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        focusNextField(textField)
        
        return false
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        // Create a button bar for the number pad
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        
        // Setup the buttons to be put in the system.
        var item: UIBarButtonItem = UIBarButtonItem()
        item = UIBarButtonItem(title: NSLocalizedString("Label_Done", comment: "Done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onTextViewDoneTap) )
        
        let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let toolbarButtons = [flexSpace,item]
        
        //Put the buttons into the ToolBar and display the tool bar
        keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
        textView.inputAccessoryView = keyboardDoneButtonView
        
        return true
    }
    
    func onTextViewDoneTap(){
        orderTextView.resignFirstResponder()
    }
    
    // MARK: - onSaveButtonTap
    func onSaveButtonTap(sender: UIBarButtonItem){
        let customOrder: NSMutableDictionary = ["additional_notes": orderTextView.text!, "payment_method": payment_method]
        if pickupTime != nil{
            customOrder.setValue(pickupTime, forKey: "pickup")
        }
        
        if prefs.objectForKey("user_type") as? String == "customer" && selectedLocationId != nil{
            customOrder.setValue(self.selectedLocationId!, forKey: "user_location")
        }else if prefs.objectForKey("user_type") as? String == "store_owner"{
            if let locArray: NSArray = csLocationField.text?.componentsSeparatedByString(",") where locArray.count == 2{
                let lat: Double = (locArray.firstObject as! NSString).doubleValue
                let lng: Double = (locArray.lastObject as! NSString).doubleValue
                let addressDict: NSMutableDictionary = ["name": csNameField.text!, "phone": csMobileField.text!, "region": csDistrictField.text!, "gps": ["lat": lat, "lng": lng], "house_no": csHouseNo.text!, "door_color": "", "street": "", "door_no": ""]
                if csDoorColor.text != ""{
                    addressDict.setValue(csDoorColor.text!, forKey: "door_color")
                }
                customOrder.setValue(addressDict, forKey: "delivery_address")
            }
        }
        prefs.setObject(customOrder, forKey: "customOrder")
        prefs.synchronize()
        
        AlertManager.showAlert(self, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Draft_Save", comment: "success"), buttonNames: nil, completion: nil)
    }
    
    // MARK: - onCartTap
    func onCartTap(sender: UIBarButtonItem){
        let vc: CartTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CartTableViewController") as! CartTableViewController
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - AddAddressMapViewControllerDelegate
    func onLocationMarked(location: CLLocationCoordinate2D) {
        csLocationField.text = "\(location.latitude), \(location.longitude)"
    }
    
    func onPaymentOptionChange(sender: RadioButton){
        if sender.selected{
            print("payment_method: \(sender.tag)")
            if sender.tag == 1{
                payment_method = 1
            }else if sender.tag == 2{
                payment_method = 2
            }else if sender.tag == 3{
                payment_method = 3
            }else if sender.tag == 4{
                payment_method = 4
            }else if sender.tag == 5{
                payment_method = 5
            }
        }
    }
    
    func onAddressBoxTap(recognizer: UITapGestureRecognizer){
        selectedTextField?.resignFirstResponder()
        selectedTextField = nil
        orderTextView.resignFirstResponder()
        
        var addressarray: [String] = [String]()
        for address in addresses{
            if let dict: NSDictionary = address as? NSDictionary{
                if let name: String = dict.objectForKey("name") as? String{
                    addressarray.append(name)
                }
            }
        }
        ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Address", comment: "Select Address"), rows: addressarray, initialSelection: 0, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
                self.addressLabel.text = object as? String
                self.selectedLocationId = ((self.addresses[selectedIndex] as! NSDictionary).objectForKey("id") as! NSNumber).integerValue
            }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                print("nothing")
            }, origin: self.addressBox)
    }
    
    func onTimeBoxTap(recognizer: UITapGestureRecognizer){
        selectedTextField?.resignFirstResponder()
        selectedTextField = nil
        orderTextView.resignFirstResponder()
        
        ActionSheetDatePicker.showPickerWithTitle(NSLocalizedString("Label_Select_Time", comment: "Select Time"), datePickerMode: UIDatePickerMode.DateAndTime, selectedDate: NSDate(), doneBlock: { (picker: ActionSheetDatePicker!, selectedDate: AnyObject!, sender: AnyObject!) -> Void in
                self.pickupTime = selectedDate as? NSDate
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                dateFormatter.locale = NSLocale(localeIdentifier: "ar")
            }
            self.timeLabel.text = dateFormatter.stringFromDate(self.pickupTime!)
            }, cancelBlock: { (picker: ActionSheetDatePicker!) -> Void in
                print("nothing")
            }, origin: self.timeBox)
    }

    @IBAction func onOrderNowTap(sender: UIButton) {
        let postData: NSMutableDictionary = ["items": NSArray(array: itemsArray), "status": "REQ", "additional_notes": orderTextView.text, "user": (user_id as NSString).integerValue, "driver": NSNull(), "total": total, "payment_method": payment_method]
        
        if cartArray.count == 0{
            if (orderTextView.text as NSString).length == 0{
                AlertManager.showAlert(self, title: nil, message: NSLocalizedString("Label_Type_Order_Details", comment: "Type custom order text"), buttonNames: nil, completion: nil)
                return
            }else{
                postData.setValue(NSNull(), forKey: "items")
                postData.setValue(55, forKey: "total")
            }
        }
        
        if prefs.objectForKey("user_type") as? String == "customer"{
            if self.selectedLocationId == nil{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_No_Location_Selected", comment: "Please select a delivery address"), buttonNames: nil, completion: nil)
                return
            }else{
                postData.setValue(self.selectedLocationId!, forKey: "user_location")
            }
        }else if prefs.objectForKey("user_type") as? String == "store_owner"{
            if csNameField.text == ""{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Order_CS_Name", comment: "Please enter customer name"), buttonNames: nil, completion: nil)
                return
            }else if csMobileField.text == "" || !FormValidation.isValidPhone(csMobileField.text!, minLength: 3, maxLength: 12, country: "KSA"){
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Order_CS_Phone", comment: "Please enter valid phone no"), buttonNames: nil, completion: nil)
                return
            }else if csDistrictField.text == ""{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Order_CS_District", comment: "Please enter district"), buttonNames: nil, completion: nil)
                return
            }else if csLocationField.text == "" || !FormValidation.isValidPhone(csMobileField.text!, minLength: 3, maxLength: 12, country: "KSA"){
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Order_CS_GPS", comment: "Please select a delivery location"), buttonNames: nil, completion: nil)
                return
            }else if csHouseNo.text == ""{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Order_CS_House", comment: "Please enter house no"), buttonNames: nil, completion: nil)
                return
            }else{
                if let locArray: NSArray = csLocationField.text?.componentsSeparatedByString(",") where locArray.count == 2{
                    let lat: Double = (locArray.firstObject as! NSString).doubleValue
                    let lng: Double = (locArray.lastObject as! NSString).doubleValue
                    let addressDict: NSMutableDictionary = ["name": csNameField.text!, "phone": csMobileField.text!, "region": csDistrictField.text!, "gps": ["lat": lat, "lng": lng], "house_no": csHouseNo.text!, "door_color": "", "street": "", "door_no": ""]
                    if selectedDistrict != nil{
                        addressDict.setValue(selectedDistrict!, forKey: "district")
                    }
                    if csDoorColor.text != ""{
                        addressDict.setValue(csDoorColor.text!, forKey: "door_color")
                    }
                    postData.setValue(addressDict, forKey: "delivery_address")
                }
            }
        }
        
        if pickupTime != nil{
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
            let pickupTimeString: String = dateFormatter.stringFromDate(pickupTime!)
            postData.setValue(pickupTimeString, forKey: "pickup")
        }
        do{
            let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions.init(rawValue: 0))
            print(NSString(data: jsonData, encoding: NSUTF8StringEncoding))
        }catch{
            print("Unknown error JSON decode")
        }
        
        self.activityView.showActivityIndicator()
        DataManager.postDataAsyncWithCallback("api/orders/", data: postData) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do {
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                    if let order_id: NSNumber = json.objectForKey("order_id") as? NSNumber{
                        let vc: OrderSuccessViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OrderSuccessViewController") as! OrderSuccessViewController
                        vc.orderID = order_id.stringValue
                        self.presentViewController(vc, animated: true) { () -> Void in
                            self.clearAndNavigateCart()
                            self.prefs.setObject(nil, forKey: "customOrder")
                            self.prefs.removeObjectForKey("customOrder")
                            self.prefs.synchronize()
                        }
                    }else if json.objectForKey("status") as? Bool == false{
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Fail", comment: "Order fail"), buttonNames: nil, completion: nil)
                        print(json)
                    }
                }catch{
                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Fail", comment: "Order fail"), buttonNames: nil, completion: nil)
                    print("Unknown error JSON decode")
                }
                
            }else{
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Fail", comment: "Order fail"), buttonNames: nil, completion: nil)
                print(error?.localizedDescription)
            }
        }
    }
    
    // MARK: - onScrollViewTap
    func onScrollViewTap(recognizer: UITapGestureRecognizer){
        orderTextView.resignFirstResponder()
        for view in addressBox2.subviews{
            if let textField: UITextField = view as? UITextField{
                textField.resignFirstResponder()
            }
        }
    }
    
    // MARK: - Clear Local Cart Items
    func clearAndNavigateCart() {
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext()
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc)
        for result in results{
            let cart: Carts = result as! Carts
            //print(cart)
            moc.deleteObject(cart)
        }
        do{
            try moc.save()
            print("All record deleted")
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 250 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), { () -> Void in
                self.navigationController?.popToRootViewControllerAnimated(false)
            })
        }catch{
            print(error)
        }
    }
    
}

extension PlaceOrderViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableRowHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: PlaceOrderPaymentTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! PlaceOrderPaymentTableViewCell
        cell.selectionStyle = .None
        
        let cellData: NSDictionary = tableData[indexPath.row]
        
        if selectedIndexPath == indexPath{
            cell.leftImage.image = UIImage(named: "icon-radio-checked")
        }else{
            cell.leftImage.image = UIImage(named: "icon-radio-unchecked")
        }
        
        cell.title.text = cellData["name"] as? String
        
        if cellData["enabled"] as? Bool == false{
            cell.title.textColor = UIColor(rgba: "cccccc")
        }else{
            cell.title.textColor = UIColor(rgba: "8D8C84")
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //print("didSelectRowAtIndexPath = \(indexPath)")
        if selectedIndexPath != nil{
            if let cell: PlaceOrderPaymentTableViewCell = tableView.cellForRowAtIndexPath(selectedIndexPath!) as? PlaceOrderPaymentTableViewCell{
                cell.leftImage.image = UIImage(named: "icon-radio-unchecked")
            }
        }
        selectedIndexPath = indexPath
        if let cell: PlaceOrderPaymentTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as? PlaceOrderPaymentTableViewCell{
            cell.leftImage.image = UIImage(named: "icon-radio-checked")
            payment_method = (tableData[indexPath.row].objectForKey("id") as! NSNumber).integerValue
            //print(payment_method)
        }
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        let cellData: NSDictionary = tableData[indexPath.row]
        if cellData["enabled"] as? Bool == false{
            return nil
        }
        return indexPath
    }
}
