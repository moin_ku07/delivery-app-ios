//
//  StoreItemsTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/1/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreData

class StoreItemsTableViewController: UITableViewController, UICollectionViewDataSource, UICollectionViewDelegate, StoreItemsTableViewCellDelegate {
    
    var store_id: NSNumber!
    var storeName: String!
    var laodedOnce: Bool = false
    var activity: UIActivityIndicatorView!
    var noDataMessageLabel: UILabel?
    @IBOutlet var collectionView: UICollectionView!
    var imageData: NSMutableArray = NSMutableArray()
    
    var tableData: NSMutableArray = NSMutableArray()
    var enableAddButton: Bool = false
    
    let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("Devtoyou")
    var navigationTitle: String?
    
    var addedCells: NSMutableArray = NSMutableArray()
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()
    
    var expandedIndexPaths: [NSIndexPath] = [NSIndexPath]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        //numberFormatter.minimumFractionDigits = 2
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self

        activity = UIActivityIndicatorView(frame: CGRectMake(0, 0, 20, 20))
        activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        activity.hidesWhenStopped = true
        activity.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(activity)
        
        let viewDict: [String: AnyObject] = ["view": view, "activity": activity]
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[view]-(<=1)-[activity(20)]", options: NSLayoutFormatOptions.AlignAllCenterY, metrics: nil, views: viewDict))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[view]-(<=1)-[activity(20)]", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: viewDict))
        
        if enableAddButton{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(onItemAddTap(_:)))
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationTitle != nil{
            self.navigationItem.title = navigationTitle!
        }
        
        self.loadTableData()
        loadCollectionViewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableData.count > 0{
            self.noDataMessageLabel?.hidden = true
        }else{
            if self.noDataMessageLabel == nil{
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                    self.tableView.bounds.size.height))
                
                self.noDataMessageLabel!.text = NSLocalizedString("Label_No_Item", comment: "No Item")
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.tableView.backgroundView = self.noDataMessageLabel!
                self.noDataMessageLabel?.hidden = true
                //print("label added")
            }else if laodedOnce{
                self.noDataMessageLabel?.hidden = false
            }
        }
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 85.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if expandedIndexPaths.contains(indexPath){
            return UITableViewAutomaticDimension
        }
        return 85.0
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let additionalSeparatorThickness = CGFloat(5)
        let additionalSeparator = UIView(frame: CGRectMake(0, cell.frame.size.height - additionalSeparatorThickness, cell.frame.size.width, additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor(rgba: "efecd9")
        cell.addSubview(additionalSeparator)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: StoreItemsTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! StoreItemsTableViewCell
        cell.delegate = self
        cell.selectionStyle = .None
        
        if cell.thumbnail.image == nil{
            cell.thumbActivity.startAnimating()
        }else{
            cell.thumbActivity.stopAnimating()
        }
        
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        if let item_id: NSNumber = cellData.objectForKey("id") as? NSNumber{
            //print(NSPredicate(format: "item_id=\(item_id)"))
            if let result: [Carts] = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: NSPredicate(format: "item_id=\(item_id)"), andSorter: nil, managedObjectContext: moc) as? [Carts] where result.count > 0{
                cell.addtocartButton.setTitle(NSLocalizedString("Label_Added", comment: "Added"), forState: UIControlState.Normal)
                cell.addtocartButton.backgroundColor = UIColor(rgba: "#0AACF1")
            }else{
                cell.addtocartButton.setTitle(NSLocalizedString("Label_AddTo_Cart", comment: "Add to cart"), forState: UIControlState.Normal)
                cell.addtocartButton.backgroundColor = UIColor(rgba: "#026C7B")
            }
        }else{
            cell.addtocartButton.setTitle(NSLocalizedString("Label_AddTo_Cart", comment: "Add to cart"), forState: UIControlState.Normal)
            cell.addtocartButton.backgroundColor = UIColor(rgba: "#026C7B")
        }
        
        if store_id != nil{
            cell.title.text = cellData.objectForKey("name") as? String
            cell.details.text = cellData.objectForKey("description") as? String
            if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                //print(price)
                cell.price.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    cell.price.text = numberFormatter.stringFromNumber(price)! + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                }
            }
            
            if let urlString: String = cellData.objectForKey("photo") as? String{
                cell.thumbUrl = NSURL(string: urlString)!
            }
        }else{
            if cellData.objectForKey("type") as? String == "store"{
                cell.addtocartButton.hidden = true
                
                cell.title.text = cellData.objectForKey("name") as? String
                cell.details.text = cellData.objectForKey("description") as? String
                if let urlString: String = cellData.objectForKey("thumb") as? String{
                    cell.thumbUrl = NSURL(string: urlString)!
                }
            }else{
            
                cell.title.text = cellData.objectForKey("name") as? String
                cell.details.text = cellData.objectForKey("description") as? String
                if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                    //print(price)
                    cell.price.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        cell.price.text = price.stringValue + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                    }
                }
                
                if let urlString: String = cellData.objectForKey("photo") as? String{
                    cell.thumbUrl = NSURL(string: urlString)!
                }
            }
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("id") as? NSNumber == nil{
            cell.addtocartButton.hidden = true
        }

        //cell.details.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
        
        if expandedIndexPaths.contains(indexPath){
            cell.details.numberOfLines = 0
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), {
                cell.contentView.layoutSubviews()
                cell.contentView.layoutIfNeeded()
            })
        }else{
            cell.details.numberOfLines = 2
        }
        cell.selectionStyle = .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if store_id == nil{
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            let cell: StoreItemsTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! StoreItemsTableViewCell
            if cellData.objectForKey("type") as? String == "store"{
                let itemvc: StoreItemsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsViewController") as! StoreItemsViewController
                itemvc.rootTabBarController = self.tabBarController as? TabBarController
                itemvc.storeData = cellData
                itemvc.titleText = cellData.objectForKey("name") as? String
                itemvc.thumbImage = cell.thumbnail.image
                itemvc.store_id = cellData.objectForKey("id") as! NSNumber
                
                self.navigationController?.presentViewController(itemvc, animated: true, completion: nil)
            }
        }
    }
    
    func onDetailTextTap(cell: StoreItemsTableViewCell) {
//        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
//            if !expandedIndexPaths.contains(indexPath){
//                expandedIndexPaths.append(indexPath)
//                tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
//                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: true)
//            }
//        }
        if cell.details.text != ""{
            let textView: UITextView = UITextView(frame: CGRectMake(0, 0, tableView.frame.size.width, 250))
            textView.selectable = false
            textView.font = UIFont(name: "BahijTheSansArabicPlain", size: 15)
            textView.text = cell.details.text
            if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                textView.textAlignment = .Right
            }
            
            let popup: DVAlertViewController = DVAlertViewController(parentController: self, popoverView: textView, style: DVAlertViewControllerStyle.Popup, contentSize: CGSizeMake(tableView.frame.size.width, 200))
            popup.shouldRemoveControl = true
            popup.show()
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), {
                textView.setContentOffset(CGPointZero, animated: false)
            })
        }
    }
    
    func onAddtocartTap(cell: StoreItemsTableViewCell) {
        if let indexPath: NSIndexPath = tableView.indexPathForCell(cell){
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            
            let item_id: NSNumber = cellData.objectForKey("id") as! NSNumber
            //print(NSPredicate(format: "item_id=\(item_id)"))
            if let result: [Carts] = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: NSPredicate(format: "item_id=\(item_id)"), andSorter: nil, managedObjectContext: moc) as? [Carts] where result.count > 0{
                result.first!.quantity = result.first!.quantity!.integerValue + 1
            }else{
            
                let cart: Carts = CoreDataHelper.insertManagedObject(NSStringFromClass(Carts), managedObjectContext: moc) as! Carts
                
                cart.id = NSNumber(double: NSDate().timeIntervalSince1970 * 1000000).stringValue
                cart.name = cellData.objectForKey("name") as? String
                cart.details = cellData.objectForKey("description") as? String
                cart.item_id = cellData.objectForKey("id") as? NSNumber
                cart.quantity = 1
                if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                    cart.price = NSDecimalNumber(decimal: price.decimalValue)
                }
                if let delivery_charge: NSNumber = cellData.objectForKey("delivery_charge") as? NSNumber{
                    cart.delivery_charge = NSDecimalNumber(decimal: delivery_charge.decimalValue)
                }
                cart.photo = cellData.objectForKey("photo") as? String
                if store_id != nil{
                    cart.store_id = self.store_id
                    cart.store_name = self.storeName
                }else{
                    cart.store_id = cellData.objectForKey("store") as? NSNumber
                    cart.store_name = cellData.objectForKey("store_name") as? String
                }
            }
            
            let success: Bool = CoreDataHelper.saveManagedObjectContext(moc)
            if success{
                //AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Item_Added_Cart", comment: "Item added to cart"), buttonNames: nil, completion: nil)
                cell.addtocartButton.setTitle(NSLocalizedString("Label_Added", comment: "Added"), forState: UIControlState.Normal)
                cell.addtocartButton.backgroundColor = UIColor(rgba: "#0AACF1")
                if let tvc: TabBarController = ((UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController)?.viewControllers[0] as? TabBarController{
                    var placeOrderVC: CartTableViewController?
                    for vc in tvc.viewControllers!{
                        if let nvc: CartTableViewController = vc as? CartTableViewController{
                            placeOrderVC = nvc
                        }
                    }
                    if placeOrderVC != nil{
                        var badgeNumber: NSNumber = 1
                        if let badgeString: String = placeOrderVC?.tabBarItem.badgeValue{
                            badgeNumber = NSNumber(int: (badgeString as NSString).intValue + badgeNumber.intValue)
                        }
                        print(badgeNumber)
                        placeOrderVC?.tabBarItem.badgeValue = badgeNumber.stringValue
                    }
                }
                
            }else{
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Item_Not_Added_Cart", comment: "Item added to cart"), buttonNames: nil, completion: nil)
            }
        }
    }
    
    // MARK: - onItemAddTap
    func onItemAddTap(sender: UIBarButtonItem){
        let vc: AddItemViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddItemViewController") as! AddItemViewController
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - loadTableData
    func loadTableData(){
        if tableData.count == 0{
            activity.startAnimating()
        }
        if store_id != nil{
            DataManager.loadDataAsyncWithCallback("api/store-items/?store=" + store_id.stringValue) { (data, error) -> Void in
                self.activity.stopAnimating()
                self.laodedOnce = true
                if error == nil{
                    do{
                        let jsonArray: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSArray
                        //print(jsonArray)
                        self.tableData = NSMutableArray(array: jsonArray)
                        self.tableView.reloadData()
                        self.tableView.showsHorizontalScrollIndicator = true
                        self.tableView.flashScrollIndicators()
                    }catch{
                        print("JSON decode error")
                    }
                }else{
                    print(error?.localizedDescription)
                    self.noDataMessageLabel?.hidden = false
                }
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageData.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: StoreImageCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! StoreImageCollectionViewCell
        
        let cellData: NSDictionary = imageData[indexPath.row] as! NSDictionary
        cell.thumbUrl = NSURL(string: cellData.objectForKey("photo") as! String)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("select")
    }
    
    // MARK: - loadCollectionViewData
    func loadCollectionViewData(){
        
        if store_id != nil{
            DataManager.loadDataAsyncWithCallback("api/store-photos/?store=" + store_id.stringValue) { (data, error) -> Void in
                if error == nil{
                    do{
                        let jsonArray: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSArray
                        //print(jsonArray)
                        self.imageData = NSMutableArray(array: jsonArray)
                        self.collectionView.reloadData()
                        
                        if self.imageData.count == 0{
                            self.tableView.tableHeaderView = nil
                        }
                    }catch{
                        print("JSON decode error")
                    }
                }
            }
        }
    }

}
