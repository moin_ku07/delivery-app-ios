//
//  MenuCollectionViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/6/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var title: UILabel!
}
