//
//  LoginViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/23/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import TwitterKit

class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginTwitterButton: UIButton!
    @IBOutlet var loginFacebookButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var orLabel: UILabel!
    
    var textFieldFocused: Bool = false
    var selectedTextField: UITextField?
    var previousScrollOffset: CGPoint!
    
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        loginTwitterButton.titleEdgeInsets = UIEdgeInsetsMake(0, -loginTwitterButton.imageView!.frame.size.width, 0, loginTwitterButton.imageView!.frame.size.width)
        loginTwitterButton.imageEdgeInsets = UIEdgeInsetsMake(0, loginTwitterButton.titleLabel!.frame.size.width + 20, 0, -loginTwitterButton.titleLabel!.frame.size.width)
        
        loginFacebookButton.titleEdgeInsets = UIEdgeInsetsMake(0, -loginFacebookButton.imageView!.frame.size.width, 0, loginFacebookButton.imageView!.frame.size.width)
        loginFacebookButton.imageEdgeInsets = UIEdgeInsetsMake(0, loginFacebookButton.titleLabel!.frame.size.width + 20, 0, -loginFacebookButton.titleLabel!.frame.size.width)
        
        // scrollview tap event
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:)))
        tap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tap)
        
        
        // Google SignIn setup
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        
        if let username: String = self.prefs.objectForKey("username") as? String, password: String = self.prefs.objectForKey("password") as? String{
            emailTextField.text = username
            passwordTextField.text = password
            self.loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }else if let session = Twitter.sharedInstance().sessionStore.session(){
            activityView.showActivityIndicator()
            fetchTWTUserAndLogin(session.userID, token: session.authToken)
        }else if let gplus = prefs.objectForKey("isGplusUser") as? Bool where gplus == true{
            activityView.showActivityIndicator()
            GIDSignIn.sharedInstance().signInSilently()
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.doLocalisation()
        }
        
        emailTextField.font = UIFont.systemFontOfSize(15)
        passwordTextField.font = UIFont.systemFontOfSize(15)
        loginTwitterButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        loginFacebookButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        loginButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        skipButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        registerButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        orLabel.font = UIFont.systemFontOfSize(15)
    }
    
    func doLocalisation(){
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            if UIDevice.currentDevice().systemVersion.hasPrefix("8"){
                loginTwitterButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                loginTwitterButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
                
                loginFacebookButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                loginFacebookButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
            }else{
                loginTwitterButton.titleEdgeInsets = UIEdgeInsetsMake(0, loginTwitterButton.imageView!.frame.size.width, 0, -loginTwitterButton.imageView!.frame.size.width)
                loginTwitterButton.imageEdgeInsets = UIEdgeInsetsMake(0, -(loginTwitterButton.titleLabel!.frame.size.width + 10), 0, loginTwitterButton.titleLabel!.frame.size.width)
                print(loginTwitterButton.titleEdgeInsets)
                print(loginTwitterButton.imageEdgeInsets)
                
                loginFacebookButton.titleEdgeInsets = UIEdgeInsetsMake(0, loginFacebookButton.imageView!.frame.size.width, 0, -loginFacebookButton.imageView!.frame.size.width)
                loginFacebookButton.imageEdgeInsets = UIEdgeInsetsMake(0, -(loginFacebookButton.titleLabel!.frame.size.width + 10), 0, loginFacebookButton.titleLabel!.frame.size.width)
            }
            
            emailTextField.textAlignment = .Right
            passwordTextField.textAlignment = .Right
            
            skipButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
            registerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - onLoginWithTwitterTap
    @IBAction func onLoginWithTwitterTap(sender: UIButton) {
        activityView.showActivityIndicator()
        if let session = Twitter.sharedInstance().sessionStore.session(){
            fetchTWTUserAndLogin(session.userID, token: session.authToken)
        }else{
            Twitter.sharedInstance().logInWithCompletion { session, error in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if (session != nil) {
                        self.fetchTWTUserAndLogin(session!.userID, token: session!.authToken)
                        //print("signed in as \(session?.userID)");
                    } else {
                        self.activityView.hideActivityIndicator()
                        print("error: \(error?.localizedDescription)");
                    }
                })
            }
        }
    }
    
    func fetchTWTUserAndLogin(userID: String, token: String){
        let client = TWTRAPIClient(userID: userID)
        let statusesShowEndpoint = "https://api.twitter.com/1.1/users/show.json"
        let params = ["id": userID]
        var clientError : NSError?
        
        do{
            let request: NSURLRequest = client.URLRequestWithMethod("GET", URL: statusesShowEndpoint, parameters: params, error: &clientError)
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if (connectionError == nil) {
                    do{
                        let json : NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        //print(json)
                        if let name: String = json.objectForKey("name") as? String, nameArray: NSArray = name.componentsSeparatedByString(" "){
                            let twtdata: NSDictionary = ["service_name": "twitter", "profile_id": userID, "email":  "N/A", "first_name": nameArray[0], "last_name": nameArray[1], "access_token": token, "user_type": "customer"]
                            //print(twtdata)
                            DataManager.postDataAsyncWithCallback("auth/social/", data: twtdata, completion: { (data, error) -> Void in
                                self.activityView.hideActivityIndicator()
                                if error == nil && data != nil{
                                    do{
                                        let response: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                                        print(response)
                                        self.prefs.setValue("tw", forKey: "social")
                                        self.prefs.setValue(response.objectForKey("first_name"), forKey: "first_name")
                                        self.prefs.setValue(response.objectForKey("last_name"), forKey: "last_name")
                                        self.prefs.setValue(response.objectForKey("auth_token"), forKey: "auth_token")
                                        self.prefs.setValue(response.objectForKey("auth_token"), forKey: "token")
                                        if self.prefs.objectForKey("notification") as? Bool == nil{
                                            self.prefs.setValue(true, forKey: "notification")
                                        }
                                        if let user_id: NSNumber = response.objectForKey("id") as? NSNumber{
                                            self.prefs.setValue(user_id, forKey: "id")
                                        }else if let user_id: String = response.objectForKey("id") as? String{
                                            self.prefs.setValue((user_id as NSString).integerValue, forKey: "id")
                                        }
                                        self.prefs.setValue(response.objectForKey("user_type") as! String, forKey: "user_type")
                                        if response.objectForKey("user_type") as? String == "store_owner"{
                                            self.prefs.setValue(response.objectForKey("store_id") as! NSNumber, forKey: "store_id")
                                        }
                                        self.prefs.setBool(true, forKey: "login_once")
                                        self.prefs.synchronize()
                                        self.navigateMainView()
                                    }catch{
                                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("TW_Login_Error", comment: "Twitter login fails"), buttonNames: nil, completion: nil)
                                    }
                                }else if error != nil && data != nil{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("TW_Login_Error", comment: "Twitter login fails"), buttonNames: nil, completion: nil)
                                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                                }else{
                                    print(error?.localizedDescription)
                                }
                            })
                        }
                    }catch{
                        self.activityView.hideActivityIndicator()
                        print("Unkown error")
                    }
                }
                else {
                    self.activityView.hideActivityIndicator()
                    print("Error: \(connectionError)")
                }
            }
        }
        
    }
    
    // MARK: - onLoginWithGTap
    @IBAction func onLoginWithGTap(sender: UIButton){
        activityView.showActivityIndicator()
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    // Stop the UIActivityIndicatorView animation that was started when the user pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        if let err: NSError = error{
            print(err.localizedDescription)
            activityView.hideActivityIndicator()
        }else{
            print("GIDSignIn success")
        }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!) {
        
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let nameArray: Array = user.profile.name.componentsSeparatedByString(" ")
            let firstName: String = nameArray.first!
            let lastName: String = nameArray.last!
            let email = user.profile.email
            let gdata: NSDictionary = ["service_name": "google+", "profile_id": userId, "email":  email, "first_name": firstName, "last_name": lastName, "access_token": idToken, "user_type": "customer"]
            
            DataManager.postDataAsyncWithCallback("auth/social/", data: gdata, completion: { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if error == nil && data != nil{
                    do{
                        let response: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        //print(response)
                        self.prefs.setValue("fb", forKey: "social")
                        self.prefs.setValue(true, forKey: "isGplusUser")
                        self.prefs.setValue(response.objectForKey("first_name"), forKey: "first_name")
                        self.prefs.setValue(response.objectForKey("last_name"), forKey: "last_name")
                        self.prefs.setValue(response.objectForKey("auth_token"), forKey: "auth_token")
                        self.prefs.setValue(response.objectForKey("auth_token"), forKey: "token")
                        if self.prefs.objectForKey("notification") as? Bool == nil{
                            self.prefs.setValue(true, forKey: "notification")
                        }
                        if let user_id: NSNumber = response.objectForKey("id") as? NSNumber{
                            self.prefs.setValue(user_id, forKey: "id")
                        }else if let user_id: String = response.objectForKey("id") as? String{
                            self.prefs.setValue((user_id as NSString).integerValue, forKey: "id")
                        }
                        self.prefs.setValue(response.objectForKey("user_type") as! String, forKey: "user_type")
                        if response.objectForKey("user_type") as? String == "store_owner"{
                            self.prefs.setValue(response.objectForKey("store_id") as! NSNumber, forKey: "store_id")
                        }
                        self.prefs.setBool(true, forKey: "login_once")
                        self.prefs.synchronize()
                        self.navigateMainView()
                    }catch{
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("G_Login_Error", comment: "Google+ login fails"), buttonNames: nil, completion: nil)
                    }
                }else if error != nil && data != nil{
                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("G_Login_Error", comment: "Google+ login fails"), buttonNames: nil, completion: nil)
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                }else{
                    print(error?.localizedDescription)
                }
            })
        } else {
            print("\(error.localizedDescription)")
            activityView.hideActivityIndicator()
        }
    }

    // MARK: - onLoginTap
    @IBAction func onLoginTap(sender: UIButton) {
        if emailTextField.text == "" || !FormValidation.isValidPhone(emailTextField.text!, minLength: 3, maxLength: 12, country: "KSA"){
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Phone_Email_Missing", comment: "Email/Phone required"), buttonNames: nil, completion: nil)
        }else if passwordTextField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("PW_Missing", comment: "Password missing"), buttonNames: nil, completion: nil)
        }else{
            let loginData: NSDictionary = ["username": emailTextField.text!, "password": passwordTextField.text!]
            activityView.showActivityIndicator()
            DataManager.postDataAsyncWithCallback("auth/multi/login", data: loginData) { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if error == nil && data != nil{
                    do{
                        let response: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                         as! NSDictionary
                        //print(response)
                        if response.objectForKey("status") as! Bool == true{
                            self.prefs.setValue(response.objectForKey("auth_token") as! String, forKey: "auth_token")
                            self.prefs.setValue(response.objectForKey("auth_token"), forKey: "token")
                            self.prefs.setBool(true, forKey: "login_once")
                            
                            self.prefs.setValue(self.emailTextField.text!, forKey: "username")
                            self.prefs.setValue(self.passwordTextField.text!, forKey: "password")
                            self.emailTextField.text = ""
                            self.passwordTextField.text = ""
                            
                            self.prefs.setValue(response.objectForKey("first_name") as! String, forKey: "first_name")
                            self.prefs.setValue(response.objectForKey("last_name") as! String, forKey: "last_name")
                            if self.prefs.objectForKey("notification") as? Bool == nil{
                                print("notification========")
                                self.prefs.setValue(true, forKey: "notification")
                            }
                            if let user_id: NSNumber = response.objectForKey("id") as? NSNumber{
                                self.prefs.setValue(user_id, forKey: "id")
                            }else if let user_id: String = response.objectForKey("id") as? String{
                                self.prefs.setValue((user_id as NSString).integerValue, forKey: "id")
                            }
                            self.prefs.setValue(response.objectForKey("user_type") as! String, forKey: "user_type")
                            //self.prefs.setValue("driver", forKey: "user_type")
                            if response.objectForKey("user_type") as? String == "store_owner"{
                                self.prefs.setValue(response.objectForKey("store_id") as! NSNumber, forKey: "store_id")
                            }
                            self.prefs.setValue(nil, forKey: "social")
                            self.prefs.synchronize()
                            self.navigateMainView()
                        }
                    }catch{
                        print("Unknown error")
                    }
                }else if error != nil && data != nil{
                    do{
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                        let response: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                            as! NSDictionary
                        if let message: String = response.objectForKey("message") as? String{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: message, buttonNames: nil, completion: nil)
                        }else{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("CR_Error", comment: "Credentials does not match"), buttonNames: nil, completion: nil)
                        }
                    }catch{
                        print("Unknown error 2")
                    }
                }else if error != nil{
                    print(error?.localizedDescription)
                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: error!.localizedDescription + " " + NSLocalizedString("Try_Again", comment: "Try again"), buttonNames: nil, completion: nil)
                }
            }
        }
    }
    
    func navigateMainView(){
        if let nvc: NavigationController = (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController{
            let tc: TabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarController") as! TabBarController
            //self.navigationController?.pushViewController(tc, animated: true)
            nvc.viewControllers = [tc]
        }
    }
    
    // MARK: - onTextViewDoneTap
    func onTextViewDoneTap(){
        if selectedTextField != nil{
            focusNextField(selectedTextField!)
        }
    }
    
    // MARK: focusNextField
    func focusNextField(textField: UITextField){
        let nextTag: Int = textField.tag + 1
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == 2{
            self.loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textFieldFocused = true
        selectedTextField = textField
        previousScrollOffset = scrollView.contentOffset
        let textFieldPos: CGPoint = textField.convertPoint(textField.frame.origin, fromView: scrollView)
        //println(abs(textFieldPos.y))
        if self.view.bounds.height - abs(textFieldPos.y) < 290{
            let yPost: CGFloat = abs(textFieldPos.y) - (self.view.bounds.height - 290 - textField.frame.origin.y)
            let scrollPoint: CGPoint = CGPointMake(0, yPost)
            //println(scrollPoint)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
        if textField.keyboardType == UIKeyboardType.PhonePad{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            item = UIBarButtonItem(title: NSLocalizedString("Label_Next", comment: "Next"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onTextViewDoneTap) )
            
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        focusNextField(textField)
        
        return false
    }
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        if textFieldFocused{
            textFieldFocused = false
            emailTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
            self.scrollView.setContentOffset(previousScrollOffset, animated: true)
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "skipShowTabBar"{
            ProfileTableViewController.doLogout()
        }
    }

}
