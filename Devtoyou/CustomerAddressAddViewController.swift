//
//  CustomerAddressAddViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 9/8/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreLocation

class CustomerAddressAddViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var toViewController: UIViewController!
    var coordinates: CLLocationCoordinate2D?
    var addressData: NSDictionary?

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var formContainer: UIView!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var housenoField: UITextField!
    @IBOutlet var doornoField: UITextField!
    @IBOutlet var doorcolorField: UITextField!
    @IBOutlet var districtField: UITextField!
    @IBOutlet var notes: SZTextView!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var districtOverlay: UIView!
    
    var previousScrollOffset: CGPoint!
    var previousContentSize: CGSize?
    var inputCount: Int = 0
    var textFieldFocused: Bool = false
    var selectedTextField: UITextField?
    var activityView: UICustomActivityView!
    
    var user_id: String?
    var postMethod: String = "POST"
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    var selectedDistrict: Int?
    var districtIDs: [Int] = [Int]()
    var districtNames: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.navigationBar.topItem!.title = ""
        
        self.navigationItem.title = NSLocalizedString("Label_Add_Title", comment: "Address details")
        
        if let id: NSNumber = prefs.objectForKey("id") as? NSNumber{
            user_id = id.stringValue
        }

        notes.delegate = self
        notes.textContainerInset = UIEdgeInsetsMake(5, 0, 0, 0)
        notes.textContainer.lineFragmentPadding = 0
        notes.placeholder = NSLocalizedString("Label_Notes", comment: "Notes")
        
        for subview in scrollView.subviews{
            if subview.isKindOfClass(UIView.self){
                for sv in subview.subviews{
                    if let textField: UITextField = sv as? UITextField{
                        textField.tag = inputCount
                        textField.delegate = self
                        textField.returnKeyType = UIReturnKeyType.Next
                        inputCount += 1
                    }
                }
            }
            if let textField: UITextField = subview as? UITextField{
                textField.tag = inputCount
                textField.delegate = self
                textField.returnKeyType = UIReturnKeyType.Next
                inputCount += 1
            }
        }
        notes.tag = inputCount
        
        // scrollview tap event
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:)))
        tap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tap)
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        if addressData != nil{
            if let phone: String = addressData!.objectForKey("phone") as? String{
                phoneField.text = phone
            }
            if let name: String = addressData!.objectForKey("name") as? String{
                nameField.text = name
            }
            if let district: String = addressData!.objectForKey("district") as? String{
                districtField.text = district
            }
            if let district: NSNumber = addressData!.objectForKey("district") as? NSNumber{
                selectedDistrict = district.integerValue
            }
            if let house_no: String = addressData!.objectForKey("house_no") as? String where house_no != ""{
                housenoField.text = house_no
            }
            if let door_no: String = addressData!.objectForKey("door_no") as? String where door_no != ""{
                doornoField.text = door_no
            }
            if let door_color: String = addressData!.objectForKey("door_color") as? String where door_color != ""{
                doorcolorField.text = door_color
            }
            if let note: String = addressData!.objectForKey("note") as? String where note != ""{
                notes.text = note
            }
            /*if let latString: String = addressData!.objectForKey("latitude") as? String, lngString: String = addressData!.objectForKey("longitude") as? String{
                let latTrim: NSString = latString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                let lngTrim: NSString = lngString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                let lat: Double = Double(latTrim as String)!
                let lng: Double = Double(lngTrim as String)!
                coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            }*/
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon-map"), style: UIBarButtonItemStyle.Plain, target: self, action: "onMapTap:")
        }
        
        let discrictTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onDistrictFieldTap(_:)))
        discrictTap.numberOfTapsRequired = 1
        districtOverlay.userInteractionEnabled = true
        districtOverlay.addGestureRecognizer(discrictTap)
        
        fetchAddressList()
        
        /*dispatch_async(dispatch_get_main_queue()){
            self.scrollView.addConstraint(NSLayoutConstraint(item: self.formContainer, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.scrollView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 200))
        }*/
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // check userInterfaceLayoutDirection for Internationalizing
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft{
            for subview in scrollView.subviews{
                if subview.isKindOfClass(UIView.self){
                    for sv in subview.subviews{
                        if let textField: UITextField = sv as? UITextField{
                            textField.textAlignment = .Right
                        }
                    }
                }
                if let textField: UITextField = subview as? UITextField{
                    textField.textAlignment = .Right
                }
            }
            notes.textAlignment = .Right
        }
    }
    
    func fetchAddressList(){
        DataManager.loadDataAsyncWithCallback("api/districts/") { (data, error) in
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do{
                    let jsonArray: [NSDictionary] = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! [NSDictionary]
                    print(jsonArray)
                    self.districtIDs.removeAll()
                    self.districtNames.removeAll()
                    for dis in jsonArray{
                        self.districtIDs.append((dis.objectForKey("id") as! NSNumber).integerValue)
                        self.districtNames.append(dis.objectForKey("name") as! String)
                        if dis.objectForKey("id") as? NSNumber == self.addressData?.objectForKey("district") as? NSNumber{
                            self.districtField.text = dis.objectForKey("name") as? String
                        }
                    }
                }catch{
                    print((error as NSError).localizedDescription)
                }
            }
        }
    }
    
    func onDistrictFieldTap(recognizer: UITapGestureRecognizer?){
        if districtNames.count > 0{
            let popup: DVAlertViewController = DVAlertViewController(parentController: self, popoverVC: nil, style: DVAlertViewControllerStyle.Popup, contentSize: CGSizeMake(200, 150))
            popup.tableData = districtNames
            popup.shouldRemoveControl = true
            if let district: String = self.districtField.text where district != ""{
                if districtNames.contains(district){
                    if let index: Int = districtNames.indexOf(district){
                        popup.selectedIndex = index
                    }
                }
            }
            popup.doneBlock = {(index: Int?)-> Void in
                if index != nil{
                    self.districtField.text = self.districtNames[index!]
                    self.selectedDistrict = self.districtIDs[index!]
                }
            }
            popup.show()
        }else{
            AlertManager.showAlert(self, title: "Warning", message: "There was no districts. Please contact support to inform", buttonNames: nil, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        if textFieldFocused{
            textFieldFocused = false
            for subview in scrollView.subviews{
                if subview.isKindOfClass(UIView.self){
                    for sv in subview.subviews{
                        if let textField: UITextField = sv as? UITextField{
                            textField.resignFirstResponder()
                        }
                    }
                }
                if let textField: UITextField = subview as? UITextField{
                    textField.resignFirstResponder()
                }
            }
            notes.resignFirstResponder()
            /*self.scrollView.setContentOffset(previousScrollOffset, animated: true)
            if self.previousContentSize != nil{
                self.scrollView.contentSize = self.previousContentSize!
            }
            self.previousContentSize = nil*/
        }
    }
    
    // MARK: - onTextViewDoneTap
    func onTextFieldDoneTap(){
        if selectedTextField != nil{
            focusNextField(selectedTextField!)
        }
    }
    
    // MARK: focusNextField
    func focusNextField(textField: UITextField){
        let nextTag: Int = textField.tag + 1
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            if nextResponder == districtField{
                onDistrictFieldTap(nil)
            }else{
                nextResponder.becomeFirstResponder()
            }
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == inputCount - 1{
            notes.becomeFirstResponder()
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textFieldFocused = true
        selectedTextField = textField
        previousScrollOffset = scrollView.contentOffset
        let textFieldPos: CGPoint = scrollView.convertPoint(textField.frame.origin, fromView: scrollView)
        //print(textFieldPos)
        if self.view.bounds.height - abs(textFieldPos.y) - textField.bounds.height < 255{
            let yPost: CGFloat = abs(textFieldPos.y) + textField.bounds.height - (self.view.bounds.height - 255)
            let scrollPoint: CGPoint = CGPointMake(0, yPost)
            //print(scrollPoint)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
            
            /*if self.previousContentSize != nil{
                self.scrollView.contentSize = self.previousContentSize!
            }
            self.previousContentSize = self.scrollView.contentSize
            self.scrollView.contentSize = CGSizeMake(self.previousContentSize!.width, self.previousContentSize!.height + scrollPoint.y)*/
        }
        
        if textField.keyboardType == UIKeyboardType.PhonePad{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            item = UIBarButtonItem(title: NSLocalizedString("Label_Next", comment: "Next"), style: UIBarButtonItemStyle.Plain, target: self, action: Selector("onTextFieldDoneTap") )
            
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        focusNextField(textField)
        
        return false
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        textFieldFocused = true
        previousScrollOffset = scrollView.contentOffset
        let textFieldPos: CGPoint = scrollView.convertPoint(textView.frame.origin, fromView: scrollView)
        //print(textFieldPos)
        if self.view.bounds.height - abs(textFieldPos.y) - textView.bounds.height < 255{
            let yPost: CGFloat = abs(textFieldPos.y) + textView.bounds.height - (self.view.bounds.height - 255)
            let scrollPoint: CGPoint = CGPointMake(0, yPost)
            //print(scrollPoint)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
            
            /*if self.previousContentSize != nil{
                self.scrollView.contentSize = self.previousContentSize!
            }
            self.previousContentSize = self.scrollView.contentSize
            self.scrollView.contentSize = CGSizeMake(self.previousContentSize!.width, self.previousContentSize!.height + scrollPoint.y)*/
        }
        
        // Create a button bar for the number pad
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        
        // Setup the buttons to be put in the system.
        var item: UIBarButtonItem = UIBarButtonItem()
        item = UIBarButtonItem(title: NSLocalizedString("Label_Submit", comment: "Submit"), style: UIBarButtonItemStyle.Plain, target: self, action: Selector("onTextViewSubmitTap") )
        
        let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let toolbarButtons = [flexSpace,item]
        
        //Put the buttons into the ToolBar and display the tool bar
        keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
        textView.inputAccessoryView = keyboardDoneButtonView
        
        return true
    }
    
    func onTextViewSubmitTap(){
        notes.resignFirstResponder()
        submitButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
    }

    @IBAction func onSubmitTap(sender: UIButton) {
        if nameField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_Name_Missing", comment: "Address name missing"), buttonNames: nil, completion: nil)
        }else if phoneField.text == "" || !FormValidation.isValidPhone(phoneField.text!, minLength: 3, maxLength: 12, country: "KSA"){
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Valid_Phone", comment: "Vaild phone"), buttonNames: nil, completion: nil)
        }else if districtField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_District_Missing", comment: "District name missing"), buttonNames: nil, completion: nil)
        }else if postMethod == "POST" && (coordinates == nil || user_id == nil){
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_Warning", comment: "Addresss warning"), buttonNames: nil, completion: nil)
        }else{
            let postData: NSMutableDictionary = [
                "name": nameField.text!,
                "phone": phoneField.text!,
                "street": "",
                "house_no": housenoField.text!,
                "door_no": doornoField.text!,
                "door_color": doorcolorField.text!,
                "note": notes.text!,
                "user": user_id!
            ]
            
            if selectedDistrict != nil{
                postData.setValue(selectedDistrict!, forKey: "district")
            }
            
            self.activityView.showActivityIndicator()
            var url: String = "api/user-locations/"
            if postMethod == "PATCH" || postMethod == "PUT"{
                url = url + (addressData!.objectForKey("id") as! NSNumber).stringValue + "/"
            }else{
                postData.setValue(coordinates!.latitude, forKey: "latitude")
                postData.setValue(coordinates!.longitude, forKey: "longitude")
            }
            print(postData)
            DataManager.postDataAsyncWithCallback(url, method: postMethod, data: NSDictionary(dictionary: postData)) { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if data != nil{
                    do{
                        let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        print(json)
                        self.navigationController?.popToViewController(self.toViewController, animated: true)
                    }catch{
                        AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_Warning", comment: "Addresss warning"), buttonNames: nil, completion: nil)
                        print("Address add json decode error")
                    }
                }else{
                    print(error?.localizedDescription)
                }
            }
        }
    }
    
    // MARK: - onMapTap
    func onMapTap(sender: UIBarButtonItem){
        if let latString: String = addressData!.objectForKey("latitude") as? String, lngString: String = addressData!.objectForKey("longitude") as? String{
            let latTrim: NSString = latString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lngTrim: NSString = lngString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lat: Double = Double(latTrim as String)!
            let lng: Double = Double(lngTrim as String)!
            coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            let vc: AddAddressMapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAddressMapViewController") as! AddAddressMapViewController
            vc.addressCoordinate = coordinates
            vc.user_id = self.user_id
            vc.userLocation_id = (addressData!.objectForKey("id") as? NSNumber)?.stringValue
            vc.toViewController = toViewController!
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_No_Coordinate", comment: "No coordinates found"), buttonNames: nil, completion: nil)
            print("No coordinates found")
        }
    }
}
