//
//  DVImagePopup.swift
//  Devtoyou
//
//  Created by Moin Uddin on 2/23/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class DVImagePopup: UIViewController, UIScrollViewDelegate {
    
    var largeBookImageView: UIView!
    var sourceView: UIView!
    var zoomImageView: UIImageView!
    var scrollView: UIScrollView!
    var image: UIImage!
    
    convenience init(image: UIImage, sourceView: UIView){
        self.init()
        
        self.sourceView = sourceView
        self.image = image
        
        largeBookImageView = UIView()
        largeBookImageView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        largeBookImageView.frame = sourceView.bounds
        largeBookImageView.center = sourceView.center
        largeBookImageView.clipsToBounds = true
        largeBookImageView.translatesAutoresizingMaskIntoConstraints = false
        //largeBookImageView.hidden = true
        sourceView.addSubview(largeBookImageView)
        
        sourceView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: sourceView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
        sourceView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: sourceView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0))
        sourceView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: sourceView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0))
        sourceView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: sourceView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0))
        sourceView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: sourceView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        
        UIView.transitionWithView(sourceView, duration: 0.25, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
            //self.largeBookImageView.hidden = false
            self.setupViews()
            }, completion: nil)
    }
    
    func setupViews(){
        scrollView = UIScrollView()
        //scrollView.backgroundColor = UIColor.redColor()
        //scrollView.clipsToBounds = true
        //scrollView.maximumZoomScale = 5
        print(CGSizeMake(largeBookImageView.bounds.width, largeBookImageView.bounds.height))
        scrollView.contentSize = CGSizeMake(largeBookImageView.bounds.width, largeBookImageView.bounds.height)
        scrollView.delegate = self
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        largeBookImageView.addSubview(scrollView)
        
        largeBookImageView.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: largeBookImageView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
        largeBookImageView.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: largeBookImageView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0))
        largeBookImageView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0))
        largeBookImageView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0))
        
        zoomImageView = UIImageView()
        zoomImageView.image = image
        zoomImageView.contentMode = UIViewContentMode.ScaleAspectFit
        zoomImageView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(zoomImageView)
        
        scrollView.addConstraint(NSLayoutConstraint(item: zoomImageView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
        scrollView.addConstraint(NSLayoutConstraint(item: zoomImageView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0))
        scrollView.addConstraint(NSLayoutConstraint(item: zoomImageView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0))
        scrollView.addConstraint(NSLayoutConstraint(item: zoomImageView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        scrollView.addConstraint(NSLayoutConstraint(item: zoomImageView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
        scrollView.addConstraint(NSLayoutConstraint(item: zoomImageView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0))
        
        let closeButton: UIButtonWithBlock = UIButtonWithBlock()
        closeButton.backgroundColor = UIColor.blackColor()
        closeButton.setTitle("X", forState: UIControlState.Normal)
        closeButton.titleLabel?.textAlignment = NSTextAlignment.Center
        closeButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        closeButton.layer.borderColor = UIColor.whiteColor().CGColor
        closeButton.layer.borderWidth = 2
        closeButton.layer.cornerRadius = 15
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        largeBookImageView.addSubview(closeButton)
        
        largeBookImageView.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: largeBookImageView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 25))
        largeBookImageView.addConstraint(NSLayoutConstraint(item: largeBookImageView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: closeButton, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 5))
        largeBookImageView.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 30))
        largeBookImageView.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 30))
        
        closeButton.handleControlEvent(UIControlEvents.TouchUpInside) { () -> Void in
            UIView.transitionWithView(self.sourceView, duration: 0.1, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
                self.largeBookImageView.removeFromSuperview()
                }, completion: nil)
        }
        
        
        let doubleTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        doubleTapGesture.numberOfTouchesRequired = 1
        self.scrollView.addGestureRecognizer(doubleTapGesture)
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 250 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) { () -> Void in
            let scrollViewFrame: CGRect = self.scrollView.frame
            let scaleWidth: CGFloat = scrollViewFrame.size.width / self.scrollView.contentSize.width
            let scaleHeight: CGFloat = scrollViewFrame.size.height / self.scrollView.contentSize.height
            let minScale: CGFloat = min(scaleWidth, scaleHeight)
            self.scrollView.minimumZoomScale = minScale
            
            self.scrollView.maximumZoomScale = 2.0
            self.scrollView.zoomScale = minScale
            
            //self.centerScrollViewContents()
        }
    }
    
    func centerScrollViewContents(){
        let boundsSize: CGSize = self.scrollView.bounds.size
        var contentsFrame: CGRect = self.zoomImageView.frame
        
        if (contentsFrame.size.width < boundsSize.width) {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if (contentsFrame.size.height < boundsSize.height) {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        
        self.zoomImageView.frame = contentsFrame
        self.scrollView.setContentOffset(CGPointMake(0,0), animated: true)
        
//        print(contentsFrame)
//        print(scrollView.contentSize)
//        print(scrollView.contentInset)
//        print(scrollView.contentOffset)
    }
    
    func doubleTapped(recognizer: UIGestureRecognizer){
        let pointInView: CGPoint = recognizer.locationInView(self.zoomImageView)
        
        var newZoomScale: CGFloat = self.scrollView.zoomScale * 2.0
        newZoomScale = min(newZoomScale, self.scrollView.maximumZoomScale)
        
        let scrollViewSize: CGSize = self.scrollView.bounds.size
        
        let w: CGFloat = scrollViewSize.width / newZoomScale
        let h: CGFloat = scrollViewSize.height / newZoomScale
        let x: CGFloat = pointInView.x - (w / 2.0)
        let y: CGFloat = pointInView.y - (h / 2.0)
        
        let rectToZoomTo: CGRect = CGRectMake(x, y, w, h)
        
        self.scrollView.zoomToRect(rectToZoomTo, animated: true)
    }
    
    // MARK: - ScrollViewDelegate
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.zoomImageView
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        //self.centerScrollViewContents()
    }
}
