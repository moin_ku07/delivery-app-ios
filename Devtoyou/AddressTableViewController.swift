//
//  AddressTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 9/7/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class AddressTableViewController: UITableViewController {
    
    var tableData: NSMutableArray = NSMutableArray()
    
    var activityView: UICustomActivityView!
    
    var user_id: String?
    var user_type: String?
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var navigationTitle: String?
    
    var noDataMessageLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("Label_Add_Tbl_Title", comment: "Address")
        //self.navigationController?.navigationBar.topItem!.title = ""

        if let id: NSNumber = prefs.objectForKey("id") as? NSNumber{
            user_id = id.stringValue
        }
        if let user_type: String = prefs.objectForKey("user_type") as? String{
            self.user_type = user_type
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(onAddTap(_:)))
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationTitle != nil{
            self.navigationItem.title = navigationTitle!
        }
        
        self.loadTableData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableData.count > 0{
            self.noDataMessageLabel?.hidden = true
        }else{
            if self.noDataMessageLabel == nil{
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                    self.tableView.bounds.size.height))
                
                self.noDataMessageLabel!.text = NSLocalizedString("Label_No_Address", comment: "No address")
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.tableView.backgroundView = self.noDataMessageLabel!
                
                //print("label added")
            }else{
                self.noDataMessageLabel?.hidden = false
            }
        }
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: AddressTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AddressTableViewCell
        //cell.selectionStyle = UITableViewCellSelectionStyle.None

        let cellData: NSDictionary = tableData.objectAtIndex(indexPath.row) as! NSDictionary
        
        cell.name.text = cellData.objectForKey("name") as? String
        cell.name.textAlignment = .Natural
        cell.phone.text = cellData.objectForKey("phone") as? String
        cell.phone.textAlignment = .Natural
        
        var addressText: String = ""
        let addressTextArray: NSMutableArray = NSMutableArray()
        
        if let houseno: String = cellData.objectForKey("house_no") as? String where houseno != ""{
            addressTextArray.addObject(NSLocalizedString("Label_House_No", comment: "House no.") + " " + houseno)
        }
        if let street: String = cellData.objectForKey("street") as? String where street != ""{
            addressTextArray.addObject(street)
        }
        
        addressText = (NSArray(array: addressTextArray) as! Array).joinWithSeparator(", ")
        
        if let region: String = cellData.objectForKey("region") as? String where region != ""{
            //print(region)
            addressText = addressText + "\n" + region
        }
        if let note: String = cellData.objectForKey("note") as? String where note != ""{
            addressText = addressText + "\n" + note
        }
        cell.address.text = addressText.stringByReplacingOccurrencesOfString("^\\n*", withString: "", options: .RegularExpressionSearch)
        cell.address.textAlignment = .Natural
        cell.address.textColor = UIColor(rgba: "928a81")
        cell.address.font = UIFont.systemFontOfSize(14)

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc: CustomerAddressAddViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CustomerAddressAddViewController") as! CustomerAddressAddViewController
        vc.addressData = tableData.objectAtIndex(indexPath.row) as? NSDictionary
        vc.toViewController = self
        vc.postMethod = "PATCH"
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let cellData: NSDictionary = tableData.objectAtIndex(indexPath.row) as! NSDictionary
            let postData: NSDictionary = [
                "user": user_id!
            ]
            self.activityView.showActivityIndicator()
            DataManager.postDataAsyncWithCallback("api/user-locations/" + (cellData.objectForKey("id") as! NSNumber).stringValue + "/", method: "DELETE", data: postData) { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if data != nil{
                    //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    self.tableData.removeObjectAtIndex(indexPath.row)
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                }else{
                    AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_Delete_Warning", comment: "Addresss warning"), buttonNames: nil, completion: nil)
                    print(error?.localizedDescription)
                }
            }
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func onAddTap(sender: UIBarButtonItem){
        let vc: AddAddressMapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAddressMapViewController") as! AddAddressMapViewController
        vc.toViewController = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - loadTableData
    func loadTableData(){
        if tableData.count == 0{
            activityView.showActivityIndicator()
        }
        var url: String = "api/"
        if user_type == "customer"{
            url += "customers/" + user_id! + "/"
        }
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    if let locations: NSArray = json.objectForKey("locations") as? NSArray{
                        self.tableData.removeAllObjects()
                        self.tableData = NSMutableArray(array: locations)
                        self.tableView.reloadData()
                    }
                }catch{
                    print("Address add json decode error")
                }
            }
        }
    }

}
