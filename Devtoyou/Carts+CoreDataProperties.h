//
//  Carts+CoreDataProperties.h
//  Devtoyou
//
//  Created by Moin Uddin on 10/3/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Carts.h"

NS_ASSUME_NONNULL_BEGIN

@interface Carts (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSNumber *item_id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *photo;
@property (nullable, nonatomic, retain) NSDecimalNumber *price;
@property (nullable, nonatomic, retain) NSNumber *quantity;
@property (nullable, nonatomic, retain) NSNumber *store_id;
@property (nullable, nonatomic, retain) NSString *store_name;
@property (nullable, nonatomic, retain) NSString *details;
@property (nullable, nonatomic, retain) NSDecimalNumber *delivery_charge;

@end

NS_ASSUME_NONNULL_END
