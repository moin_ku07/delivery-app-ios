//
//  CSRegisterViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 9/11/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class CSRegisterViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var userPic: UIImageView!
    @IBOutlet var addUserPic: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var formContainer: UIView!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var phoneBorder: UIView!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var selectGender: UILabel!
    @IBOutlet var genderView: UIView!
    
    var userImageData: NSData?
    var textFieldFocused: Bool = false
    var isProfileUpdating: Bool = false
    var selectedTextField: UITextField?
    var previousScrollOffset: CGPoint!
    
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    let genderArrayKey: [String] = ["M", "F"]
    let genderArray: [String] = ["ذكر", "أنثى"]
    var selectedGenderKey: String?{
        didSet{
            self.selectGender.text = self.genderArray[self.genderArrayKey.indexOf(self.selectedGenderKey!)!]
        }
    }
    
    var tagCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedGenderKey = "F"
        
        // scrollview tap event
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:)))
        tap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tap)
        
        
        for subview in formContainer.subviews{
            if let textField: UITextField = subview as? UITextField{
                textField.delegate = self
                textField.returnKeyType = UIReturnKeyType.Next
                textField.tag = tagCount
                print(textField.font)
                tagCount += 1
            }
        }
        //(formContainer.viewWithTag(tagCount-1) as! UITextField).returnKeyType = UIReturnKeyType.Go
        
        let addUserPicTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onAddUserPicTap(_:)))
        addUserPicTap.numberOfTapsRequired = 1
        addUserPic.addGestureRecognizer(addUserPicTap)
        
        let gTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onGenderViewTap))
        gTap.numberOfTapsRequired = 1
        genderView.addGestureRecognizer(gTap)
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // check userInterfaceLayoutDirection for Internationalizing
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            skipButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
            loginButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            
            for subview in formContainer.subviews{
                if let textField: UITextField = subview as? UITextField{
                    textField.textAlignment = .Right
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "skipShowTabBar"{
            ProfileTableViewController.doLogout()
        }
    }
    
    // MARK: - onRegisterTap
    @IBAction func onRegisterTap(sender: UIButton) {
        if phoneField.text == "" || !FormValidation.isValidPhone(phoneField.text!, minLength: 3, maxLength: 12, country: "KSA"){
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Valid_Phone", comment: "Valid phone"), buttonNames: nil, completion: nil)
        }else if passwordField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("PW_Missing", comment: "Password can not be empty"), buttonNames: nil, completion: nil)
        }else{
            let registerData: NSMutableDictionary = ["username": phoneField.text!, "password": passwordField.text!, "user_type": "customer"]
            if self.selectedGenderKey != nil{
                registerData.setValue(self.selectedGenderKey!, forKey: "gender")
            }
            if emailField.text != ""{
                registerData.setValue(emailField.text!, forKey: "email")
            }
            activityView.showActivityIndicator()
            DataManager.postDataWithImageAsyncWithCallback("auth/multi/register", method: "POST", data: registerData, imageObj: nil) { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if error == nil && data != nil{
                    do{
                        let response: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                            as! NSDictionary
                        print(response)
                        if response.objectForKey("auth_token") as? String != ""{
                            self.prefs.setValue(response.objectForKey("auth_token") as! String, forKey: "auth_token")
                            self.prefs.setValue(response.objectForKey("auth_token"), forKey: "token")
                            self.prefs.setBool(true, forKey: "login_once")
                            
                            self.prefs.setValue(self.phoneField.text!, forKey: "username")
                            self.prefs.setValue(self.passwordField.text!, forKey: "password")
                            self.phoneField.text = ""
                            self.passwordField.text = ""
                            
                            self.prefs.setValue(response.objectForKey("first_name") as! String, forKey: "first_name")
                            self.prefs.setValue(response.objectForKey("last_name") as! String, forKey: "last_name")
                            if self.prefs.objectForKey("notification") as? Bool == nil{
                                self.prefs.setValue(true, forKey: "notification")
                            }
                            if let user_id: NSNumber = response.objectForKey("id") as? NSNumber{
                                self.prefs.setValue(user_id, forKey: "id")
                            }else if let user_id: String = response.objectForKey("id") as? String{
                                self.prefs.setValue((user_id as NSString).integerValue, forKey: "id")
                            }
                            self.prefs.setValue(response.objectForKey("user_type") as! String, forKey: "user_type")
                            self.prefs.setValue(nil, forKey: "social")
                            self.prefs.synchronize()
                            if self.userImageData != nil{
                                self.postImageData()
                            }else{
                                AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Acount_Success_Message", comment: "Register success"), buttonNames: nil, completion: { (index) -> Void in
                                    if let nvc: NavigationController = (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController{
                                        let tc: TabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarController") as! TabBarController
                                        //self.navigationController?.pushViewController(tc, animated: true)
                                        nvc.viewControllers = [tc]
                                    }
                                })
                            }
                        }
                    }catch{
                        print("Unknown error")
                    }
                }else if error != nil && data != nil{
                    do{
                        let response: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                            as! NSDictionary
                        print(response)
                        if response.objectForKey("auth_token") as? String != ""{
                            AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Acount_Success_Message", comment: "Register success"), buttonNames: nil, completion: { (index) -> Void in
                                let tc: TabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarController") as! TabBarController
                                self.navigationController?.pushViewController(tc, animated: true)
                            })
                        }
                    }catch{
                        AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Account_Failure_Message", comment: "Account failure") + " " + NSLocalizedString("Try_Again", comment: "Try again"), buttonNames: nil, completion: nil)
                        print("Unknown error 2")
                    }
                }else if error != nil{
                    print(error?.localizedDescription)
                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: error!.localizedDescription + " " + NSLocalizedString("Try_Again", comment: "Try again"), buttonNames: nil, completion: nil)
                }
            }
        }
    }
    
    // MARK: - postImageData
    func postImageData(){
        activityView.showActivityIndicator()
        var imageObj: NSMutableDictionary = NSMutableDictionary()
        
        let user_type: String = prefs.objectForKey("user_type") as! String
        let user_id: String = (prefs.objectForKey("id") as! NSNumber).stringValue
        var url: String = "api/"
        if user_type == "customer"{
            url += "customers/" + user_id + "/"
            imageObj = ["filename": "photo.png", "fieldname": "photo", "mimetype": "image/png", "data": userImageData!]
        }else if user_type == "store_owner"{
            url = "api/stores/" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "/"
            imageObj = ["filename": "photo.png", "fieldname": "thumb", "mimetype": "image/png", "data": userImageData!]
        }else if user_type == "driver"{
            url += "drivers/" + user_id + "/"
            imageObj = ["filename": "photo.png", "fieldname": "photo", "mimetype": "image/png", "data": userImageData!]
        }
        
        isProfileUpdating = true
        DataManager.postDataWithImageAsyncWithCallback(url, method: "PATCH", data: nil, imageObj: imageObj) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            self.isProfileUpdating = false
            if data != nil{
                //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                print("success. Nothing to do atm")
                AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Acount_Success_Message", comment: "Register success"), buttonNames: nil, completion: { (index) -> Void in
                    if let nvc: NavigationController = (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController{
                        let tc: TabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarController") as! TabBarController
                        //self.navigationController?.pushViewController(tc, animated: true)
                        nvc.viewControllers = [tc]
                    }
                })
            }else{
                print(error?.localizedDescription)
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Try_Again", comment: "Try again"), buttonNames: nil, completion: nil)
            }
        }
    }
    
    // MARK: - onTextViewDoneTap
    func onTextFieldDoneTap(){
        if selectedTextField != nil{
            focusNextField(selectedTextField!)
        }
    }
    
    // MARK: focusNextField
    func focusNextField(textField: UITextField){
        let nextTag: Int = textField.tag + 1
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == 2{
            //self.registerButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
            onGenderViewTap()
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textFieldFocused = true
        selectedTextField = textField
        previousScrollOffset = scrollView.contentOffset
        let textFieldPos: CGPoint = scrollView.convertPoint(CGPointMake(0, 0), fromView: textField)
        //print(abs(textFieldPos.y))
        if self.view.bounds.height - abs(textFieldPos.y) < 290{
            let yPost: CGFloat = abs(textFieldPos.y) - (self.view.bounds.height - 290 - textField.frame.origin.y)
            let scrollPoint: CGPoint = CGPointMake(0, yPost)
            //println(scrollPoint)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
        if textField.keyboardType == UIKeyboardType.PhonePad{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            item = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onTextFieldDoneTap) )
            
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        focusNextField(textField)
        
        return false
    }
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        if textFieldFocused{
            textFieldFocused = false
            for subview in formContainer.subviews{
                if subview.isKindOfClass(UITextField){
                    (subview as! UITextField).resignFirstResponder()
                }
            }
            self.scrollView.setContentOffset(previousScrollOffset, animated: true)
        }
    }
    
    // MARK: - addUserPicTap
    func onAddUserPicTap(recognizer: UITapGestureRecognizer){
        let imagePicker: UIImagePickerController = UIImagePickerController()
        if TARGET_IPHONE_SIMULATOR == 1{
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        
        //imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let imageInfo = info as NSDictionary
        
        var image: UIImage = imageInfo.objectForKey(UIImagePickerControllerOriginalImage) as! UIImage
        
        let imageSize: CGSize = image.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        if (width != height) {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2;
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0)
            image.drawAtPoint(CGPointMake(-widthOffset, -heightOffset), blendMode: CGBlendMode.Copy, alpha: 1)
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
        }
        
        //scale down image
        let scaledImage = self.scaleImageWith(image, newSize: CGSizeMake(100, 100 / (image.size.width / image.size.height)))
        
        let imageData = UIImagePNGRepresentation(scaledImage)
        userImageData = imageData
        
        self.userPic.image = UIImage(data: imageData!)
        
    }
    
    func scaleImageWith( image: UIImage, newSize: CGSize) -> UIImage{
        //println(newSize)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    // MARK: - onGenderViewTap
    func onGenderViewTap(){
        onViewTap(view)
        var inititalSelection: Int = 0
        if self.selectedGenderKey != nil{
            inititalSelection = self.genderArrayKey.indexOf(self.selectedGenderKey!)!
        }
        ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Gender", comment: "Select Gender"), rows: genderArray, initialSelection: inititalSelection, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
            self.selectedGenderKey = self.genderArrayKey[selectedIndex]
            }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                print("nothing")
            }, origin: self.genderView)
    }

}
