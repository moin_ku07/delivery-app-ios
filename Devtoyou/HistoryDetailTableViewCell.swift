//
//  HistoryDetailTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/15/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RateView

protocol HistoryDetailTableViewCellDelegate{
    func onRateTap(cell: HistoryDetailTableViewCell)
}

class HistoryDetailTableViewCell: UITableViewCell {
    
    var delegate: HistoryDetailTableViewCellDelegate?
    
    @IBOutlet var name: UILabel!
    @IBOutlet var rateView: RateView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        rateView.starSize = 15
        rateView.starFillColor = UIColor(rgba: "de3929")
        rateView.starBorderColor = UIColor(rgba: "de3929")
        rateView.starNormalColor = UIColor(rgba: "efecd9")
        rateView.canRate = false
        
        let rateTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onRateTap(_:)))
        rateTap.numberOfTapsRequired = 1
        rateView.userInteractionEnabled = true
        rateView.addGestureRecognizer(rateTap)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func onRateTap(recognizer: UITapGestureRecognizer){
        delegate?.onRateTap(self)
    }

}
