//
//  TrackOrderViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/6/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import GoogleMaps

class TrackOrderViewController: UIViewController, GMSMapViewDelegate {
    
    var order_id: NSNumber!

    @IBOutlet var statusImageContainer: UIView!
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var requestLabel: UILabel!
    @IBOutlet var requestTimeLabel: UILabel!
    @IBOutlet var reviewLabel: UILabel!
    @IBOutlet var reviewTimeLabel: UILabel!
    @IBOutlet var approveLabel: UILabel!
    @IBOutlet var approveTimeLabel: UILabel!
    @IBOutlet var asdLabel: UILabel!
    @IBOutlet var asdTimeLabel: UILabel!
    @IBOutlet var dgsLabel: UILabel!
    @IBOutlet var dgsTimeLabel: UILabel!
    @IBOutlet var inStoreLabel: UILabel!
    @IBOutlet var inStoreTimeLabel: UILabel!
    @IBOutlet var leaveStoreLabel: UILabel!
    @IBOutlet var leaveStoreTimeLabel: UILabel!
    @IBOutlet var deliveryLabel: UILabel!
    @IBOutlet var deliveryTimeLabel: UILabel!
    @IBOutlet var mapContainer: UIView!
    
    var mapView: GMSMapView!
    var marker: GMSMarker!
    var driverMarker: GMSMarker!
    var orderData: NSDictionary!
    var bounds: GMSCoordinateBounds!
    var polyLine: GMSPolyline!
    
    var activityView: UICustomActivityView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = NSLocalizedString("Label_Track_Order_Title", comment: "Track Order")
        //self.navigationController?.navigationBar.topItem!.title = ""
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: #selector(onRefreshTap(_:)))
        
        let camera = GMSCameraPosition.cameraWithLatitude(24.7494029, longitude: 46.90283750000003, zoom: 10)
        mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        //mapView.delegate = self
        mapView.myLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapContainer.addSubview(mapView)
        
        let vdict: [String: AnyObject] = ["mapview": mapView, "view": mapContainer]
        
        mapContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[mapview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: vdict))
        mapContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[mapview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: vdict))
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                self.doLocalisation()
            }
        }
        
        loadTrackingData()
    }
    
    func doLocalisation(){
//        let labelArray: [UILabel] = [requestLabel, reviewLabel, approveLabel, asdLabel, dgsLabel, inStoreLabel, leaveStoreLabel, deliveryLabel]
//        
//        let labelTimeArray: [UILabel] = [requestTimeLabel, reviewTimeLabel, approveTimeLabel, asdTimeLabel, asdTimeLabel, dgsTimeLabel, inStoreTimeLabel, leaveStoreTimeLabel, deliveryTimeLabel]
        
        // first 4
        requestLabel.addConstraintXY(120, y: -20)
        requestTimeLabel.addConstraintXY(120, y: -50)
        
        reviewLabel.addConstraintXY(40, y: -50)
        reviewTimeLabel.addConstraintXY(40, y: -20)
        
        approveLabel.addConstraintXY(-40, y: -20)
        approveTimeLabel.addConstraintXY(-40, y: -50)
        
        asdLabel.addConstraintXY(-120, y: -60)
        asdTimeLabel.addConstraintXY(-120, y: -20)
        
        // rest 4
        dgsLabel.addConstraintXY(-120, y: 60)
        dgsTimeLabel.addConstraintXY(-120, y: 20)
        
        inStoreLabel.addConstraintXY(-40, y: 20)
        inStoreTimeLabel.addConstraintXY(-40, y: 50)
        
        leaveStoreLabel.addConstraintXY(40, y: 50)
        leaveStoreTimeLabel.addConstraintXY(40, y: 20)
        
        deliveryLabel.addConstraintXY(120, y: 50)
        deliveryTimeLabel.addConstraintXY(120, y: 20)
    }
    
    func loadTrackingData(){
        activityView.showActivityIndicator()
        DataManager.loadDataAsyncWithCallback("api/track-order/?order=" + order_id.stringValue) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    //print(json)
                    if let order_status: String = json.objectForKey("status") as? String, datetimeString: String = json.objectForKey("updated_at") as? String{
                        let dateFormatter: NSDateFormatter = NSDateFormatter()
                        dateFormatter.timeZone = NSTimeZone(name: "UTC")
                        //print(datetimeString)
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
                        if let datetime: NSDate = dateFormatter.dateFromString(datetimeString){
                            //print(datetime)
                            dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                            dateFormatter.dateFormat = "hh:mm a"
                            dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                            self.updateStatusAndImage(order_status, timeString: dateFormatter.stringFromDate(datetime), updateImage: true)
                        }
                        
                    }
                    
                    if let changes: NSArray = json.objectForKey("changes") as? NSArray where changes.count > 0{
                        for dict in (changes as! [NSDictionary]){
                            if let order_status: String = dict.objectForKey("status") as? String, datetimeString: String = dict.objectForKey("updated_at") as? String{
                                let dateFormatter: NSDateFormatter = NSDateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
                                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                                if let datetime: NSDate = dateFormatter.dateFromString(datetimeString){
                                    dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                                    dateFormatter.dateFormat = "hh:mm a"
                                    dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                                    //print(datetime)
                                    self.updateStatusAndImage(order_status, timeString: dateFormatter.stringFromDate(datetime))
                                }
                                
                            }
                        }
                    }
                    
                    if let driver_location: [NSDictionary] = json.objectForKey("driver_location") as? [NSDictionary]{
                        if self.driverMarker == nil{
                            self.driverMarker = GMSMarker()
                        }
                        if driver_location.count > 0{
                            if let latString: NSString = driver_location[0].objectForKey("lat") as? NSString, lngString: NSString = driver_location[0].objectForKey("lng") as? NSString where latString != "" && lngString != ""{
                                if let driverLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latString.doubleValue, lngString.doubleValue){
                                    self.driverMarker.draggable = false
                                    self.driverMarker.icon = UIImage(named: "marker-bicycle")
                                    self.driverMarker.position = driverLocation
                                    self.driverMarker.map = self.mapView
                                }
                            }
                        }
                        
                        /*self.polyLine = GMSPolyline()
                         //polyLine.path = path
                         self.polyLine.strokeColor = UIColor.blueColor()
                         self.polyLine.strokeWidth = 3.0
                         self.polyLine.geodesic = true
                         self.polyLine.map = self.mapView
                         
                         for dl in driver_location.reverse(){
                         if let latString: NSString = dl.objectForKey("lat") as? NSString, lngString: NSString = dl.objectForKey("lng") as? NSString where latString != "" && lngString != ""{
                         if let pathLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latString.doubleValue, lngString.doubleValue){
                         if self.polyLine.path != nil{
                         let path: GMSMutablePath = GMSMutablePath(path: self.polyLine.path)
                         path.addCoordinate(pathLocation)
                         self.polyLine.path = path
                         //print(path)
                         }else{
                         let path: GMSMutablePath = GMSMutablePath()
                         path.addCoordinate(pathLocation)
                         self.polyLine.path = path
                         }
                         }
                         }
                         }*/
                    }
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
        
        let url: String = "api/orders/" + order_id.stringValue + "/?include_address=true&include_store_location=true"
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                    self.orderData = json
                    self.drawMarkersOnMap()
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func onRefreshTap(sender: UIBarButtonItem){
        loadTrackingData()
    }
    
    func drawMarkersOnMap(){
        if let user_location: NSDictionary = orderData.objectForKey("user_location") as? NSDictionary{
            if let latString: NSString = user_location.objectForKey("latitude") as? NSString, lngString: NSString = user_location.objectForKey("longitude") as? NSString where latString != "" && lngString != ""{
                if let userLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latString.doubleValue, lngString.doubleValue){
                    marker = GMSMarker()
                    marker.draggable = false
                    marker.icon = UIImage(named: "marker-usernew")
                    marker.position = userLocation
                    marker.map = mapView
                    //bounds.includingCoordinate(userLocation)
                    if let devicegps: CLLocationCoordinate2D = self.mapView.myLocation?.coordinate{
                            bounds = GMSCoordinateBounds(coordinate: devicegps, coordinate: userLocation)
                    }else{
                        bounds = GMSCoordinateBounds(coordinate: userLocation, coordinate: userLocation)
                    }
                }
            }
        }
        
        if let items: [NSDictionary] = orderData.objectForKey("items") as? [NSDictionary]{
            var tempStoreName: String = ""
            for item in items{
                if tempStoreName != item.objectForKey("store_name") as! String{
                    tempStoreName = item.objectForKey("store_name") as! String
                    //print(tempStoreName)
                    if let store_branches: [NSDictionary] = item.objectForKey("store_branches") as? [NSDictionary]{
                        //print(store_branches)
                        for branch in store_branches{
                            if let latString: NSString = branch.objectForKey("latitude") as? NSString, lngString: NSString = branch.objectForKey("longitude") as? NSString where latString != "" && lngString != ""{
                                if let storeLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latString.doubleValue, lngString.doubleValue){
                                    let storeMarker: GMSMarker = GMSMarker()
                                    var markerTitle: String = tempStoreName
                                    if let branchName: String = branch.objectForKey("branch_name") as? String{
                                        markerTitle += ", " + branchName
                                    }
                                    storeMarker.title = markerTitle
                                    storeMarker.draggable = false
                                    storeMarker.icon = UIImage(named: "marker-giftshop")
                                    storeMarker.position = storeLocation
                                    storeMarker.map = mapView
                                    bounds = bounds.includingCoordinate(storeLocation)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 250 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), { () -> Void in
            self.mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(self.bounds, withPadding: 15))
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - updateStatusLabel
    func updateStatusAndImage(order_status: String, timeString: String, updateImage: Bool = false){
        switch order_status{
        case "REQ": self.requestLabel.textColor = UIColor(rgba: "de3929")
            self.requestTimeLabel.hidden = false
            self.requestTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-req")
            }
            
        case "REV": self.reviewLabel.textColor = UIColor(rgba: "de3929")
            self.reviewTimeLabel.hidden = false
            self.reviewTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-rev")
            }
            
        case "APPR": self.approveLabel.textColor = UIColor(rgba: "de3929")
            self.approveTimeLabel.hidden = false
            self.approveTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-apr")
            }
            
        case "AD": self.asdLabel.textColor = UIColor(rgba: "de3929")
            self.asdTimeLabel.hidden = false
            self.asdTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-asd")
            }
            
        case "DOTW": self.dgsLabel.textColor = UIColor(rgba: "de3929")
            self.dgsTimeLabel.hidden = false
            self.dgsTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-dgs")
            }
            
        case "DIS": self.inStoreLabel.textColor = UIColor(rgba: "de3929")
            self.inStoreTimeLabel.hidden = false
            self.inStoreTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-dis")
            }
            
        case "DLS": self.leaveStoreLabel.textColor = UIColor(rgba: "de3929")
            self.leaveStoreTimeLabel.hidden = false
            self.leaveStoreTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-dls")
            }
            
        case "DELIV": self.deliveryLabel.textColor = UIColor(rgba: "de3929")
            self.deliveryTimeLabel.hidden = false
            self.deliveryTimeLabel.text = timeString
            if updateImage{
                self.statusImageView.image = UIImage(named: "delivery-dlv")
            }
            
        default: break
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }

}

extension UILabel{
    func removeAllConstraints(){
        var list = [NSLayoutConstraint]()
        for c in self.superview!.constraints {
            if c.firstItem as? UIView == self || c.secondItem as? UIView == self {
                list.append(c)
            }
        }
        self.superview!.removeConstraints(list)
        self.removeConstraints(self.constraints)
    }
    
    func addConstraintXY(x: CGFloat, y: CGFloat){
        self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.superview!, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: x))
        self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.superview!, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: y))
    }
}
