//
//  AddAddressMapViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 9/7/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

protocol AddAddressMapViewControllerDelegate{
    func onLocationMarked(location: CLLocationCoordinate2D)
}

class AddAddressMapViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate, AddAddressMapTableViewControllerDelegate {
    
    var delegate: AddAddressMapViewControllerDelegate?
    
    var toViewController: UIViewController!
    
    var locationManager: CLLocationManager!
    
    var addressTextField: UITextField!
    var rightBarButton: UIBarButtonItem!
    
    var mapView: GMSMapView!
    var marker: GMSMarker!
    var placesClient: GMSPlacesClient?
    
    var isMarkerDragging: Bool = false
    var isAutocomplete: Bool = false
    
    var operationQueue: NSOperationQueue?
    
    var addressCoordinate: CLLocationCoordinate2D?
    var user_id: String?
    var userLocation_id: String?
    
    var tableVC: AddAddressMapTableViewController!
    
    var activityView: UICustomActivityView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.navigationBar.topItem!.title = ""
        
        addressTextField = UITextField(frame: CGRectMake(0, 0, 200, 21.0))
        addressTextField.delegate = self
        addressTextField.textColor = UIColor.whiteColor()
        addressTextField.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleTopMargin]
        addressTextField.placeholder = NSLocalizedString("Label_Enter_Address", comment: "Enter Address")
        addressTextField.attributedPlaceholder = NSAttributedString(string:self.addressTextField.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor().colorWithAlphaComponent(0.9)])
        addressTextField.returnKeyType = UIReturnKeyType.Search
        addressTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        addressTextField.textAlignment = .Center
        addressTextField.leftViewMode = UITextFieldViewMode.Always
        addressTextField.leftView = UIImageView(image: UIImage(named: "icon-search-white"))
        self.navigationItem.titleView = addressTextField
        
        rightBarButton = UIBarButtonItem(title: NSLocalizedString("Label_Done", comment: "Done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onDoneTap(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.navigationItem.rightBarButtonItem?.enabled = false

        let camera = GMSCameraPosition.cameraWithLatitude(24.7494029, longitude: 46.90283750000003, zoom: 10)
        mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.delegate = self
        mapView.settings.compassButton = true
        
        self.view = mapView
        
        marker = GMSMarker()
        marker.draggable = true
        marker.icon = UIImage(named: "marker")
        marker.map = mapView
        
        if addressCoordinate != nil{
            let camera: GMSCameraUpdate = GMSCameraUpdate.setTarget(addressCoordinate!, zoom: 16)
            self.mapView.animateWithCameraUpdate(camera)
            
            marker.position = addressCoordinate!
            marker.title = nil
            
            if delegate == nil{
                rightBarButton = UIBarButtonItem(title: NSLocalizedString("Label_Save", comment: "Save"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onSaveTap(_:)))
                self.navigationItem.rightBarButtonItem = rightBarButton
                self.navigationItem.rightBarButtonItem?.enabled = true
            }
            
            let reverseLocation: GMSGeocoder = GMSGeocoder()
            reverseLocation.reverseGeocodeCoordinate(addressCoordinate!) { (response: GMSReverseGeocodeResponse!, error: NSError!) -> Void in
                if error == nil && response != nil{
                    if (response.firstResult() != nil){
                        self.marker.title = response.firstResult().thoroughfare
                    }
                }else if error != nil{
                    print(error.localizedDescription)
                }
                //self.isMarkerDragging = !self.isMarkerDragging
            }
        }
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        placesClient = GMSPlacesClient()
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 500 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) { () -> Void in
            
            self.tableVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddAddressMapTableViewController") as! AddAddressMapTableViewController
            self.tableVC.delegate = self
            self.tableVC.view.frame = CGRectMake(0, 0, 320, 132)
            self.tableVC.view.backgroundColor = UIColor.clearColor()
            self.tableVC.tableView.backgroundColor = UIColor.clearColor()
            self.tableVC.view.hidden = true
            self.mapView.addSubview(self.tableVC.view)
            self.addChildViewController(self.tableVC)
            self.tableVC.didMoveToParentViewController(self)
            self.tableVC.view.translatesAutoresizingMaskIntoConstraints = false
            
            self.mapView.addConstraint(NSLayoutConstraint(item: self.tableVC.view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 132))
            self.mapView.addConstraint(NSLayoutConstraint(item: self.tableVC.view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 0))
            self.mapView.addConstraint(NSLayoutConstraint(item: self.tableVC.view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0))
            self.mapView.addConstraint(NSLayoutConstraint(item: self.tableVC.view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0))
            self.mapView.addConstraint(NSLayoutConstraint(item: self.tableVC.view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0))
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }else if CLLocationManager.authorizationStatus() == .AuthorizedAlways{
            locationManager.startUpdatingLocation()
        }else if CLLocationManager.authorizationStatus() == .NotDetermined{
            locationManager.requestWhenInUseAuthorization()
        }else{
            print("I am here")
        }
        
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print("status: \(status.rawValue)")
        if status == .AuthorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }else if status == .AuthorizedAlways{
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("didUpdateLocations")
        if let location: CLLocation = locations.last{
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            isMarkerDragging = true
            if addressTextField.isFirstResponder(){
                addressTextField.resignFirstResponder()
            }
            
            marker.position = location.coordinate
            marker.title = nil
            
            self.navigationItem.rightBarButtonItem?.enabled = true
            addressCoordinate = location.coordinate
            
            let reverseLocation: GMSGeocoder = GMSGeocoder()
            reverseLocation.reverseGeocodeCoordinate(location.coordinate) { (response: GMSReverseGeocodeResponse!, error: NSError!) -> Void in
                if error == nil && response != nil{
                    if (response.firstResult() != nil){
                        self.marker.title = response.firstResult().thoroughfare
                    }
                }else{
                    print(error.localizedDescription)
                }
                self.isMarkerDragging = !self.isMarkerDragging
            }
            
            locationManager.stopUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - GMSMapViewDelegate
    func mapView(mapView: GMSMapView!, didEndDraggingMarker marker: GMSMarker!) {
        isMarkerDragging = false
        self.navigationItem.rightBarButtonItem?.enabled = true
        
        let reverseLocation: GMSGeocoder = GMSGeocoder()
        reverseLocation.reverseGeocodeCoordinate(marker.position) { (response: GMSReverseGeocodeResponse!, error: NSError!) -> Void in
            if error == nil && response != nil{
                if (response.firstResult() != nil){
                    self.marker.title = response.firstResult().thoroughfare
                }
            }else{
                print(error.localizedDescription)
            }
        }
        
        // store coordinates
        print("store coordinates")
        addressCoordinate = marker.position
    }
    
    func mapView(mapView: GMSMapView!, didBeginDraggingMarker marker: GMSMarker!) {
        isMarkerDragging = true
        if addressTextField.isFirstResponder(){
            addressTextField.resignFirstResponder()
        }
    }
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        isMarkerDragging = true
        if addressTextField.isFirstResponder(){
            addressTextField.resignFirstResponder()
        }
        
        marker.position = coordinate
        marker.title = nil
        
        self.navigationItem.rightBarButtonItem?.enabled = true
        addressCoordinate = coordinate
        
        let reverseLocation: GMSGeocoder = GMSGeocoder()
        reverseLocation.reverseGeocodeCoordinate(coordinate) { (response: GMSReverseGeocodeResponse!, error: NSError!) -> Void in
            if error == nil && response != nil{
                if (response.firstResult() != nil){
                    self.marker.title = response.firstResult().thoroughfare
                }
            }else{
                print(error.localizedDescription)
            }
            self.isMarkerDragging = !self.isMarkerDragging
        }
    }
    
    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView! {
        let view: UIView = UIView(frame: CGRectMake(0, 0, 200, 57))
        view.backgroundColor = UIColor.clearColor()
        
        let view1: UIView = UIView(frame: CGRectMake(0, 0, 200, 40))
        view1.backgroundColor = UIColor.blackColor()
        //view1.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        view1.layer.cornerRadius = view1.frame.size.height / 2
        view.addSubview(view1)
        
        
        let leg: UIImageView = UIImageView(frame: CGRectMake(94.25, 40, 12.5, 17))
        leg.image = UIImage(named: "infowindowleg")
        leg.contentMode = UIViewContentMode.Top
        view.addSubview(leg)
        
        let label: UILabel = UILabel(frame: CGRectMake(0, 0, view1.frame.size.width - 20, 40))
        label.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        label.text = marker.title
        label.textAlignment = .Center
        label.font = UIFont.boldSystemFontOfSize(15)
        label.textColor = UIColor.whiteColor()
        label.center = view1.center
        view1.addSubview(label)
        
        if marker.title == nil{
            mapView.selectedMarker = nil
        }
        
        return view
    }
    
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
        mapView.selectedMarker = nil
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        print("textFieldDidEndEditing")
        if textField.text == ""{
            self.navigationItem.rightBarButtonItem?.enabled = false
            self.tableVC.view.hidden = true
            addressCoordinate = nil
        }else if !isMarkerDragging && !isAutocomplete{
            //self.getGeoCoding(textField.text!)
            print("here")
            self.GMSAutoCompleteQuery(textField.text!)
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.navigationItem.rightBarButtonItem?.enabled = false
        addressCoordinate = nil
        isAutocomplete = false
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var queryText: String = ""
        if range.length == 0{
            queryText = textField.text! + string
        }else if range.length == 1{
            queryText = textField.text!.substringToIndex(textField.text!.endIndex.advancedBy(-1))
        }
        
        print(queryText)
        
        self.GMSAutoCompleteQuery(queryText)
        
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        self.tableVC.view.hidden = true
        //addressCoordinate = nil
        return true
    }
    
    // MARK: - AddAddressMapTableViewControllerDelegate
    func onDidSelectRowAtIndexPath(place: GMSAutocompletePrediction?) {
        isAutocomplete = true
        self.tableVC.view.hidden = true
        self.tableVC.tableData.removeAllObjects()
        addressTextField.resignFirstResponder()
        if place != nil{
            placesClient?.lookUpPlaceID(place!.placeID, callback: { (result: GMSPlace?, error: NSError?) -> Void in
                if result != nil{
                    self.marker.position = result!.coordinate
                    self.marker.title = result!.name
                    self.mapView.selectedMarker = self.marker
                    
                    self.navigationItem.rightBarButtonItem?.enabled = true
                    self.addressCoordinate = result!.coordinate
                    
                    let camera: GMSCameraUpdate = GMSCameraUpdate.setTarget(result!.coordinate, zoom: 16)
                    self.mapView.animateWithCameraUpdate(camera)
                }
            })
        }
    }
    
    // MARK: - GMSAutoCompleteQuery
    func GMSAutoCompleteQuery(queryText: String){
        if queryText != ""{
            let filter = GMSAutocompleteFilter()
            filter.type = GMSPlacesAutocompleteTypeFilter.Address
            placesClient?.autocompleteQuery(queryText, bounds: nil, filter: filter, callback: { (results, error: NSError?) -> Void in
                //print(error)
                //print(results)
                if let error = error {
                    print("Autocomplete error \(error)")
                    self.tableVC.view.hidden = true
                }else if results?.count > 0{
                    self.tableVC.tableData.removeAllObjects()
                    for result in results! {
                        self.tableVC.tableData.addObject(result)
                    }
                    self.tableVC.tableView.reloadData()
                    self.tableVC.view.hidden = false
                }
            })
        }else{
            self.tableVC.view.hidden = true
            //addressCoordinate = nil
        }
    }
    
    // MARK: - getGeoCoding
    func getGeoCoding(address: String){
        operationQueue?.cancelAllOperations()
        
        operationQueue = NSOperationQueue()
        
        let url:NSURL = NSURL(string: NSString(string: "https://maps.googleapis.com/maps/api/geocode/json?address=" + address).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!
        //print(url)
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData
        
        NSURLConnection.sendAsynchronousRequest(request, queue: operationQueue!) { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                if error ==  nil && data != nil{
                    do {
                        let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        print(json)
                        if json.objectForKey("status") as? String == "OK"{
                            self.navigationItem.rightBarButtonItem?.enabled = true
                            if let results: NSArray = json.objectForKey("results") as? NSArray where results.count != 0, let locationObject: NSDictionary = results.firstObject as? NSDictionary, geometry: NSDictionary = locationObject.objectForKey("geometry") as? NSDictionary{
                                //print(geometry)
                                if let boundsObj: NSDictionary = geometry.objectForKey("bounds") as? NSDictionary{
                                    let bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: self.locationFromObject(boundsObj.objectForKey("northeast") as! NSDictionary), coordinate: self.locationFromObject(boundsObj.objectForKey("southwest") as! NSDictionary))
                                    //print(bounds.northEast)
                                    //print(bounds.southWest)
                                    self.mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(bounds, withPadding: 15.0))
                                    
                                    self.marker.position = self.locationFromObject(geometry.objectForKey("location") as! NSDictionary)
                                    self.marker.title = locationObject.objectForKey("formatted_address") as? String
                                    self.addressCoordinate = self.marker.position
                                }
                            }
                        }
                    } catch {
                        print(error)
                    }
                }else if error != nil{
                    print(error?.localizedDescription)
                }
            }
        }
    }
    
    // MARK: - locationFromObject
    func locationFromObject(obj: NSDictionary) -> CLLocationCoordinate2D{
        /*let latString = obj.objectForKey("lat") as! String
        let lngString = obj.objectForKey("lng") as! String
        let latTrim: NSString = latString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let lngTrim: NSString = lngString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())*/
        let lat: Double = obj.objectForKey("lat") as! Double
        let lng: Double = obj.objectForKey("lng") as! Double
        let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        //print(location)
        return location
    }
    
    // MARK: - onDoneTap
    func onDoneTap(sender: UIBarButtonItem){
        if delegate != nil{
            delegate?.onLocationMarked(addressCoordinate!)
            self.navigationController?.popViewControllerAnimated(true)
        }else{
            let vc: CustomerAddressAddViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CustomerAddressAddViewController") as! CustomerAddressAddViewController
            vc.toViewController = toViewController
            vc.coordinates = addressCoordinate
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - onSaveTap
    func onSaveTap(sender: UIBarButtonItem){
        let postData: NSDictionary = [
            "longitude": addressCoordinate!.longitude,
            "latitude": addressCoordinate!.latitude,
            "user": user_id!
        ]
        self.activityView.showActivityIndicator()
        DataManager.postDataAsyncWithCallback("api/user-locations/"  + userLocation_id! + "/", method: "PATCH", data: postData) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    //print(json)
                    self.navigationController?.popViewControllerAnimated(true)
                }catch{
                    AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Add_Warning", comment: "Addresss warning"), buttonNames: nil, completion: nil)
                    print("Address add json decode error")
                }
            }else{
                print(error?.localizedDescription)
            }
        }
    }

}
