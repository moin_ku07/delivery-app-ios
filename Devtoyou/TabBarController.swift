//
//  TabBarController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/24/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate, UISearchBarDelegate {
    
    var searchBar: UISearchBar!
    var logoImageView   : UIImageView!
    var menuButton: UIBarButtonItem?
    var searchButton: UIBarButtonItem?
    var isSearchVisible: Bool = false
    var activeViewController: UIViewController?
    var activeTabIndex: Int?
    var homeVCTapCount: Int = 0
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        /*let statusBarView: UIView = UIView(frame: CGRectMake(0, -20, self.view.frame.size.width, 20))
        statusBarView.backgroundColor = UIColor(rgba: "c73325")
        self.navigationController?.navigationBar.addSubview(statusBarView)*/
        
        self.navigationController?.navigationBarHidden = false
        logoImageView = UIImageView(image: UIImage(named: "icon-logo"))
        self.navigationItem.titleView = logoImageView
        menuButton = UIBarButtonItem(image: UIImage(named: "icon-menu"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onMenuTap(_:)))
        
        if prefs.objectForKey("user_type") as? String != "driver"{
            searchButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: self, action: #selector(onSearchTap(_:)))
            
            searchBar = UISearchBar()
            searchBar.delegate = self
            searchBar.searchBarStyle = UISearchBarStyle.Minimal
            searchBar.showsCancelButton = true
            (searchBar.valueForKey("searchField") as? UITextField)?.textColor = UIColor.whiteColor()
            (searchBar.valueForKey("searchField") as? UITextField)?.leftView = UIImageView(image: UIImage(named: "icon-search-white"))
        }else if prefs.objectForKey("user_type") as? String == "driver"{
            var viewControllers: [UIViewController] = self.viewControllers!
            viewControllers.removeFirst()
            viewControllers.removeAtIndex(1)
            self.viewControllers = viewControllers
        }
        
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext()
        let cartArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc) as! [Carts]
        //print(cartArray)
        if cartArray.count > 0{
            (self.tabBar.items![2] as UITabBarItem).badgeValue = "\(cartArray.count)"
        }
        
        if prefs.objectForKey("user_type") as? String == nil{
            viewControllers = [viewControllers![0]]
        }else if prefs.objectForKey("user_type") as? String == "driver"{
            if let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate{
                appdelegate.initLocationManager()
            }
        }else{
            if let appdelegate: AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate{
                if appdelegate.locationManager != nil{
                    appdelegate.locationManager.stopUpdatingLocation()
                    appdelegate.locationManager.delegate = nil
                }
                //appdelegate.requestInUseAuth()
            }
        }
        
        if prefs.objectForKey("user_type") as? String != nil && prefs.objectForKey("notification") as? Bool == true{
            // unregister push notification first
            /*if let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate{
                appDelegate.unregisterDevice({ (data, error) in*/
                    // register for push notification
                    if UIApplication.sharedApplication().respondsToSelector(#selector(UIApplication.registerUserNotificationSettings(_:))){
                        let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert,UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
                        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
                        UIApplication.sharedApplication().registerForRemoteNotifications()
                    }
                /*})
            }*/
        }
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            let menuBtn: UIBarButtonItem = self.menuButton!
            self.menuButton = self.searchButton
            self.searchButton = menuBtn
        }
        
        self.navigationItem.leftBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem = searchButton
        
        for vc in self.viewControllers!{
            if let nvc: NotificationsTableViewController = vc as? NotificationsTableViewController{
                if UIApplication.sharedApplication().applicationIconBadgeNumber > 0{
                    nvc.tabBarItem.badgeValue = "\(UIApplication.sharedApplication().applicationIconBadgeNumber)"
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.doLocalisation()
        }
        
        loadSurvery()
        
        loadPaymentMethods()
    }
    
    func doLocalisation(){
        if let searchTextField: UITextField = searchBar?.valueForKey("searchField") as? UITextField{
            searchTextField.textAlignment = .Right
        }
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            let viewControllers: [UIViewController] = self.viewControllers!
            self.viewControllers = viewControllers.reverse()
            
            let scalingTransform : CGAffineTransform = CGAffineTransformMakeScale(-1, 1)
            searchBar?.transform = scalingTransform
            
            if let searchTextField: UITextField = (searchBar?.valueForKey("searchField") as? UITextField){
                searchTextField.transform = scalingTransform
                //let leftView: UIView = searchTextField.leftView!
                //searchTextField.leftView = searchTextField.rightView
                //searchTextField.rightView = leftView
            }
            if let cancelButton: UIButton = searchBar?.valueForKey("cancelButton") as? UIButton{
                //print("#######################")
                cancelButton.transform = scalingTransform
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - setSearchAsRightBarButton
    func setSearchAsRightBarButton(){
        self.navigationItem.rightBarButtonItem = searchButton
    }
    
    func showSearchBar() {
        // store initial viewcontroller
        activeTabIndex = self.selectedIndex
        activeViewController = self.selectedViewController
        
        //load controller to show search result
        let svc: StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsTableViewController") as! StoreItemsTableViewController
        //let svcTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "svcTap")
        //svcTap.numberOfTapsRequired = 1
        //svc.view.addGestureRecognizer(svcTap)
        svc.tabBarItem = self.selectedViewController?.tabBarItem
        self.viewControllers![self.selectedIndex] = svc
        
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButtonItem(nil, animated: true)
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            self.isSearchVisible = true
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.navigationItem.setLeftBarButtonItem(self.menuButton, animated: true)
        self.navigationItem.setRightBarButtonItem(self.searchButton, animated: true)
        logoImageView.alpha = 0
        (searchBar.valueForKey("searchField") as? UITextField)?.text = ""
        UIView.animateWithDuration(0.3, animations: {
            self.isSearchVisible = false
            self.viewControllers![self.selectedIndex] = self.activeViewController!
            self.navigationItem.titleView = self.logoImageView
            self.navigationItem.setLeftBarButtonItem(self.menuButton, animated: true)
            self.navigationItem.setRightBarButtonItem(self.searchButton, animated: true)
            self.logoImageView.alpha = 1
            }, completion: { finished in
                
        })
    }
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        DataManager.loadDataAsyncWithCallback("api/search/?query=" + searchText) { (data, error) -> Void in
            if data != nil{
                do{
                    let result: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSArray
                    if let svc: StoreItemsTableViewController = self.selectedViewController as? StoreItemsTableViewController{
                        svc.activity.stopAnimating()
                        svc.tableData = NSMutableArray(array: result)
                        svc.tableView.reloadData()
                    }
                }catch{
                    print(error)
                    print("search error ^")
                }
            }
        }
    }
    
    // MARK: - onMenuTap
    func onMenuTap(sender: UIBarButtonItem){
        //let menuvc: MenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        let menuvc: MenuCollectionViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenuCollectionViewController") as! MenuCollectionViewController
        menuvc.rootTabBarController = self
        self.navigationController?.presentViewController(menuvc, animated: true, completion: nil)
        
    }
    
    // MARK: - onMenuTap
    func onSearchTap(sender: UIBarButtonItem){
        showSearchBar()
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        print("here")
        if isSearchVisible{
            hideSearchBar()
            return false
        }else if let vc: HomeCollectionViewController = viewController as? HomeCollectionViewController where vc.isPopularStore == true{
            homeVCTapCount += 1
            if selectedViewController != nil && selectedViewController!.isEqual(viewController){
                homeVCTapCount += 1
            }
            if homeVCTapCount > 1{
                vc.isPopularStore = false
                vc.tableData.removeAllObjects()
                vc.loadTableData()
                homeVCTapCount = 0
            }
        }else if let vc: PopularItemViewController = viewController as? PopularItemViewController{
            homeVCTapCount += 1
            if selectedViewController != nil && selectedViewController!.isEqual(viewController){
                homeVCTapCount += 1
            }
            if homeVCTapCount > 1{
                let nvc: HomeCollectionViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeCollectionViewController") as! HomeCollectionViewController
                if let index: Int = viewControllers?.indexOf(vc){
                    viewControllers?[index] = nvc
                }
                homeVCTapCount = 0
                return false
            }
        }
        
        return true
    }
    
    // MARK: - loadSurvery
    func loadSurvery(){
        DataManager.loadDataAsyncWithCallback("api/app-settings/") { (data, error) -> Void in
            if data != nil{
                //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do{
                    let json: [NSDictionary] = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! [NSDictionary]
                    for item in json{
                        //print(item)
                        if item.objectForKey("config") as? String == "survey" && item.objectForKey("enabled") as? Bool == true{
                            var alreadyTaken: Bool = false
                            
                            if let cookieName: String = item.objectForKey("additional_value") as? String{
                                //print("RP_" + cookieName)
                                self.prefs.setValue(cookieName, forKey: "surveyID")
                                self.prefs.synchronize()
                            }
                            
                            let cookieJar: NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
                            for cookie in cookieJar.cookies!{
                                print("name: \"\(cookie.name)\", domain: \"\(cookie.domain)\"")
                                if let cookieName: String = self.prefs.objectForKey("surveyID") as? String where "RP_" + cookieName == cookie.name{
                                    //print("if  RP_" + cookieName)
                                    alreadyTaken = true
                                }else{
                                    //print("else")
                                    let regex: String = "^RP_[A-Za-z0-9]+$"
                                    let test: NSPredicate = NSPredicate(format:"SELF MATCHES %@", regex)
                                    if test.evaluateWithObject(cookie.name){
                                        if let surveyTakenCookie: Bool = self.prefs.objectForKey(cookie.name) as? Bool where surveyTakenCookie == true{
                                            alreadyTaken = true
                                        }
                                    }
                                }
                            }
                            
                            if !alreadyTaken{
                                let vc: WebViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
                                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                                vc.navigationItem.title = NSLocalizedString("Label_Survey_Title", comment: "Survey")
                                vc.link = NSURL(string: item.objectForKey("val") as! String)
                                vc.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(self.onWebViewDoneTap(_:)))
                                self.navigationController?.pushViewController(vc, animated: true)
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                                })
                            }
                            
                            break
                        }
                    }
                }catch{
                    print("servery load error: \(error)")
                }
            }
        }
    }
    
    func onWebViewDoneTap(sender: UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func loadPaymentMethods(){
        DataManager.loadDataAsyncWithCallback("api/payment-method/", completion: { (data, error) in
            if data != nil{
                //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do{
                    let json: [NSDictionary] = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! [NSDictionary]
                    (UIApplication.sharedApplication().delegate as! AppDelegate)._paymentMethods = json
                }catch{
                    print((error as NSError).localizedDescription)
                }
            }
        })
    }

}
