//
//  WebViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 11/11/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate, UIScrollViewDelegate {
    
    var link: NSURL?
    var fileName: String?
    
    @IBOutlet var webview: UIWebView!
    
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        self.webview.delegate = self
        //self.webview.scrollView.delegate = self

        webview.scrollView.backgroundColor = UIColor(rgba: "EFECD9")
        
        loadLink()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadLink(){
        if link != nil{
            print("weblink: \(link)")
            self.webview.loadRequest(NSURLRequest(URL: link!))
        }else if fileName != nil{
            let pathExtention: String = fileName!.pathExtension
            print(pathExtention)
            let pathPrefix: String = fileName!.stringByDeletingPathExtension
            print(pathPrefix)
            let path: String = NSBundle.mainBundle().pathForResource(pathPrefix, ofType: pathExtention)!
            if let fileLink: NSURL = NSURL(fileURLWithPath: path){
                self.webview.loadRequest(NSURLRequest(URL: fileLink))
            }
        }
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityView.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(webview: UIWebView) {
        activityView.hideActivityIndicator()
        self.webview.scrollView.contentOffset = CGPointMake(0,0)
        
        let cookieJar: NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookie in cookieJar.cookies!{
            //print("name: \"\(cookie.name)\", domain: \"\(cookie.domain)\"")
            
            if let surveyID: String = self.prefs.objectForKey("surveyID") as? String, cookieName: String = "RP_" + surveyID where cookie.name == cookieName{
                //print(cookieName)
                self.prefs.setValue(true, forKey: cookieName)
                self.prefs.synchronize()
            }else{
                let regex: String = "^RP_[A-Za-z0-9]+$"
                let test: NSPredicate = NSPredicate(format:"SELF MATCHES %@", regex)
                if test.evaluateWithObject(cookie.name){
                    //print("else \(cookie.name)")
                    self.prefs.setValue(true, forKey: cookie.name)
                    self.prefs.synchronize()
                    
                }
            }
        }
    }

}

extension String {
    
    
    var lastPathComponent: String {
        
        get {
            return (self as NSString).lastPathComponent
        }
    }
    var pathExtension: String {
        
        get {
            
            return (self as NSString).pathExtension
        }
    }
    var stringByDeletingLastPathComponent: String {
        
        get {
            
            return (self as NSString).stringByDeletingLastPathComponent
        }
    }
    var stringByDeletingPathExtension: String {
        
        get {
            
            return (self as NSString).stringByDeletingPathExtension
        }
    }
    var pathComponents: [String] {
        
        get {
            
            return (self as NSString).pathComponents
        }
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.stringByAppendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        
        let nsSt = self as NSString  
        
        return nsSt.stringByAppendingPathExtension(ext)  
    }  
}
