//
//  StoreItemsViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/1/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import SDWebImage

class StoreItemsViewController: UIViewController {

    var storeData: NSDictionary?
    var store_id: NSNumber!
    var titleText: String?
    var thumbImage: UIImage?
    @IBOutlet var thumbActivity: UIActivityIndicatorView!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var tablecontentView: UIView!
    
    var rootTabBarController: TabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
        contentView.layer.cornerRadius = 8
        
        titleLabel.text = titleText
        
        if thumbImage == nil{
            if let urlString: String = storeData?.objectForKey("thumb") as? String{
                /*let delay = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
                dispatch_async(delay){
                    do{
                        let imageData: NSData = try NSData(contentsOfURL: NSURL(string: urlString)!, options: NSDataReadingOptions())
                        dispatch_async(dispatch_get_main_queue()){
                            self.thumbActivity.stopAnimating()
                            self.thumbnail.image = UIImage(data: imageData)
                        }
                    }catch{
                        print("Thumb fetch error")
                    }
                }*/
                thumbnail.sd_setImageWithURL(NSURL(string: urlString)!) { (image:UIImage!, error: NSError!, cache: SDImageCacheType, url: NSURL!) -> Void in
                    self.thumbActivity.stopAnimating()
                }
            }
        }else{
            thumbActivity.stopAnimating()
            thumbnail.image = thumbImage
        }
        
        let tvc: StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsTableViewController") as! StoreItemsTableViewController
        tvc.store_id = store_id
        tvc.storeName = titleText!
        self.tablecontentView.addSubview(tvc.view)
        tvc.view.translatesAutoresizingMaskIntoConstraints = false
        let viewDict: [String: AnyObject] = ["view": self.tablecontentView, "tview": tvc.view]
        self.tablecontentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[tview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewDict))
        self.tablecontentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[tview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewDict))
        
        self.addChildViewController(tvc)
        tvc.didMoveToParentViewController(self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onCloseTap(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

}
