//
//  HomeCollectionViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/24/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import SDWebImage

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var imageUrl: NSURL?{
        didSet{
            /*let delay = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(delay){
                do{
                    let imageData: NSData = try NSData(contentsOfURL: self.imageUrl!, options: NSDataReadingOptions())
                    dispatch_async(dispatch_get_main_queue()){
                        self.activityIndicator.stopAnimating()
                        self.imageView.image = UIImage(data: imageData)
                    }
                }catch{
                    dispatch_async(dispatch_get_main_queue()){
                        self.activityIndicator.stopAnimating()
                    }
                    print("Thumb fetch error")
                }
            }*/
            imageView.sd_setImageWithURL(imageUrl) { (image:UIImage!, error: NSError!, cache: SDImageCacheType, url: NSURL!) -> Void in
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
}
