//
//  AddressTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 9/7/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var phone: UILabel!
    @IBOutlet var address: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        address.textContainerInset = UIEdgeInsetsZero
        address.textContainer.lineFragmentPadding = 0
        address.textColor = UIColor(rgba: "928a81")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
