//
//  AddItemViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/18/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddItemViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var formContainer: UIView!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var addItemPic: UIButton!
    @IBOutlet var itemPicture: UIImageView!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var priceField: UITextField!
    @IBOutlet var descriptionField: UITextField!
    
    var itemImageData: NSData?
    
    var textFieldFocused: Bool = false
    var selectedTextField: UITextField?
    var previousScrollOffset: CGPoint!
    
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = NSLocalizedString("Label_Add_Item_Title", comment: "Add Item")
        
        var tagCount: Int = 0
        for subview in formContainer.subviews{
            if let textField: UITextField = subview as? UITextField{
                textField.delegate = self
                textField.returnKeyType = UIReturnKeyType.Next
                textField.tag = tagCount
                tagCount += 1
            }
        }
        (formContainer.viewWithTag(tagCount-1) as! UITextField).returnKeyType = UIReturnKeyType.Go
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tapGesture)
        
        let addItemPicTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onItemPicTap(_:)))
        addItemPicTap.numberOfTapsRequired = 1
        addItemPic.addGestureRecognizer(addItemPicTap)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // check userInterfaceLayoutDirection for Internationalizing
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft{
            for subview in formContainer.subviews{
                if let textField: UITextField = subview as? UITextField{
                    textField.textAlignment = .Right
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - onTextViewDoneTap
    func onTextFieldDoneTap(){
        if selectedTextField != nil{
            focusNextField(selectedTextField!)
        }
    }
    
    // MARK: focusNextField
    func focusNextField(textField: UITextField){
        let nextTag: Int = textField.tag + 1
        
        if let nextResponder: UIResponder = textField.superview?.viewWithTag(nextTag){
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            self.onViewTap(self.view)
        }
        
        if textField.tag == 2{
            self.submitButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textFieldFocused = true
        selectedTextField = textField
        previousScrollOffset = previousScrollOffset != nil ? previousScrollOffset! : scrollView.contentOffset
        //let textFieldPos: CGPoint = textField.convertPoint(textField.frame.origin, fromView: scrollView)
        let textFieldPos: CGPoint = scrollView.convertPoint(CGPointMake(0, 0), fromView: textField)
        print(abs(textFieldPos.y))
        if self.view.bounds.height - abs(textFieldPos.y) < 290{
            let yPost: CGFloat = abs(textFieldPos.y) - (self.view.bounds.height - 290 - textField.frame.origin.y)
            let scrollPoint: CGPoint = CGPointMake(0, yPost)
            //println(scrollPoint)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
        if textField.keyboardType == UIKeyboardType.DecimalPad{
            // Create a button bar for the number pad
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            
            // Setup the buttons to be put in the system.
            var item: UIBarButtonItem = UIBarButtonItem()
            item = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onTextFieldDoneTap) )
            
            let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let toolbarButtons = [flexSpace,item]
            
            //Put the buttons into the ToolBar and display the tool bar
            keyboardDoneButtonView.setItems(toolbarButtons, animated: true)
            textField.inputAccessoryView = keyboardDoneButtonView
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        focusNextField(textField)
        
        return false
    }
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        if textFieldFocused{
            textFieldFocused = false
            for subview in formContainer.subviews{
                if subview.isKindOfClass(UITextField){
                    (subview as! UITextField).resignFirstResponder()
                }
            }
            self.scrollView.setContentOffset(previousScrollOffset, animated: true)
        }
    }
    
    // MARK: - addUserPicTap
    func onItemPicTap(recognizer: UITapGestureRecognizer){
        let imagePicker: UIImagePickerController = UIImagePickerController()
        ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Photo_Option", comment: "Choose Photo"), rows: [NSLocalizedString("Label_Select_Photo_Library", comment: "Photo Gallery"), NSLocalizedString("Label_Select_Photo_Camera", comment: "Camera")], initialSelection: 0, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                if selectedIndex == 0{
                    imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                }else{
                    imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                }
                
                //imagePicker.allowsEditing = true
                imagePicker.delegate = self
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
            }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                print("nothing")
            }, origin: recognizer.view)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let imageInfo = info as NSDictionary
        
        var image: UIImage = imageInfo.objectForKey(UIImagePickerControllerOriginalImage) as! UIImage
        
        let imageSize: CGSize = image.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        if (width != height) {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2;
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0)
            image.drawAtPoint(CGPointMake(-widthOffset, -heightOffset), blendMode: CGBlendMode.Copy, alpha: 1)
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
        }
        
        //scale down image
        let scaledImage = self.scaleImageWith(image, newSize: CGSizeMake(100, 100 / (image.size.width / image.size.height)))
        
        let imageData = UIImagePNGRepresentation(scaledImage)
        
        itemPicture.image = UIImage(data: imageData!)
        
        itemImageData = imageData
    }
    
    func scaleImageWith( image: UIImage, newSize: CGSize) -> UIImage{
        //println(newSize)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    @IBAction func onSubmitTap(sender: UIButton) {
        if nameField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Enter_Item_Name", comment: "Enter item name"), buttonNames: nil, completion: nil)
        }else if descriptionField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Enter_Item_Description", comment: "Enter item description"), buttonNames: nil, completion: nil)
        }else if priceField.text == ""{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Enter_Item_Price", comment: "Enter item price"), buttonNames: nil, completion: nil)
        }else if itemImageData == nil{
            AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Enter_Item_Photo", comment: "Select item photo"), buttonNames: nil, completion: nil)
        }else{
            let imageObj: NSDictionary = ["filename": "photo.png", "fieldname": "photo", "mimetype": "image/png", "data": itemImageData!]
            
            let postData: NSDictionary = [
                "name": nameField.text!,
                "description": descriptionField.text!,
                "price": (priceField.text! as NSString).floatValue,
                "store": prefs.objectForKey("store_id") as! NSNumber
            ]
            print(NSString.stringFromObject(postData, encoding: NSUTF8StringEncoding))
            activityView.showActivityIndicator()
            DataManager.postDataWithImageAsyncWithCallback("api/store-items/", method: "POST", data: postData, imageObj: imageObj) { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if data != nil{
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    do{
                        let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                        if json.objectForKey("status") as? Bool == false{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Item_Post_Error", comment: "Label_Item_Post_Error"), buttonNames: nil, completion: nil)
                        }else{
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                    }catch{
                        print(error)
                    }
                }else{
                    print(error?.localizedDescription)
                }
            }
        }
    }

}
