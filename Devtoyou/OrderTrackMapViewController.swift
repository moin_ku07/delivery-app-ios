//
//  OrderTrackMapViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/22/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class OrderTrackMapViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    var mapView: GMSMapView!
    var marker: GMSMarker!
    var markers: [GMSMarker] = [GMSMarker]()
    
    var orderData: NSDictionary!
    var bounds: GMSCoordinateBounds!
    
    var destinationGPS: CLLocationCoordinate2D?
    var rightBarButtonItem: UIBarButtonItem!
    
    var locationManager: CLLocationManager!
    var userLocation: CLLocationCoordinate2D?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationItem.title = NSLocalizedString("Label_Track_Map_Title", comment: "Tracking Map")
        
        rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon-direction"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onDirectionTap(_:)))

        let camera = GMSCameraPosition.cameraWithLatitude(24.7494029, longitude: 46.90283750000003, zoom: 10)
        mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.delegate = self
        //mapView.myLocationEnabled = true
        mapView.settings.compassButton = true
        //mapView.settings.myLocationButton = true
        
        self.view = mapView
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 250 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), { () -> Void in
            self.drawMarkersOnMap()
        })
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }else if CLLocationManager.authorizationStatus() == .AuthorizedAlways{
            locationManager.startUpdatingLocation()
        }else if CLLocationManager.authorizationStatus() == .NotDetermined{
            locationManager.requestWhenInUseAuthorization()
        }else{
            print("I am here")
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print("status: \(status.rawValue)")
        if status == .AuthorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }else if status == .AuthorizedAlways{
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("didUpdateLocations")
        if let location: CLLocation = locations.last{
            if mapView.myLocationEnabled != true{
                mapView.myLocationEnabled = true
                mapView.settings.myLocationButton = true
            }
            
            userLocation = location.coordinate
        }
    }
    
    // MARK: - drawMarkersOnMap
    
    func drawMarkersOnMap(){
        //bounds = GMSCoordinateBounds(coordinate: self.mapView.myLocation.coordinate, coordinate: self.mapView.myLocation.coordinate)
        
        if let user_location: NSDictionary = orderData.objectForKey("user_location") as? NSDictionary{
            if let latString: NSString = user_location.objectForKey("latitude") as? NSString, lngString: NSString = user_location.objectForKey("longitude") as? NSString where latString != "" && lngString != ""{
                if let userLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latString.doubleValue, lngString.doubleValue){
                    marker = GMSMarker()
                    marker.draggable = false
                    marker.icon = UIImage(named: "marker-usernew")
                    marker.position = userLocation
                    marker.map = mapView
                    //bounds.includingCoordinate(userLocation)
                    if let devicegps: CLLocationCoordinate2D = self.mapView.myLocation?.coordinate{
                        bounds = GMSCoordinateBounds(coordinate: devicegps, coordinate: userLocation)
                    }else{
                        bounds = GMSCoordinateBounds(coordinate: userLocation, coordinate: userLocation)
                    }
                }
            }
        }
        
        if let items: [NSDictionary] = orderData.objectForKey("items") as? [NSDictionary]{
            var tempStoreName: String = ""
            for item in items{
                if tempStoreName != item.objectForKey("store_name") as! String{
                    tempStoreName = item.objectForKey("store_name") as! String
                    //print(tempStoreName)
                    if let store_branches: [NSDictionary] = item.objectForKey("store_branches") as? [NSDictionary]{
                        for branch in store_branches{
                            if let latString: NSString = branch.objectForKey("latitude") as? NSString, lngString: NSString = branch.objectForKey("longitude") as? NSString where latString != "" && lngString != ""{
                                if let storeLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latString.doubleValue, lngString.doubleValue){
                                    let storeMarker: GMSMarker = GMSMarker()
                                    var markerTitle: String = tempStoreName
                                    if let branchName: String = branch.objectForKey("branch_name") as? String{
                                        markerTitle += ", " + branchName
                                    }
                                    storeMarker.title = markerTitle
                                    storeMarker.draggable = false
                                    storeMarker.icon = UIImage(named: "marker-giftshop")
                                    storeMarker.position = storeLocation
                                    storeMarker.map = mapView
                                    bounds = bounds.includingCoordinate(storeLocation)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 500 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), { () -> Void in
            self.mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(self.bounds, withPadding: 15))
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - GMSMapViewDelegate
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        destinationGPS = marker.position
        navigationItem.setRightBarButtonItem(rightBarButtonItem, animated: true)
        return true
    }
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        destinationGPS = nil
        navigationItem.setRightBarButtonItem(nil, animated: true)
    }

    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView! {
        let view: UIView = UIView(frame: CGRectMake(0, 0, 200, 57))
        view.backgroundColor = UIColor.clearColor()
        
        let view1: UIView = UIView(frame: CGRectMake(0, 0, 200, 40))
        view1.backgroundColor = UIColor.blackColor()
        //view1.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        view1.layer.cornerRadius = view1.frame.size.height / 2
        view.addSubview(view1)
        
        
        let leg: UIImageView = UIImageView(frame: CGRectMake(94.25, 40, 12.5, 17))
        leg.image = UIImage(named: "infowindowleg")
        leg.contentMode = UIViewContentMode.Top
        view.addSubview(leg)
        
        let label: UILabel = UILabel(frame: CGRectMake(0, 0, view1.frame.size.width - 20, 40))
        label.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        label.text = marker.title
        label.textAlignment = .Center
        label.font = UIFont.boldSystemFontOfSize(15)
        label.textColor = UIColor.whiteColor()
        label.center = view1.center
        view1.addSubview(label)
        
        if marker.title == nil{
            mapView.selectedMarker = nil
        }
        
        return view
    }
    
    // MARK: - onDirectionTap
    func onDirectionTap(sender: UIBarButtonItem){
        if destinationGPS != nil && userLocation != nil{
            if UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!){
                let url: String = "comgooglemaps://?t=m&saddr=\(userLocation!.latitude),\(userLocation!.longitude)&daddr=\(destinationGPS!.latitude),\(destinationGPS!.longitude)&directionsmode=driving"
                print(url)
                UIApplication.sharedApplication().openURL(NSURL(string: url)!)
            }else{
                let url: String = "http://maps.apple.com/?saddr=\(userLocation!.latitude),\(userLocation!.longitude)&daddr=\(destinationGPS!.latitude),\(destinationGPS!.longitude)"
                print(url)
                UIApplication.sharedApplication().openURL(NSURL(string: url)!)
            }
        }
    }

}
