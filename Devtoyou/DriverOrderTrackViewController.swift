//
//  DriverOrderTrackViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/19/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class DriverOrderTrackViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var orderTextView: UITextView!
    @IBOutlet var addressMapButton: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var scrollView: UIScrollView!
    
    var tableData: [NSDictionary] = [NSDictionary]()
    var storeArray: [NSDictionary] = [NSDictionary]()
    var orderData: NSMutableDictionary!
    var activityView: UICustomActivityView!
    var tableHeightConstraints: [NSLayoutConstraint]?
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        
        self.navigationItem.title = NSLocalizedString("Label_Track_Order_Title", comment: "Track Order")
        self.navigationController?.navigationBar.topItem!.title = ""
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        orderTextView.textContainerInset = UIEdgeInsetsMake(8, 8, 8, 8)
        orderTextView.text = ""
        
        loadOrderData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = NSLocalizedString("Label_Track_Order_Title", comment: "Track Order")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: DriverOrderTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! DriverOrderTableViewCell
        
        let cellData: NSDictionary = tableData[indexPath.row]
        //print(cellData)
        
        cell.cellButton.tag = indexPath.row
        cell.cellButton.setTitle(cellData.objectForKey("name") as? String, forState: UIControlState.Normal)
        cell.cellButton.addTarget(self, action: #selector(onCellButtonTap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        if cellData.objectForKey("type") as? String == "accept"{
            cell.cellButton.backgroundColor = UIColor(rgba: "0AACF1")
        }else if cellData.objectForKey("type") as? String == "delivered"{
            cell.cellButton.backgroundColor = UIColor(rgba: "DE3929")
        }else{
            cell.cellButton.backgroundColor = UIColor(rgba: "026E7B")
        }
        cell.cellButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        let enableButton: Bool = cellData.objectForKey("enabled") as! Bool
        
        cell.cellButton.enabled = enableButton
        if !enableButton{
            cell.cellButton.backgroundColor = cell.cellButton.backgroundColor?.colorWithAlphaComponent(0.6)
            cell.cellButton.setTitleColor(UIColor.whiteColor().colorWithAlphaComponent(0.6), forState: UIControlState.Normal)
        }
        
        return cell
    }
    
    // MARK: - submitBalance
    func submitBalance(postData: NSDictionary, twoStep: Bool? = false, completion: (status: Bool) -> Void){
        print(postData)
        DataManager.postDataAsyncWithCallback("api/user-balance/", data: postData, completion: { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                    if json.objectForKey("status") as? Bool == false{
                        completion(status: false)
                    }else{
                        if twoStep == true{
                            let newPostData: NSMutableDictionary = NSMutableDictionary(dictionary: postData)
                            newPostData.removeObjectForKey("store")
                            newPostData.setValue(-(postData.objectForKey("amount") as! NSNumber).floatValue, forKey: "amount")
                            newPostData.setValue(postData.objectForKey("given_by") as! NSNumber, forKey: "user")
                            print("newPostData: \(newPostData)")
                            self.activityView.showActivityIndicator()
                            self.submitBalance2(newPostData, completion: completion)
                        }else{
                            //AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                            completion(status: true)
                        }
                    }
                }catch{
                    print(error)
                    print("Unknow error")
                    completion(status: false)
                }
            }else{
                completion(status: false)
            }
        })
    }
    
    func submitBalance2(postData: NSDictionary, completion: (status: Bool) -> Void){
        print(postData)
        DataManager.postDataAsyncWithCallback("api/user-balance/", data: postData, completion: { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                    if json.objectForKey("status") as? Bool == false{
                        completion(status: false)
                    }else{
                        //AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                        completion(status: true)
                    }
                }catch{
                    print(error)
                    print("Unknow error")
                    completion(status: false)
                }
            }else{
                completion(status: false)
            }
        })
    }
    
    
    // MARK: - submitDLS
    func submitDLS(postData: NSDictionary){
        let alertController: UIAlertController = UIAlertController(title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Input_Store_Amount", comment: "Enter store amount"), preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField: UITextField) -> Void in
            textField.text = "0"
            textField.keyboardType = UIKeyboardType.NumbersAndPunctuation
        }
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Cancel", comment: "Cancel"), style: UIAlertActionStyle.Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("AM_Okay", comment: "Okay"), style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in
            if let amountText: NSString = alertController.textFields?.last?.text where amountText != ""{
                
                let storeAmountPostData: NSDictionary = [ "amount": amountText.floatValue, "store": postData.objectForKey("store") as! NSNumber, "given_by": self.prefs.objectForKey("id") as! NSNumber]
                
                self.activityView.showActivityIndicator()
                self.submitBalance(storeAmountPostData, twoStep: true, completion: { (status) -> Void in
                    if status == true{
                        DataManager.postDataAsyncWithCallback("api/update-order/", data: postData, completion: { (data, error) -> Void in
                            self.activityView.hideActivityIndicator()
                            if data != nil{
                                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                                do{
                                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                                    if json.objectForKey("status") as? Bool == false{
                                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                    }else{
                                        AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                                        self.loadOrderData()
                                    }
                                }catch{
                                    print(error)
                                    print("Unknow error")
                                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                }
                            }else{
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                            }
                        })
                    }else{
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Store_Input_Error_Driver", comment: "try again"), buttonNames: nil, completion: nil)
                    }
                })
            }
        }))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - submitDELIV
    func submitDELIV(postData: NSDictionary){
        let url: String = "api/orders/" + (orderData.objectForKey("id") as! NSNumber).stringValue + "/"
        
        let alertController: UIAlertController = UIAlertController(title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Input_Customer_Amount", comment: "Enter store amount"), preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField: UITextField) -> Void in
            textField.text = "0"
            textField.keyboardType = UIKeyboardType.NumberPad
        }
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Cancel", comment: "Cancel"), style: UIAlertActionStyle.Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("AM_Okay", comment: "Okay"), style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in
            if let amountText: NSString = alertController.textFields?.last?.text where amountText != ""{
                
                // object to add amount to customers account by driver
                let customerAmountPostData: NSDictionary = [ "amount": amountText.floatValue, "user": self.orderData.objectForKey("user") as! NSNumber, "given_by": self.prefs.objectForKey("id") as! NSNumber]
                
                // object to add amount to driver by customer
                let customerAmountPostData1: NSDictionary = [ "amount": amountText.floatValue, "user": self.prefs.objectForKey("id") as! NSNumber, "given_by": self.orderData.objectForKey("user") as! NSNumber]
                
                self.activityView.showActivityIndicator()
                self.submitBalance(customerAmountPostData, completion: { (status) -> Void in
                    if status == true{
                        self.submitBalance(customerAmountPostData1, completion: { (status) -> Void in
                            if status == true{
                                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postData, completion: { (data, error) -> Void in
                                    self.activityView.hideActivityIndicator()
                                    if data != nil{
                                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                                        do{
                                            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                                            if json.objectForKey("status") as? Bool == false{
                                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                            }else{
                                                AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: { (index) -> Void in
                                                    dispatch_async(dispatch_get_main_queue()){
                                                        self.navigationController?.popViewControllerAnimated(true)
                                                    }
                                                })
                                                self.loadOrderData()
                                            }
                                        }catch{
                                            print(error)
                                            print("Unknow error")
                                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                        }
                                    }else{
                                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                    }
                                })
                            }else{
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Customer_Input_Error_Driver", comment: "try again"), buttonNames: nil, completion: nil)
                            }
                        })
                    }else{
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Customer_Input_Error_Driver", comment: "try again"), buttonNames: nil, completion: nil)
                    }
                })
            }
        }))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - onCellButtonTap
    func onCellButtonTap(sender: UIButton){
        if let cellData: NSDictionary = tableData[sender.tag]{
            if let type: String = cellData.objectForKey("type") as? String{
                if ["instore", "leavestore"].contains(type){
                    let postData: NSMutableDictionary = ["order": orderData.objectForKey("id") as! NSNumber, "store": cellData.objectForKey("store_id") as! NSNumber]
                    if type == "instore"{
                        postData.setValue("DIS", forKey: "status")
                    }else if type == "leavestore"{
                        postData.setValue("DLS", forKey: "status")
                        if let store_owner: NSNumber = cellData.objectForKey("store_owner") as? NSNumber{
                            postData.setValue(store_owner, forKey: "store_owner")
                        }
                        self.submitDLS(postData)
                        return
                    }
                    print(postData)
                    activityView.showActivityIndicator()
                    DataManager.postDataAsyncWithCallback("api/update-order/", data: postData, completion: { (data, error) -> Void in
                        self.activityView.hideActivityIndicator()
                        if data != nil{
                            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            do{
                                let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                                if json.objectForKey("status") as? Bool == false{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                }else{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                                    self.loadOrderData()
                                }
                            }catch{
                                print(error)
                                print("Unknow error")
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                            }
                        }else{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                        }
                    })
                }else if ["instorenull", "leavestorenull"].contains(type){
                    let status: String = type == "instorenull" ? "DIS" : "DLS"
                    let postData: NSDictionary = ["status": status]
                    print(postData)
                    let url: String = "api/orders/" + (orderData.objectForKey("id") as! NSNumber).stringValue + "/"
                    activityView.showActivityIndicator()
                    DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postData, completion: { (data, error) -> Void in
                        self.activityView.hideActivityIndicator()
                        if data != nil{
                            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            do{
                                let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                                if json.objectForKey("status") as? NSString == nil{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                }else{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                                    self.loadOrderData()
                                }
                            }catch{
                                print(error)
                                print("Unknow error")
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                            }
                        }else{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                        }
                    })
                }else{
                    let url: String = "api/orders/" + (orderData.objectForKey("id") as! NSNumber).stringValue + "/"
                    let postData: NSMutableDictionary = ["driver": prefs.objectForKey("id") as! NSNumber]
                    if type == "accept"{
                        postData.setValue("DOTW", forKey: "status")
                    }else if type == "delivered"{
                        postData.setValue("DELIV", forKey: "status")
                        if orderData.objectForKey("user_type") as? String == "customer"{
                            let dateFormatter: NSDateFormatter = NSDateFormatter()
                            dateFormatter.timeZone = NSTimeZone(name: "UTC")
                            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
                            let pickupTimeString: String = dateFormatter.stringFromDate(NSDate())
                            postData.setValue(pickupTimeString, forKey: "pickedup")
                            
                            self.submitDELIV(postData)
                            return
                        }
                    }
                    print(postData)
                    activityView.showActivityIndicator()
                    DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postData, completion: { (data, error) -> Void in
                        self.activityView.hideActivityIndicator()
                        if data != nil{
                            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            do{
                                let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                                if json.objectForKey("status") as? Bool == false{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                                }else{
                                    AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Success", comment: "Success"), buttonNames: nil, completion: nil)
                                    self.loadOrderData()
                                    if type == "delivered"{
                                        dispatch_async(dispatch_get_main_queue()){
                                            self.navigationController?.popViewControllerAnimated(true)
                                        }
                                    }
                                }
                            }catch{
                                print(error)
                                print("Unknow error")
                                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                            }
                        }else{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                        }
                    })
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onAddressMapTap(sender: UIButton) {
        let vc: OrderTrackMapViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OrderTrackMapViewController") as! OrderTrackMapViewController
        vc.orderData = self.orderData
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: - loadOrderData
    func loadOrderData(){
        let url: String = "api/orders/" + (orderData.objectForKey("id") as! NSNumber).stringValue + "/?&include_address=true&include_store_location=true&dis_dls=true"
        self.activityView.showActivityIndicator()
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                    if let _: NSNumber = json.objectForKey("id") as? NSNumber{
                        self.orderData = NSMutableDictionary(dictionary: json)
                        self.loadTableData()
                    }else{
                        self.scrollView.removeFromSuperview()
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: { void in
                            dispatch_async(dispatch_get_main_queue()){
                                self.navigationController?.popViewControllerAnimated(true)
                            }
                        })
                    }
                }catch{
                    print(error)
                    print("Unknow error")
                    self.scrollView.removeFromSuperview()
                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: { void in
                        dispatch_async(dispatch_get_main_queue()){
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                    })
                }
            }else{
                self.scrollView.removeFromSuperview()
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: { void in
                    dispatch_async(dispatch_get_main_queue()){
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                })
            }
        }
    }
    
    //MARK: - loadTableData
    func loadTableData(){
        tableData.removeAll()
        storeArray.removeAll()
        
        let formattedString: NSMutableAttributedString = NSMutableAttributedString()
        
        let newStr = NSAttributedString(string: NSLocalizedString("Label_Order_Details", comment: "Order Details") + ":\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)])
        formattedString.appendAttributedString(newStr)
        
        if let orederID: String = orderData.objectForKey("display_id") as? String{
            let newStr = NSAttributedString(string: "#\(orederID)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)])
            formattedString.appendAttributedString(newStr)
        }
        
        if let pickup: String = orderData.objectForKey("pickup") as? String{
            var pickupStr: String = NSLocalizedString("Label_Pickup_Time", comment: "Delivery time")
            
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let datetime: NSDate = dateFormatter.dateFromString(pickup){
                //print(datetime)
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    dateFormatter.locale = NSLocale(localeIdentifier: "ar")
                }
                pickupStr += ": " + dateFormatter.stringFromDate(datetime)
            }
            
            let newStr = NSAttributedString(string: "\(pickupStr)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            formattedString.appendAttributedString(newStr)
        }
        
        if let items: NSArray = orderData.objectForKey("items") as? NSArray where items.count > 0{
            var tempStoreName: String = ""
            
            for item in (items as! [NSDictionary]){
                
                if tempStoreName != item.objectForKey("store_name") as! String{
                    tempStoreName = item.objectForKey("store_name") as! String
                    
                    let store_id: NSNumber = item.objectForKey("store_id") as! NSNumber
                    let purchaseState: Bool = item.objectForKey("purchased") as! Bool
                    
                    storeArray.append(["name": tempStoreName, "id": store_id.integerValue, "enabled": purchaseState])
                }
                
                var itemName: String = String()
                if let item_name: String = item.objectForKey("item_name") as? String{
                    itemName = item_name
                }
                
                var priceStr: String = String()
                if let price: NSNumber = item.objectForKey("price") as? NSNumber{
                    priceStr = NSString(format: "%.2f", price.floatValue) as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        priceStr = numberFormatter.stringFromNumber(price)!
                    }
                }
                
                var quantityStr: String = String()
                if let quantity: NSString = item.objectForKey("quantity") as? NSString{
                    quantityStr = quantity as String
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity.integerValue)!
                    }
                }
                
                if let quantity: NSNumber = item.objectForKey("quantity") as? NSNumber{
                    quantityStr = quantity.stringValue
                    if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                        quantityStr = numberFormatter.stringFromNumber(quantity)!
                    }
                }
                
                var storeName: String = String()
                if let store_name: String = item.objectForKey("store_name") as? String{
                    storeName = store_name
                }
                
                let currencyCode: String = NSLocalizedString("Label_Currency_Code", comment: "SAR")
                let fromStr: String = NSLocalizedString("Label_Order_From_Store", comment: "From")
                
                var newStr = NSAttributedString(string: "\(itemName) (\(priceStr) \(currencyCode)) x \(quantityStr) \(fromStr) \(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    newStr = NSAttributedString(string: "\u{200F}\(itemName) (\(priceStr) \(currencyCode)) \u{200F}x \(quantityStr) \(fromStr) \u{200F}\(storeName)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
                }
                formattedString.appendAttributedString(newStr)
            }
        }else if let additional_notes: String = orderData.objectForKey("additional_notes") as? String{
            let newStr: NSAttributedString = NSAttributedString(string: "\(additional_notes)\n")
            formattedString.appendAttributedString(newStr)
        }
        
        if let total: NSNumber = orderData.objectForKey("total") as? NSNumber where total.floatValue > 0{
            var newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(total)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                newStr = NSAttributedString(string: NSLocalizedString("Label_Total", comment: "Total") + ": \(numberFormatter.stringFromNumber(total)!)\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)])
            }
            formattedString.appendAttributedString(newStr)
        }
        
        if let paymentMethods: [NSDictionary] = (UIApplication.sharedApplication().delegate as? AppDelegate)?._paymentMethods{
            for dict in paymentMethods{
                if (dict["id"] as? NSNumber)?.integerValue == (orderData["payment_method"] as? NSNumber)?.integerValue{
                    formattedString.appendAttributedString(NSAttributedString(string: NSLocalizedString("Label_Payment_Option", comment: "Payment Option") + ": " + (dict["name"] as! String), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12)]))
                    break
                }
            }
        }
        
        var addressText: String = ""
        let addressTextArray: NSMutableArray = NSMutableArray()
        
        if let locationDict: NSDictionary = orderData.objectForKey("user_location") as? NSDictionary{
            if let name: String = locationDict.objectForKey("name") as? String where name != ""{
                addressTextArray.addObject(name)
            }
            if let houseno: String = locationDict.objectForKey("house_no") as? String where houseno != ""{
                addressTextArray.addObject(NSLocalizedString("Label_House_No", comment: "House no.") + " " + houseno)
            }
            if let street: String = locationDict.objectForKey("street") as? String where street != ""{
                addressTextArray.addObject(street)
            }
            
            addressText = (NSArray(array: addressTextArray) as! Array).joinWithSeparator(", ")
            
            if let region: String = locationDict.objectForKey("region") as? String where region != ""{
                //print(region)
                addressText = addressText + "\n" + region
            }
            if let phone: String = locationDict.objectForKey("phone") as? String where phone != ""{
                addressText = addressText + "\n" + NSLocalizedString("Label_Phone", comment: "Phone no") + ": " + phone
            }
            if let note: String = locationDict.objectForKey("note") as? String where note != ""{
                addressText = addressText + "\n" + note
            }
            formattedString.appendAttributedString(NSAttributedString(string: "\n" + NSLocalizedString("Label_Order_Delivery_Address", comment: "Order Details") + ":\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(15)]))
            formattedString.appendAttributedString(NSAttributedString(string: addressText.stringByReplacingOccurrencesOfString("^\\n*", withString: "", options: .RegularExpressionSearch), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)]))
        }
        
        orderTextView.attributedText = formattedString
        orderTextView.textAlignment = .Natural
        orderTextView.textColor = UIColor(rgba: "8B837B")
        
        dispatch_async(dispatch_get_main_queue(), {
            self.orderTextView.scrollsToTop = true
            self.orderTextView.contentOffset = CGPointZero
        })
        
        // create tableData based on storesArray
        var enableButton: Bool = true
        // check with status to show this button
        // button enable / disable logic
        // 1. All buttons except "Accept" disabled when accept is enabled
        // 2. Delivery disabled until all store has DLS status
        // 3. Disable other buttons when DIS for any store
        // 4. Enable DLS button for DIS store
        // 5. When DIS store changed to DLS, enable all other buttons except DLS store
        
        if ["REQ", "REV", "APPR", "AD"].contains(orderData.objectForKey("status") as! String){
            tableData.append(["name": NSLocalizedString("Label_Track_Order_Accept", comment: "accept order"), "type": "accept", "enabled": true])
            enableButton = false
        }
        
        var deliveryButtonEnabled: Bool = enableButton
        var disableForDIS: Bool = false
        if let driverStatusArray: [NSDictionary] = orderData.objectForKey("dis_dls") as? [NSDictionary] where driverStatusArray.count > 0{
            if driverStatusArray.count == storeArray.count{
                var isDLS: Bool = true
                for dict in driverStatusArray{
                    if dict.objectForKey("status") as? String == "DIS"{
                        isDLS = false
                    }
                }
                if isDLS{
                    deliveryButtonEnabled = true
                }
            }else{
                for dict in driverStatusArray{
                    if dict.objectForKey("status") as? String == "DIS"{
                        disableForDIS = true
                    }
                }
                deliveryButtonEnabled = false
            }
        }else{
            deliveryButtonEnabled = false
        }
        //print("disableForDIS: \(disableForDIS)")
        //print(storeArray)
        
        for dict in storeArray{
            var name: String = NSLocalizedString("Label_Track_Order_In_Store", comment: "in store") + " " + (dict.objectForKey("name") as! String)
            let disState: Bool = enableButton == false ? false : getButtonEnabledState(dict, type: "instore", state: !disableForDIS)
            tableData.append(["name": name, "type": "instore", "enabled": disState, "store_id": dict.objectForKey("id") as! NSNumber])
            
            let dlsState: Bool = enableButton == false ? false : getButtonEnabledState(dict, type: "leavestore", state: !disableForDIS)
            name = NSLocalizedString("Label_Track_Order_Leave_Store", comment: "leave store") + " " + (dict.objectForKey("name") as! String)
            tableData.append(["name": name, "type": "leavestore", "enabled": dlsState, "store_id": dict.objectForKey("id") as! NSNumber])
        }
        
        // for text order when there is no stores
        if storeArray.count == 0{
            var purchaseState: Bool = orderData.objectForKey("status") as? String == "DOTW" ? true : false
            tableData.append(["name": NSLocalizedString("Label_Track_Order_In_Store", comment: "in store"), "type": "instorenull", "enabled": purchaseState, "store_id": NSNull()])
            
            purchaseState = orderData.objectForKey("status") as? String == "DIS" ? true : false
            tableData.append(["name": NSLocalizedString("Label_Track_Order_Leave_Store", comment: "leave store"), "type": "leavestorenull", "enabled": purchaseState, "store_id": NSNull()])
            if orderData.objectForKey("status") as? String == "DLS"{
                deliveryButtonEnabled = true
            }
        }
        
        if orderData.objectForKey("status") as! String == "DELIV"{
            deliveryButtonEnabled = false
        }
        tableData.append(["name": NSLocalizedString("Label_Status_Code_DELIV", comment: "delivered"), "type": "delivered",  "enabled": deliveryButtonEnabled])
        
        if tableHeightConstraints != nil{
            scrollView.removeConstraints(tableHeightConstraints!)
        }
        
        tableHeightConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[tableView(height)]", options: NSLayoutFormatOptions(), metrics: ["height": tableData.count*50], views: ["tableView": tableView])
        scrollView.addConstraints(tableHeightConstraints!)
        
        tableView.reloadData()
        
        //self.tableView.beginUpdates()
        //self.tableView.reloadRowsAtIndexPaths(self.tableView.indexPathsForVisibleRows!, withRowAnimation: UITableViewRowAnimation.Automatic)
        //self.tableView.endUpdates()
    }
    
    func getButtonEnabledState(data: NSDictionary, type: String, state: Bool) -> Bool{
        if let driverStatusArray: [NSDictionary] = orderData.objectForKey("dis_dls") as? [NSDictionary] where driverStatusArray.count > 0{
            for dict in driverStatusArray{
                if dict.objectForKey("store") as? NSNumber == data.objectForKey("id") as? NSNumber{
                    print("store match")
                    if dict.objectForKey("status") as? String == "DIS" && type == "instore"{
                        return false
                    }else if dict.objectForKey("status") as? String == "DIS" && type == "leavestore"{
                        print("pere")
                        return true
                    }else if dict.objectForKey("status") as? String == "DLS" && type == "instore"{
                        return false
                    }else if dict.objectForKey("status") as? String == "DLS" && type == "leavestore"{
                        print("pere")
                        return false
                    }
                }
            }
        }
        
        return state
    }

}
