//
//  Carts+CoreDataProperties.m
//  Devtoyou
//
//  Created by Moin Uddin on 10/3/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Carts+CoreDataProperties.h"

@implementation Carts (CoreDataProperties)

@dynamic id;
@dynamic item_id;
@dynamic name;
@dynamic photo;
@dynamic price;
@dynamic quantity;
@dynamic store_id;
@dynamic store_name;
@dynamic details;
@dynamic delivery_charge;

@end
