//
//  MenuCollectionViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/6/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MenuCollectionViewController: UICollectionViewController {
    
    var rootTabBarController: TabBarController!
    
    var tableData: NSMutableArray = NSMutableArray()
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = collectionView!.backgroundColor
        self.collectionView!.frame = CGRectMake(((view.frame.size.width - 320) / 2), 0, 320, self.collectionView!.frame.size.height)

        if let user_type: String = prefs.objectForKey("user_type") as? String{
            if user_type == "customer" || user_type == "store_owner"{
                tableData = [
                    ["title": NSLocalizedString("Label_All_Store", comment: "All stores"), "icon": "icon-store", "type": "all_store"],
                    ["title": NSLocalizedString("Label_About_App", comment: "About App"), "icon": "icon-information", "type": "about_app"],
                    ["title": NSLocalizedString("Label_Popular_Store", comment: "Popular Stores"), "icon": "icon-popular-store", "type": "popular_store"],
                    ["title": NSLocalizedString("Label_Popular_Item", comment: "Popular Items"), "icon": "icon-popular-item", "type": "popular_item"],
                    ["title": NSLocalizedString("Label_Help_Center", comment: "Help Center"), "icon": "icon-ask", "type": "help_center"],
                    ["title": NSLocalizedString("Label_Rate_App", comment: "Rate this app"), "icon": "icon-star", "type": "rate_app"],
                    //["title": "Swap Language", "icon": "icon-star", "type": "lang_change"]
                ]
            }else if user_type == "driver"{
                tableData = [
                    ["title": NSLocalizedString("Label_About_App", comment: "About App"), "icon": "icon-information", "type": "about_app"],
                    ["title": NSLocalizedString("Label_Help_Center", comment: "Help Center"), "icon": "icon-ask", "type": "help_center"],
                    ["title": NSLocalizedString("Label_Rate_App", comment: "Rate this app"), "icon": "icon-star", "type": "rate_app"],
                    //["title": "Swap Language", "icon": "icon-star", "type": "lang_change"]
                ]
            }
        }else{
            tableData = [
                ["title": NSLocalizedString("Label_All_Store", comment: "All stores"), "icon": "icon-store", "type": "all_store"],
                ["title": NSLocalizedString("Label_About_App", comment: "About App"), "icon": "icon-information", "type": "about_app"],
                ["title": NSLocalizedString("Label_Popular_Store", comment: "Popular Stores"), "icon": "icon-popular-store", "type": "popular_store"],
                ["title": NSLocalizedString("Label_Popular_Item", comment: "Popular Items"), "icon": "icon-popular-item", "type": "popular_item"],
                ["title": NSLocalizedString("Label_Help_Center", comment: "Help Center"), "icon": "icon-ask", "type": "help_center"],
                ["title": NSLocalizedString("Label_Rate_App", comment: "Rate this app"), "icon": "icon-star", "type": "rate_app"],
                ["title": "دخول", "icon": "icon-login-grey", "type": "login"],
                ["title": "تسجيل", "icon": "icon-register-grey", "type": "register"]
            ]
        }
        
        let closeButton: UIButton = UIButton(frame: CGRectMake(0, 0, 30, 30))
        //closeButton.backgroundColor = UIColor.yellowColor()
        closeButton.setBackgroundImage(UIImage(named: "icon-close-black"), forState: UIControlState.Normal)
        closeButton.addTarget(self, action: #selector(onCloseTap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(closeButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 30))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 30))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 15))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: -12))
        
        collectionView?.reloadData()
        
        dispatch_async(dispatch_get_main_queue()){ () -> Void in
            self.doLocalisation()
        }
    }
    
    func doLocalisation(){
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1);
            collectionView!.transform = scalingTransform
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return tableData.count
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let view: UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "headerCell", forIndexPath: indexPath)
        
        return view
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: MenuCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MenuCollectionViewCell
    
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        cell.title.text = cellData.objectForKey("title") as? String
        cell.title.font = UIFont.systemFontOfSize(14)
        if let imageName: String = cellData.objectForKey("icon") as? String{
            cell.imageView.image = UIImage(named: imageName)
        }
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1);
            cell.transform = scalingTransform
        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        let type: String = cellData.objectForKey("type") as! String
        var homeindex: Int = 0
        for (index, vc) in rootTabBarController.viewControllers!.enumerate(){
            if let _: HomeCollectionViewController = vc as? HomeCollectionViewController{
                homeindex = index
            }else if let _: PopularItemViewController = vc as? PopularItemViewController{
                homeindex = index
            }
        }
        
        if type == "all_store"{
            let vc: HomeCollectionViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeCollectionViewController") as! HomeCollectionViewController
            rootTabBarController.selectedIndex = homeindex
            rootTabBarController.viewControllers![homeindex] = vc
        }else if type == "popular_store"{
            let vc: HomeCollectionViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeCollectionViewController") as! HomeCollectionViewController
            vc.isPopularStore = true
            rootTabBarController.selectedIndex = homeindex
            rootTabBarController.viewControllers![homeindex] = vc
        }else if type == "popular_item"{
            let hvc: HomeCollectionViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeCollectionViewController") as! HomeCollectionViewController
            let vc: PopularItemViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PopularItemViewController") as! PopularItemViewController
            rootTabBarController.selectedIndex = homeindex
            vc.tabBarItem.title = hvc.tabBarItem.title
            vc.tabBarItem.image  = hvc.tabBarItem.image
            vc.tabBarItem.titlePositionAdjustment = hvc.tabBarItem.titlePositionAdjustment
            rootTabBarController.viewControllers![homeindex] = vc
        }else if type == "lang_change"{
            let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
            if prefs.objectForKey("isArabic") as? Bool == nil{
                prefs.setObject(["ar-SA", "en"], forKey: "AppleLanguages")
                prefs.setObject(true, forKey: "isArabic")
            }else if prefs.objectForKey("isArabic") as? Bool == false{
                prefs.setBool(true, forKey: "isArabic")
                prefs.setObject(["ar-SA", "en"], forKey: "AppleLanguages")
            }else{
                prefs.setBool(false, forKey: "isArabic")
                prefs.setObject(["en", "ar-SA"], forKey: "AppleLanguages")
            }
            prefs.synchronize()
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 500 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue(), { () -> Void in
                AlertManager.showAlert(self.rootTabBarController, title: "Alert", message: "Please restart app so see changes", buttonNames: nil, completion: nil)
            })
        }else if type == "login"{
            if let nvc: NavigationController = (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController{
                nvc.navigationBarHidden = true
                let vc: FirstScreenViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FirstScreenViewController") as! FirstScreenViewController
                nvc.viewControllers = [vc]
            }
        }else if type == "register"{
            if let nvc: NavigationController = (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController{
                nvc.navigationBarHidden = true
                let vc: CSRegisterViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CSRegisterViewController") as! CSRegisterViewController
                nvc.viewControllers = [vc]
            }
        }else if type == "about_app"{
            let vc: AboutAppViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AboutAppViewController") as! AboutAppViewController
            vc.navigationItem.title = cellData.objectForKey("title") as? String
            rootTabBarController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            rootTabBarController.navigationController?.pushViewController(vc, animated: true)
        }else if type == "help_center"{
            let vc: WebViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
            vc.navigationItem.title = cellData.objectForKey("title") as? String
            vc.link = NSURL(string: DataManager.domain().root + "apphtml/help.html")
            //vc.link = NSURL(string: "https://google.com")
            rootTabBarController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            rootTabBarController.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - onCloseTap
    func onCloseTap(sender: UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
