//
//  DashboardViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/12/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import Charts

class DashboardViewController: UIViewController {

    @IBOutlet var buttonToday: UIButton!
    @IBOutlet var buttonWeek: UIButton!
    @IBOutlet var buttonMonth: UIButton!
    @IBOutlet var buttonAlltime: UIButton!
    @IBOutlet var pieChartView: PieChartView!
    @IBOutlet var pendingNumber: UILabel!
    @IBOutlet var ordersNumber: UILabel!
    @IBOutlet var ratingsNumber: UILabel!
    @IBOutlet var balanceNumber: UILabel!
    @IBOutlet var bottomView: UIView!
    
    var activityView: UICustomActivityView!
    var navigationTitle: String?
    
    var selectedTopButton: UIButton?
    
    var statsData: NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.navigationController?.navigationBar.topItem!.title = ""
        
        selectedTopButton = buttonToday
        
        pieChartView.noDataText = NSLocalizedString("Label_Dashboard_No_ChartData", comment: "No chart data available")
        pieChartView.backgroundColor = UIColor.clearColor()
        pieChartView.rotationAngle = 250
        pieChartView.transparentCircleRadiusPercent = 0
        pieChartView.userInteractionEnabled = false
        pieChartView.legend.enabled = false
        pieChartView.holeRadiusPercent = 0.58
        pieChartView.holeColor = UIColor(rgba: "EFECD9")
        pieChartView.drawHoleEnabled = true // draw hole in the center of chart
        pieChartView.centerText = selectedTopButton?.titleLabel?.text
        pieChartView.centerTextColor = UIColor(rgba: "5e5d56")
        pieChartView.centerTextFont = UIFont.boldSystemFontOfSize(20)
        pieChartView.descriptionText = ""
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        bottomView.removeFromSuperview()
        loadStatistics()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationTitle != nil{
            self.navigationItem.title = navigationTitle!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onTopButtonTap(sender: UIButton) {
        selectedTopButton?.selected = false
        
        pieChartView.centerText = sender.titleLabel?.text
        sender.selected = true
        selectedTopButton = sender
        
        if sender.tag == 1{
            if let dayStat: NSDictionary = statsData?.objectForKey("day") as? NSDictionary{
                let otw: Double = floor((dayStat.objectForKey("on_the_way") as! NSNumber).doubleValue * 100) / 100
                let acc: Double = floor((dayStat.objectForKey("accepted") as! NSNumber).doubleValue * 100) / 100
                let dlv: Double = floor((dayStat.objectForKey("delivered") as! NSNumber).doubleValue * 100) / 100
                let pen: Double = floor((dayStat.objectForKey("pending") as! NSNumber).doubleValue * 100) / 100
                
                let pdata: [String] = ["\(otw)%", "\(pen)%", "\(acc)%", "\(dlv)%"]
                let idata: [Double] = [otw, pen, acc, dlv]
                let colors: [UIColor] = [UIColor(rgba: "026e7b"), UIColor(rgba: "de3929"), UIColor(rgba: "0aacf1"), UIColor(rgba: "f6ab1c")]
                
                if otw > 0 || pen > 0 || acc > 0 || dlv > 0{
                    self.setChart(pdata, values: idata, colors: colors)
                }else{
                    pieChartView.clear()
                }
            }
        }else if sender.tag == 2{
            if let dayStat: NSDictionary = statsData?.objectForKey("week") as? NSDictionary{
                let otw: Double = floor((dayStat.objectForKey("on_the_way") as! NSNumber).doubleValue * 100) / 100
                let acc: Double = floor((dayStat.objectForKey("accepted") as! NSNumber).doubleValue * 100) / 100
                let dlv: Double = floor((dayStat.objectForKey("delivered") as! NSNumber).doubleValue * 100) / 100
                let pen: Double = floor((dayStat.objectForKey("pending") as! NSNumber).doubleValue * 100) / 100
                
                let pdata: [String] = ["\(otw)%", "\(pen)%", "\(acc)%", "\(dlv)%"]
                let idata: [Double] = [otw, pen, acc, dlv]
                let colors: [UIColor] = [UIColor(rgba: "026e7b"), UIColor(rgba: "de3929"), UIColor(rgba: "0aacf1"), UIColor(rgba: "f6ab1c")]
                
                if otw > 0 || pen > 0 || acc > 0 || dlv > 0{
                    self.setChart(pdata, values: idata, colors: colors)
                }else{
                    pieChartView.clear()
                }
            }
        }else if sender.tag == 3{
            if let dayStat: NSDictionary = statsData?.objectForKey("month") as? NSDictionary{
                let otw: Double = floor((dayStat.objectForKey("on_the_way") as! NSNumber).doubleValue * 100) / 100
                let acc: Double = floor((dayStat.objectForKey("accepted") as! NSNumber).doubleValue * 100) / 100
                let dlv: Double = floor((dayStat.objectForKey("delivered") as! NSNumber).doubleValue * 100) / 100
                let pen: Double = floor((dayStat.objectForKey("pending") as! NSNumber).doubleValue * 100) / 100
                
                let pdata: [String] = ["\(otw)%", "\(pen)%", "\(acc)%", "\(dlv)%"]
                let idata: [Double] = [otw, pen, acc, dlv]
                let colors: [UIColor] = [UIColor(rgba: "026e7b"), UIColor(rgba: "de3929"), UIColor(rgba: "0aacf1"), UIColor(rgba: "f6ab1c")]
                
                if otw > 0 || pen > 0 || acc > 0 || dlv > 0{
                    self.setChart(pdata, values: idata, colors: colors)
                }else{
                    pieChartView.clear()
                }
            }
        }else if sender.tag == 4{
            if let dayStat: NSDictionary = statsData?.objectForKey("all") as? NSDictionary{
                let otw: Double = floor((dayStat.objectForKey("on_the_way") as! NSNumber).doubleValue * 100) / 100
                let acc: Double = floor((dayStat.objectForKey("accepted") as! NSNumber).doubleValue * 100) / 100
                let dlv: Double = floor((dayStat.objectForKey("delivered") as! NSNumber).doubleValue * 100) / 100
                let pen: Double = floor((dayStat.objectForKey("pending") as! NSNumber).doubleValue * 100) / 100
                
                let pdata: [String] = ["\(otw)%", "\(pen)%", "\(acc)%", "\(dlv)%"]
                let idata: [Double] = [otw, pen, acc, dlv]
                let colors: [UIColor] = [UIColor(rgba: "026e7b"), UIColor(rgba: "de3929"), UIColor(rgba: "0aacf1"), UIColor(rgba: "f6ab1c")]
                
                if otw > 0 || pen > 0 || acc > 0 || dlv > 0{
                    self.setChart(pdata, values: idata, colors: colors)
                }else{
                    pieChartView.clear()
                }
            }
        }
    }
    
    func setChart(dataPoints: [String], values: [Double], colors: [UIColor]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: nil)
        pieChartDataSet.drawValuesEnabled = false // show hide values label
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        //pieChartView.drawSliceTextEnabled = false // removes the xAxis labels
        
        pieChartView.data = pieChartData
        
        pieChartDataSet.colors = colors
    }

    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    func loadStatistics(){
        activityView.showActivityIndicator()
        var url: String = "api/stats/?"
        let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let user_type: String = prefs.objectForKey("user_type") as! String
        let id: NSNumber = prefs.objectForKey("id") as! NSNumber
        url += "user=\(id)"
        if user_type == "store_owner"{
            url += "&type=owner"
        }else if user_type == "driver"{
            url += "&type=driver"
        }
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                    self.statsData = json
                    
                    //if let bl: NSNumber = json.objectForKey("credits") as? NSNumber{
                        //self.balanceNumber.text = "\(bl)"
                    //}
                    
                    if let dayStat: NSDictionary = json.objectForKey("day") as? NSDictionary{
                        let otw: Double = floor((dayStat.objectForKey("on_the_way") as! NSNumber).doubleValue * 100) / 100
                        let acc: Double = floor((dayStat.objectForKey("accepted") as! NSNumber).doubleValue * 100) / 100
                        let dlv: Double = floor((dayStat.objectForKey("delivered") as! NSNumber).doubleValue * 100) / 100
                        let pen: Double = floor((dayStat.objectForKey("pending") as! NSNumber).doubleValue * 100) / 100
                        
                        let pdata: [String] = ["\(otw)%", "\(pen)%", "\(acc)%", "\(dlv)%"]
                        let idata: [Double] = [otw, pen, acc, dlv]
                        let colors: [UIColor] = [UIColor(rgba: "026e7b"), UIColor(rgba: "de3929"), UIColor(rgba: "0aacf1"), UIColor(rgba: "f6ab1c")]
                        if otw > 0 || pen > 0 || acc > 0 || dlv > 0{
                            self.setChart(pdata, values: idata, colors: colors)
                        }else{
                            self.pieChartView.clear()
                        }
                    }
                }catch{
                    print(error)
                }
                
            }
        }
    }
}
