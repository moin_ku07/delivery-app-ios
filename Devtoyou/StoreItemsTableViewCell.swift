//
//  StoreItemsTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/1/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import SDWebImage

@objc protocol StoreItemsTableViewCellDelegate{
    func onAddtocartTap(cell: StoreItemsTableViewCell)
    optional func onDetailTextTap(cell: StoreItemsTableViewCell)
}

class StoreItemsTableViewCell: UITableViewCell {
    
    var delegate: StoreItemsTableViewCellDelegate?
    
    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var details: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var addtocartButton: UIButton!
    @IBOutlet var thumbActivity: UIActivityIndicatorView!
    
    var thumbUrl: NSURL?{
        didSet{
            /*let delay = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(delay){
                do{
                    let imageData: NSData = try NSData(contentsOfURL: self.thumbUrl!, options: NSDataReadingOptions())
                    dispatch_async(dispatch_get_main_queue()){
                        self.thumbActivity.stopAnimating()
                        self.thumbnail.image = UIImage(data: imageData)
                    }
                }catch{
                    print("Thumb fetch error")
                }
            }*/
            thumbnail.sd_setImageWithURL(thumbUrl) { (image:UIImage!, error: NSError!, cache: SDImageCacheType, url: NSURL!) -> Void in
                self.thumbActivity.stopAnimating()
            }
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        title.text = ""
        details.text = ""
        price.text = ""
        
        if thumbnail.image == nil{
            thumbActivity.startAnimating()
        }else{
            thumbActivity.stopAnimating()
        }
        
        thumbnail.userInteractionEnabled = true
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onImageTap))
        tapGesture.numberOfTapsRequired = 1
        thumbnail.addGestureRecognizer(tapGesture)
        
        let detailTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onDetailTap(_:)))
        detailTapGesture.numberOfTapsRequired = 1
        details.userInteractionEnabled = true
        details.addGestureRecognizer(detailTapGesture)
    }
    
    func onImageTap(){
        if thumbnail.image != nil{
            DVImagePopup(image: thumbnail.image!, sourceView: (UIApplication.sharedApplication().delegate as! AppDelegate).window!)
        }
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onCartTap(sender: UIButton) {
        //print("add cart tap")
        delegate?.onAddtocartTap(self)
    }
    
    func onDetailTap(recognizer: UITapGestureRecognizer){
        delegate?.onDetailTextTap?(self)
    }
}
