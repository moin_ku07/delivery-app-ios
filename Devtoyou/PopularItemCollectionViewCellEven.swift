//
//  PopularItemCollectionViewCellEven.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/7/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RateView
import SDWebImage

class PopularItemCollectionViewCellEven: UICollectionViewCell {
    
    @IBOutlet var borderImage: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var borderView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var rateView: RateView!
    
    var selectedCell: Bool = false{
        didSet{
            if self.selectedCell{
                borderImage.image = UIImage(named: "icon-rect-red-up")
                borderView.layer.borderColor = UIColor(rgba: "de3929").CGColor
            }else{
                borderImage.image = UIImage(named: "icon-rect-grey-up")
                borderView.layer.borderColor = UIColor(rgba: "bcbaad").CGColor
            }
        }
    }
    
    var imageUrl: NSURL?{
        didSet{
            /*let delay = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
            dispatch_async(delay){
                do{
                    let imageData: NSData = try NSData(contentsOfURL: self.imageUrl!, options: NSDataReadingOptions())
                    dispatch_async(dispatch_get_main_queue()){
                        self.activityIndicator.stopAnimating()
                        self.thumbnail.image = UIImage(data: imageData)
                    }
                }catch{
                    dispatch_async(dispatch_get_main_queue()){
                        self.activityIndicator.stopAnimating()
                    }
                    print("Thumb fetch error")
                }
            }*/
            thumbnail.sd_setImageWithURL(imageUrl) { (image:UIImage!, error: NSError!, cache: SDImageCacheType, url: NSURL!) -> Void in
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rateView.starFillColor = UIColor(rgba: "de3929")
        rateView.starBorderColor = UIColor(rgba: "dc3928")
        rateView.starNormalColor = UIColor(rgba: "efecd9")
        rateView.starSize = 10
        rateView.canRate = false
        
    }
}
