//
//  PopularItemViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/6/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RateView
import CoreData

class PopularItemViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var itemContainer: UIView!
    @IBOutlet var popContentView: UIView!
    @IBOutlet var popImage: UIImageView!
    @IBOutlet var popName: UILabel!
    @IBOutlet var popStore: UILabel!
    @IBOutlet var popRateView: RateView!
    @IBOutlet var popDetails: UILabel!
    @IBOutlet var popPrice: UILabel!
    
    var collectionView: UICollectionView!
    var tableData: NSMutableArray = NSMutableArray()
    
    var selectedIndexPath: NSIndexPath?
    
    var activityView: UICustomActivityView!
    let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext("Devtoyou")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)

        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        flowLayout.itemSize = CGSizeMake(80, 200)
        collectionView = UICollectionView(frame: itemContainer.frame, collectionViewLayout: flowLayout)
        
        itemContainer.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let vdict: [String: AnyObject] = ["collectionView": collectionView, "view": itemContainer]
        itemContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[collectionView]-5-|", options: NSLayoutFormatOptions(), metrics: nil, views: vdict))
        itemContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[collectionView]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: vdict))
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.registerNib(UINib(nibName: "PopularItemCollectionViewCellOdd", bundle: nil), forCellWithReuseIdentifier: "Cell")
        collectionView.registerNib(UINib(nibName: "PopularItemCollectionViewCellEven", bundle: nil), forCellWithReuseIdentifier: "CellEven")
        
        collectionView.backgroundColor = itemContainer.backgroundColor
        
        //selectedIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        collectionView.reloadData()
        
        popContentView.hidden = true
        popName.text = ""
        popStore.text = ""
        popDetails.text = ""
        popPrice.text = ""
        
        popRateView.canRate = false
        popRateView.starFillColor = UIColor(rgba: "dc3928")
        popRateView.starBorderColor = UIColor(rgba: "dc3928")
        popRateView.starNormalColor = UIColor(rgba: "efecd9")
        popRateView.starSize = 15
        
        popImage.userInteractionEnabled = true
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onImageTap))
        tapGesture.numberOfTapsRequired = 1
        popImage.addGestureRecognizer(tapGesture)
        
        loadTableData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell!
        
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        if indexPath.row % 2 == 0{
            let cell1: PopularItemCollectionViewCellOdd = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! PopularItemCollectionViewCellOdd
            if selectedIndexPath == indexPath{
                cell1.selectedCell = true
            }else{
                cell1.selectedCell = false
            }
            
            cell1.title.text = cellData.objectForKey("name") as? String
            
            popPrice.text = ""
            if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                cell1.price.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    popPrice.text = price.stringValue + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                }
            }
            
            if let photourl: String = cellData.objectForKey("photo") as? String{
                let url: NSURL = NSURL(string: photourl)!
                cell1.imageUrl = url
            }
            if let rating: NSNumber = cellData.objectForKey("avg_rating") as? NSNumber{
                cell1.rateView.rating = rating.floatValue
            }else if let rating: NSString = cellData.objectForKey("avg_rating") as? NSString{
                cell1.rateView.rating = rating.floatValue
            }
            
            cell = cell1
        }else{
            let cell1: PopularItemCollectionViewCellEven = collectionView.dequeueReusableCellWithReuseIdentifier("CellEven", forIndexPath: indexPath) as! PopularItemCollectionViewCellEven
            if selectedIndexPath == indexPath{
                cell1.selectedCell = true
            }else{
                cell1.selectedCell = false
            }
            
            cell1.title.text = cellData.objectForKey("name") as? String
            
            popPrice.text = ""
            if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                cell1.price.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    popPrice.text = price.stringValue + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                }
            }
            
            if let photourl: String = cellData.objectForKey("photo") as? String{
                let url: NSURL = NSURL(string: photourl)!
                cell1.imageUrl = url
            }
            if let rating: NSNumber = cellData.objectForKey("avg_rating") as? NSNumber{
                cell1.rateView.rating = rating.floatValue
            }else if let rating: NSString = cellData.objectForKey("avg_rating") as? NSString{
                cell1.rateView.rating = rating.floatValue
            }
            
            cell = cell1
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        if selectedIndexPath != nil{
            if let cell: PopularItemCollectionViewCellOdd = collectionView.cellForItemAtIndexPath(selectedIndexPath!) as? PopularItemCollectionViewCellOdd{
                cell.selectedCell = false
            }else if let cell: PopularItemCollectionViewCellEven = collectionView.cellForItemAtIndexPath(selectedIndexPath!) as? PopularItemCollectionViewCellEven{
                cell.selectedCell = false
            }
        }
        
        if let cell: PopularItemCollectionViewCellOdd = collectionView.cellForItemAtIndexPath(indexPath) as? PopularItemCollectionViewCellOdd{
            cell.selectedCell = true
            
            popContentView.hidden = false
            popName.text = cell.title.text
            popImage.image = cell.thumbnail.image
            popRateView.rating = cell.rateView.rating
            
            popStore.text = cellData.objectForKey("store_name") as? String
            popDetails.text = cellData.objectForKey("description") as? String
            popPrice.text = ""
            if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                popPrice.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    popPrice.text = price.stringValue + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                }
            }
        }else if let cell: PopularItemCollectionViewCellEven = collectionView.cellForItemAtIndexPath(indexPath) as? PopularItemCollectionViewCellEven{
            cell.selectedCell = true
            
            popContentView.hidden = false
            popName.text = cell.title.text
            popImage.image = cell.thumbnail.image
            popRateView.rating = cell.rateView.rating
            
            popStore.text = cellData.objectForKey("store_name") as? String
            popDetails.text = cellData.objectForKey("description") as? String
            popPrice.text = ""
            if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                popPrice.text = NSLocalizedString("Label_Currency_Code", comment: "Currency Code") + " " + price.stringValue
                if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
                    popPrice.text = price.stringValue + " " + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                }
            }
        }
        
        selectedIndexPath = indexPath
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - loadTableData
    func loadTableData(){
        if tableData.count == 0{
            activityView.showActivityIndicator()
        }
        DataManager.loadDataAsyncWithCallback("api/store-items/?favorite=true") { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    self.tableData = NSMutableArray(array: json)
                    self.collectionView?.reloadData()
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
    }
    
    
    @IBAction func onAddToCartTap(sender: UIButton) {
        if let indexPath: NSIndexPath = selectedIndexPath{
            let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
            let cart: Carts = CoreDataHelper.insertManagedObject(NSStringFromClass(Carts), managedObjectContext: moc) as! Carts
            
            cart.id = NSNumber(double: NSDate().timeIntervalSince1970 * 1000000).stringValue
            cart.name = cellData.objectForKey("name") as? String
            cart.details = cellData.objectForKey("description") as? String
            cart.item_id = cellData.objectForKey("id") as? NSNumber
            cart.quantity = 1
            if let price: NSNumber = cellData.objectForKey("price") as? NSNumber{
                cart.price = NSDecimalNumber(decimal: price.decimalValue)
            }
            cart.photo = cellData.objectForKey("photo") as? String
            cart.store_id = cellData.objectForKey("store") as? NSNumber
            cart.store_name = cellData.objectForKey("store_name") as? String
            
            
            let success: Bool = CoreDataHelper.saveManagedObjectContext(moc)
            if success{
                //AlertManager.showAlert(self, title: NSLocalizedString("Label_Success", comment: "Success"), message: NSLocalizedString("Label_Item_Added_Cart", comment: "Item added to cart"), buttonNames: nil, completion: nil)
                sender.setTitle(NSLocalizedString("Label_Added", comment: "Added"), forState: UIControlState.Normal)
                if let tvc: TabBarController = ((UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController)?.viewControllers[0] as? TabBarController{
                    var placeOrderVC: CartTableViewController?
                    for vc in tvc.viewControllers!{
                        if let nvc: CartTableViewController = vc as? CartTableViewController{
                            placeOrderVC = nvc
                        }
                    }
                    if placeOrderVC != nil{
                        var badgeNumber: NSNumber = 1
                        if let badgeString: String = placeOrderVC?.tabBarItem.badgeValue{
                            badgeNumber = NSNumber(int: (badgeString as NSString).intValue + badgeNumber.intValue)
                        }
                        print(badgeNumber)
                        placeOrderVC?.tabBarItem.badgeValue = badgeNumber.stringValue
                    }
                }
                
            }else{
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Item_Not_Added_Cart", comment: "Item added to cart"), buttonNames: nil, completion: nil)
            }
        }
    }
    
    func onImageTap(){
        if popImage.image != nil{
            DVImagePopup(image: popImage.image!, sourceView: (UIApplication.sharedApplication().delegate as! AppDelegate).window!)
        }
    }

}
