//
//  AccountSettingsViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/30/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AccountSettingsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var formcontainer1: UIView!
    @IBOutlet var firstnameField: UITextField!
    @IBOutlet var lastnameField: UITextField!
    @IBOutlet var lastnameBorder: UIView!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var changePasswordButton: UIButton!
    @IBOutlet var emailEditButton: UIButton!
    @IBOutlet var emailEditContainer: UIView!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var selectGender: UILabel!
    @IBOutlet var genderView: UIView!
    @IBOutlet var notificationSwitch: UISwitch!
    
    let genderArrayKey: [String] = ["M", "F", "O"]
    let genderArray: [String] = ["ذكر", "أنثى","Other"]
    var selectedGenderKey: String?{
        didSet{
            self.selectGender.text = self.genderArray[self.genderArrayKey.indexOf(self.selectedGenderKey!)!]
        }
    }
    
    var textFieldFocused: Bool = false
    var user_id: String?
    var user_type: String?
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var navigationTitle: String?
    let regex: NSPredicate = NSPredicate(format:"SELF MATCHES %@", "^(facebook|twitter|google\\+)_[a-zA-Z0-9]+$")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = NSLocalizedString("Label_AS_Title", comment: "Account Settings")
        
        notificationSwitch.tintColor = UIColor(rgba: "4E4040")
        
        if let user_type: String = prefs.objectForKey("user_type") as? String{
            self.user_type = user_type
        }
        
        if let id: NSNumber = prefs.objectForKey("id") as? NSNumber{
            user_id = id.stringValue
        }
        if let firstname: String = prefs.objectForKey("first_name") as? String where firstname != "N/A" && user_type != "store_owner"{
            firstnameField.text = firstname
        }
        if let lastname: String = prefs.objectForKey("last_name") as? String where lastname != "N/A" && user_type != "store_owner"{
            lastnameField.text = lastname
        }
        
        if user_type == "store_owner"{
            lastnameBorder.hidden = true
            lastnameField.hidden = true
            firstnameField.placeholder = NSLocalizedString("Label_Store_Name", comment: "Store name")
            genderView.hidden = true
            scrollView.addConstraint(NSLayoutConstraint(item: genderView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 0))
            scrollView.addConstraint(NSLayoutConstraint(item: formcontainer1, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 54))
            
            DataManager.loadDataAsyncWithCallback("api/stores/" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "/", completion: { (data, error) -> Void in
                if data != nil{
                    do{
                        let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        //print(json)
                        if let name: String = json.objectForKey("name") as? String where name != "N/A"{
                            self.firstnameField.text = name
                        }
                    }catch{
                        print("Unknow error")
                    }
                }else{
                    print(error?.localizedDescription)
                }
            })
        }/*else if user_type == "driver"{
            firstnameField.userInteractionEnabled = false
            lastnameField.userInteractionEnabled = false
        }*/
        
        if self.prefs.objectForKey("social") as? String != nil{
            changePasswordButton.hidden = true
            emailEditButton.hidden = true
        }
        if let username: String = prefs.objectForKey("username") as? String  where username != "N/A" || !self.regex.evaluateWithObject(username){
            self.phoneField.text = username
        }
        
        if let notificationEnabled: Bool = self.prefs.objectForKey("notification") as? Bool{
            notificationSwitch.on = notificationEnabled
        }
        
        DataManager.loadDataAsyncWithCallback("auth/profile", completion: { (data, error) -> Void in
            if data != nil{
                do{
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    print(json)
                    if let id: NSNumber = json.objectForKey("id") as? NSNumber{
                        self.user_id = id.stringValue
                        self.prefs.setValue(id, forKey: "id")
                        self.prefs.synchronize()
                    }
                    if let email: String = json.objectForKey("email") as? String where email != "N/A"{
                        self.emailField.text = email
                    }
                    
                    if let username: String = json.objectForKey("username") as? String  where username != "N/A" && !self.regex.evaluateWithObject(username){
                        self.phoneField.text = username
                    }
                    if let gender: String = json.objectForKey("gender") as? String where gender != ""{
                        self.selectedGenderKey = gender
                    }
                }catch{
                    print("Unknow error")
                }
            }else{
                print(error?.localizedDescription)
            }
        })
        
        for subview in scrollView.subviews{
            if subview.isKindOfClass(UIView.self){
                for sv in subview.subviews{
                    if let textField: UITextField = sv as? UITextField{
                        textField.delegate = self
                    }
                }
            }
            if let textField: UITextField = subview as? UITextField{
                textField.delegate = self
            }
        }
        
        // scrollview tap event
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:)))
        tap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tap)
        
        let gTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onGenderViewTap))
        gTap.numberOfTapsRequired = 1
        genderView.addGestureRecognizer(gTap)
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // check userInterfaceLayoutDirection for Internationalizing
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft{
            firstnameField.textAlignment = .Right
            lastnameField.textAlignment = .Right
            phoneField.textAlignment = .Right
            emailField.textAlignment = .Right
            changePasswordButton.contentHorizontalAlignment = .Right
            phoneLabel.textAlignment = .Right
            emailLabel.textAlignment = .Right
        }
        
        if navigationTitle != nil{
            self.navigationItem.title = navigationTitle!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - ContainerView tap handle
    
    func onViewTap(sender: UIView){
        if textFieldFocused{
            textFieldFocused = false
            for subview in scrollView.subviews{
                if subview.isKindOfClass(UIView.self){
                    for sv in subview.subviews{
                        if let textField: UITextField = sv as? UITextField{
                            textField.resignFirstResponder()
                        }
                    }
                }
                if let textField: UITextField = subview as? UITextField{
                    textField.resignFirstResponder()
                }
            }
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        textFieldFocused = true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    // MARK: -onSubmitTap
    @IBAction func onSubmitTap(sender: UIButton) {
        onViewTap(scrollView)
        if user_type == "store_owner"{
            if firstnameField.text == ""{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: "أدخل اسم المتجر", buttonNames: nil, completion: nil)
            }else{
                activityView.showActivityIndicator()
                let url: String = "api/stores/" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "/"
                let postdata: NSDictionary = ["name": firstnameField.text!]
                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postdata, completion: { (data, error) -> Void in
                    self.activityView.hideActivityIndicator()
                    if data != nil{
                        self.prefs.setValue(self.firstnameField.text!, forKey: "store_name")
                        self.prefs.synchronize()
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                        AlertManager.showAlert(self, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Info_Updated", comment: "Info updated"), buttonNames: nil, completion: nil)
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }
        }else{
            if firstnameField.text == ""{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Valid_Firstname", comment: "Valid first no"), buttonNames: nil, completion: nil)
            }/*else if lastnameField.text == ""{
                AlertManager.showAlert(self, title: NSLocalizedString("Warning", comment: "Warning"), message: NSLocalizedString("Label_Valid_Lastname", comment: "Valid last no"), buttonNames: nil, completion: nil)
            }*/else{
                activityView.showActivityIndicator()
                var url: String = "api/"
                if user_type == "customer"{
                    url += "customers/" + String(user_id!) + "/"
                }else if user_type == "driver"{
                    url += "drivers/" + String(user_id!) + "/"
                }
                let postdata: NSMutableDictionary = ["first_name": firstnameField.text!, "last_name": lastnameField.text!]
                if self.selectedGenderKey != nil{
                    postdata.setValue(self.selectedGenderKey!, forKey: "gender")
                }
                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postdata, completion: { (data, error) -> Void in
                    self.activityView.hideActivityIndicator()
                    if data != nil{
                        self.prefs.setValue(self.firstnameField.text!, forKey: "first_name")
                        self.prefs.setValue(self.lastnameField.text!, forKey: "last_name")
                        self.prefs.synchronize()
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                        AlertManager.showAlert(self, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Info_Updated", comment: "Info updated"), buttonNames: nil, completion: nil)
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }
        }
    }
    
    // MARK: - onPhoneEditTap
    @IBAction func onPhoneEditTap(sender: UIButton) {
        let alertController: UIAlertController = UIAlertController(title: NSLocalizedString("Label_Phone_No", comment: "Alert"), message: NSLocalizedString("Label_Valid_Phone", comment: "Valid phone message"), preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler({ (textField: UITextField) -> Void in
            textField.placeholder = NSLocalizedString("Label_Phone_No_Format", comment: "Phone placeholder")
            textField.textAlignment = .Natural
        })
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Cancel", comment: "Cancel"), style: UIAlertActionStyle.Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Submit", comment: "Submit"), style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in
            if let phoneNo: String = alertController.textFields?.first?.text where phoneNo != "" && FormValidation.isValidPhone(phoneNo, minLength: 3, maxLength: 12, country: "KSA"){
                self.activityView.showActivityIndicator()
                var url: String = "api/"
                if self.user_type == "customer"{
                    url += "customers/" + String(self.user_id!) + "/"
                }
                let postdata: NSDictionary = ["username": alertController.textFields![0].text!]
                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postdata, completion: { (data, error) -> Void in
                    self.activityView.hideActivityIndicator()
                    if data != nil{
                        if error == nil{
                            self.phoneField.text = alertController.textFields![0].text
                            if self.prefs.objectForKey("social") as? String == nil{
                                print("non social username update")
                                self.prefs.setValue(alertController.textFields![0].text!, forKey: "username")
                                self.prefs.synchronize()
                                AlertManager.showAlert(self, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Info_Updated", comment: "Info updated"), buttonNames: nil, completion: nil)
                            }
                        }else{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Phone_In_use", comment: "Phone no in use"), buttonNames: nil, completion: nil)
                            print("username update error")
                        }
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }else{
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Valid_Phone", comment: "Valid phone no"), buttonNames: nil, completion: nil)
            }
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - onEmailEditTap
    @IBAction func onEmailEditTap(sender: UIButton) {
        let alertController: UIAlertController = UIAlertController(title: NSLocalizedString("Label_Enter_Email", comment: "Enter Email"), message: NSLocalizedString("Label_Valid_Email", comment: "Valid email"), preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler({ (textField: UITextField) -> Void in
            textField.placeholder = "example@gmail.com"
            textField.textAlignment = .Natural
        })
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Cancel", comment: "Cancel"), style: UIAlertActionStyle.Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Submit", comment: "Submit"), style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in
            if let emailText: String = alertController.textFields?.first?.text where emailText != "" && FormValidation.isValidEmail(emailText){
                self.activityView.showActivityIndicator()
                var url: String = "api/"
                if self.user_type == "customer"{
                    url += "customers/" + String(self.user_id!) + "/"
                }
                let postdata: NSDictionary = ["email": alertController.textFields![0].text!]
                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postdata, completion: { (data, error) -> Void in
                    self.activityView.hideActivityIndicator()
                    if data != nil{
                        if error == nil{
                            self.emailField.text = alertController.textFields![0].text
                            AlertManager.showAlert(self, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Info_Updated", comment: "Info updated"), buttonNames: nil, completion: nil)
                        }else{
                            print("email update error")
                        }
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }else{
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Valid_Email", comment: "Valid email"), buttonNames: nil, completion: nil)
            }
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - onChangePasswordTap
    @IBAction func onChangePasswordTap(sender: UIButton) {
        let alertController: UIAlertController = UIAlertController(title: NSLocalizedString("Label_Enter_Password", comment: "Enter Password"), message: NSLocalizedString("Label_New_Password", comment: "Enter new password"), preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler({ (textField: UITextField) -> Void in
            textField.placeholder = NSLocalizedString("Label_Type_Password", comment: "Type password")
            textField.secureTextEntry = true
            textField.textAlignment = .Natural
        })
        alertController.addTextFieldWithConfigurationHandler({ (textField: UITextField) -> Void in
            textField.placeholder = NSLocalizedString("Label_Retype_Password", comment: "Re-type password")
            textField.secureTextEntry = true
            textField.textAlignment = .Natural
        })
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Cancel", comment: "Cancel"), style: UIAlertActionStyle.Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Label_Submit", comment: "Submit"), style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in
            if alertController.textFields?.first?.text != "" && alertController.textFields?.last?.text != "" && alertController.textFields?.first?.text == alertController.textFields?.last?.text{
                self.activityView.showActivityIndicator()
                var url: String = "api/"
                if self.user_type == "customer"{
                    url += "customers/" + String(self.user_id!) + "/"
                }
                let postdata: NSDictionary = ["password": alertController.textFields![0].text!]
                DataManager.postDataAsyncWithCallback(url, method: "PATCH", data: postdata, completion: { (data, error) -> Void in
                    self.activityView.hideActivityIndicator()
                    if data != nil{
                        if error == nil{
                            print("non social password update")
                            self.prefs.setValue(alertController.textFields![0].text!, forKey: "password")
                            self.prefs.synchronize()
                            AlertManager.showAlert(self, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Info_Updated", comment: "Info updated"), buttonNames: nil, completion: nil)
                        }else{
                            print("password update error")
                        }
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }else{
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_PW_Match_Error", comment: "Both password did not match"), buttonNames: nil, completion: nil)
            }
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - onGenderViewTap
    func onGenderViewTap(){
        var inititalSelection: Int = 0
        if self.selectedGenderKey != nil{
            inititalSelection = self.genderArrayKey.indexOf(self.selectedGenderKey!)!
        }
        ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Gender", comment: "Select Gender"), rows: genderArray, initialSelection: inititalSelection, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
            self.selectedGenderKey = self.genderArrayKey[selectedIndex]
            }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                print("nothing")
            }, origin: self.genderView)
    }
    
    @IBAction func onNotificationSwitchChange(sender: UISwitch) {
        if sender.on == true{
            // unregister push notification first
            if let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate{
                appDelegate.unregisterDevice({ (data, error) in
                    // register for push notification
                    if UIApplication.sharedApplication().respondsToSelector(#selector(UIApplication.registerUserNotificationSettings(_:))){
                        let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert,UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
                        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
                        UIApplication.sharedApplication().registerForRemoteNotifications()
                    }
                })
            }
        }else{
            self.activityView.showActivityIndicator()
            (UIApplication.sharedApplication().delegate as! AppDelegate).unregisterDevice { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                self.prefs.setValue(false, forKey: "notification")
                self.prefs.synchronize()
            }
        }
    }
    
}
