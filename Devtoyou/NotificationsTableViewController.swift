//
//  NotificationsTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/24/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class NotificationsTableViewController: UITableViewController {
    
    var tableData: NSMutableArray = NSMutableArray()
    var activityView: UICustomActivityView!
    var noDataMessageLabel: UILabel?
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle

        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadTableData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableData.count > 0{
            self.noDataMessageLabel?.hidden = true
        }else{
            if self.noDataMessageLabel == nil{
                self.noDataMessageLabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width,
                    self.tableView.bounds.size.height))
                
                self.noDataMessageLabel!.text = "لا توجد تنبيهات"
                self.noDataMessageLabel!.textColor = UIColor(rgba: "928a81")
                //center the text
                self.noDataMessageLabel!.textAlignment = NSTextAlignment.Center
                //auto size the text
                self.noDataMessageLabel!.sizeToFit()
                //set back to label view
                self.tableView.backgroundView = self.noDataMessageLabel!
                self.noDataMessageLabel?.hidden = true
                //print("label added")
            }else{
                self.noDataMessageLabel?.hidden = false
            }
        }
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: NotificationsTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! NotificationsTableViewCell
        cell.selectionStyle = .None
        
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        let formattedString: NSMutableAttributedString = NSMutableAttributedString()
        let newStr = NSAttributedString(string: cellData.objectForKey("message") as! String, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(11)])
        formattedString.appendAttributedString(newStr)
        
        cell.titleLabel.text = NSLocalizedString("Label_Order_Update", comment: "Order Update")
        cell.textView.textAlignment = .Natural
        cell.textView.attributedText = formattedString
        cell.textView.textColor = UIColor(rgba: "8B837B")
        dispatch_async(dispatch_get_main_queue(), {
            cell.textView.contentOffset = CGPointZero
        })
        
        if let datetimeString: String = cellData.objectForKey("created_at") as? String{
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let datetime: NSDate = dateFormatter.dateFromString(datetimeString){
                var currenttime: NSDate = NSDate()
                let stringTime: String = dateFormatter.stringFromDate(currenttime)
                //print("stringTime: \(stringTime)")
                currenttime = dateFormatter.dateFromString(stringTime)!
                //print("currenttime: \(currenttime)")
                let interval: NSTimeInterval = currenttime.timeIntervalSinceDate(datetime)
                //println(interval)
                if interval > 0 && interval < 60{
                    if interval < 1{
                        cell.timeLabel.text = numberFormatter.stringFromNumber(interval)! + " " + NSLocalizedString("Label_Time_Second", comment: "sec ago")
                    }else{
                        cell.timeLabel.text = numberFormatter.stringFromNumber(interval)! + " " + NSLocalizedString("Label_Time_Seconds", comment: "sec ago")
                    }
                }else if interval > 60 && interval < 120{
                    let intinmin: Int = Int(interval / 60)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinmin)! + " " + NSLocalizedString("Label_Time_Minute", comment: "min ago")
                }else if interval > 120 && interval < 3600{
                    let intinmin: Int = Int(interval / 60)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinmin)! + " " + NSLocalizedString("Label_Time_Minutes", comment: "min ago")
                }else if interval > 3600 && interval < 7200{
                    let intinhr: Int = Int(interval / 3600)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinhr)! + " " + NSLocalizedString("Label_Time_Hour", comment: "hour ago")
                }else if interval > 7200 && interval < 86400{
                    let intinhr: Int = Int(interval / 3600)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinhr)! + " " + NSLocalizedString("Label_Time_Hours", comment: "hours ago")
                }else if interval > 86400 && interval < 172800{
                    let intinday: Int = Int(interval / 86400)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinday)! + " " + NSLocalizedString("Label_Time_Day", comment: "day ago")
                }else if interval > 172800 && interval < 2592000{
                    let intinday: Int = Int(interval / 86400)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinday)! + " " + NSLocalizedString("Label_Time_Days", comment: "days ago")
                }else if interval > 2592000 && interval < 5184000{
                    let intinmonth: Int = Int(interval / 2592000)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinmonth)! + " " + NSLocalizedString("Label_Time_Month", comment: "month ago")
                }else if interval > 5184000 && interval < 31104000{
                    let intinmonth: Int = Int(interval / 2592000)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinmonth)! + " " + NSLocalizedString("Label_Time_Months", comment: "months ago")
                }else if interval > 31104000 && interval < 62208000{
                    let intinyear: Int = Int(interval / 31104000)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinyear)! + " " + NSLocalizedString("Label_Time_Year", comment: "year ago")
                }else if interval > 62208000{
                    let intinyear: Int = Int(interval / 31104000)
                    cell.timeLabel.text = numberFormatter.stringFromNumber(intinyear)! + " " + NSLocalizedString("Label_Time_Years", comment: "years ago")
                }
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        let vc: TrackOrderViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TrackOrderViewController") as! TrackOrderViewController
        if let string: String = cellData.objectForKey("message") as? String{
            //print(string)
            var matches: [String] = [String]()
            if let mc: [String] = matchesForRegexInText("#[0-9]+", text: string) where mc.count > 0{
                matches = mc
            }else if let mc: [String] = matchesForRegexInText("[0-9]+#", text: string) where mc.count > 0{
                matches = mc
            }
            //print(matches)
            if matches.count > 0{
                if let idString: NSString = (matches[0] as NSString).stringByReplacingOccurrencesOfString("#", withString: "") as NSString where idString.length > 8{
                    if let id: NSNumber = (idString.substringFromIndex(8) as NSString).integerValue{
                        vc.order_id = id
                        self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    

    // MARK: - loadTableData
    func loadTableData(){
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        self.tabBarItem.badgeValue = nil
        
        DataManager.postDataAsyncWithCallback("/api/notifications/mark-read/", data: ["user": prefs.objectForKey("id") as! NSNumber]) { (data, error) -> Void in
            if data != nil{
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
            }
        }
        
        var url: String = "api/user-notification/?"
        
        if prefs.objectForKey("user_type") as? String == "store_owner"{
            url = url + "store=" + (prefs.objectForKey("store_id") as! NSNumber).stringValue
        }else{
            url = url + "user=" + (prefs.objectForKey("id") as! NSNumber).stringValue
        }
    
        if tableData.count == 0{
            activityView.showActivityIndicator()
        }
        DataManager.loadDataAsyncWithCallback(url) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            if error == nil{
                do{
                    let json: NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    //print(json)
                    self.tableData = NSMutableArray(array: json)
                    self.tableView.reloadData()
                }catch{
                    print("Unknow load error")
                }
            }else{
                print(error!.localizedDescription)
            }
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - refresh
    func refresh(refreshControl: UIRefreshControl) {
        loadTableData()
        refreshControl.endRefreshing()
    }

}
