//
//  NotificationsTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/27/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var thumbImage: UIImageView!
    var datetime: NSDate?{
        didSet{
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            timeLabel.textAlignment = .Left
        }
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
