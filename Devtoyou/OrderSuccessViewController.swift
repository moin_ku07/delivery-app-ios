//
//  OrderSuccessViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/29/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class OrderSuccessViewController: UIViewController {

    @IBOutlet var orderIDLabel: UILabel!
    
    var orderID: String?{
        didSet{
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyyMMdd"
            
            var newOrderID: String = "#" + dateFormatter.stringFromDate(NSDate())
            if self.orderID!.characters.count < 6{
                let reqN: Int = 6 - self.orderID!.characters.count
                for _ in 1...reqN{
                    newOrderID += "0"
                }
            }
            newOrderID = newOrderID + self.orderID!
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 250 * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) { () -> Void in
                self.orderIDLabel.text = newOrderID
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(rgba: "000000").colorWithAlphaComponent(0.95)
        
        let closeButton: UIButton = UIButton(frame: CGRectMake(0, 0, 22, 22))
        closeButton.setBackgroundImage(UIImage(named: "icon-close"), forState: UIControlState.Normal)
        closeButton.addTarget(self, action: #selector(onCloseTap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(closeButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 22))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 22))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 30))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: -12))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - onCloseTap
    func onCloseTap(sender: UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
