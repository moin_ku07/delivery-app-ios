//
//  DataManager.swift
//  TopApps
//
//  Created by Dani Arnaout on 9/2/14.
//  Edited by Eric Cerney on 9/27/14.
//  Copyright (c) 2014 Ray Wenderlich All rights reserved.
//

import Foundation


class DataManager {
    
    struct domain {
        let root: String = "http://52.3.80.248/"
        //let root: String = "http://52.3.80.248:8080/"
        var token: String{
            get{
                let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                if let tk: String = prefs.objectForKey("token") as? String{
                    //print(tk)
                    return "Token " + tk
                }
                return ""
            }
        }
    }
    
    class func postDataAsyncWithCallback(url: NSString, data: AnyObject, json: Bool? = true, completion: (data: NSData?, error: NSError?) -> Void){
        //let nsurl:NSURL = NSURL(string: url)!
        let nsurl:NSURL = NSURL(string: (self.domain().root + (url as String)))!
        print(nsurl)
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "POST"
        
        if json == true{
            let postData: Dictionary = NSDictionary(dictionary: data as! [NSObject : AnyObject]) as Dictionary
            //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(postData, options: nil, error: &err)
            do{
                try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions.init(rawValue: 0))
            }catch let err as NSError{
                print(err)
            }catch{
                print("Unknown error _postDataAsyncWithCallback")
            }
            //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
        }else{
            let postString: NSString = data as! NSString
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                if(data != nil ) {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                            /*
                            var jsonParseError: NSError? = nil
                            let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                            print(object)
                            */
                            completion(data: data, error: nil)
                        }else{
                            let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                            completion(data: data, error: statusError)
                        }
                    }
                }else if error != nil{
                    completion(data: nil, error: error)
                }
            }
        }

    }
    
    class func postDataSyncWithCallback(url: NSString, data: AnyObject, json: Bool? = true, completion: (data: NSData?, error: NSError?) -> Void){
        //let nsurl:NSURL = NSURL(string: url)!
        let nsurl:NSURL = NSURL(string: (self.domain().root + (url as String)))!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "POST"
        print(nsurl)
        if json == true{
            let postData: Dictionary = NSDictionary(dictionary: data as! [NSObject : AnyObject]) as Dictionary
            do{
                try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions.init(rawValue: 0))
            }catch{
                print(error)
            }
            //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
        }else{
            let postString: NSString = data as! NSString
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        
        var response: NSURLResponse?
        
        do{
            let urlData: NSData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            
            if let httpResponse = response as? NSHTTPURLResponse {
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                    /*
                    var jsonParseError: NSError? = nil
                    let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                    print(object)
                    */
                    completion(data: urlData, error: nil)
                }else{
                    let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                }
            }
        }catch let err as NSError{
            completion(data: nil, error: err)
        }catch{
            completion(data: nil, error: NSError(domain: "Unknown error", code: 404, userInfo: nil))
            print("Unknown error _postDataAsyncWithCallback")
        }
        
    }
    
    class func loadDataSyncWithCallback(url: NSString, completion: (data: NSData?, error: NSError?) -> Void){
        let nsurl:NSURL = NSURL(string: (self.domain().root + (url as String)))!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = "GET"
        //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(jsonData, options: nil, error: &err)
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        print(nsurl)
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        
        var response: NSURLResponse?
        
        do{
            let urlData: NSData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            
            if let httpResponse = response as? NSHTTPURLResponse {
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                    /*
                    var jsonParseError: NSError? = nil
                    let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                    print(object)
                    */
                    completion(data: urlData, error: nil)
                }else{
                    let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                }
            }
        }catch let err as NSError{
            completion(data: nil, error: err)
        }catch{
            completion(data: nil, error: NSError(domain: "Unknown error", code: 404, userInfo: nil))
            print("Unknown error _postDataAsyncWithCallback")
        }
        
    }
    
    class func loadDataAsyncWithCallback(url: NSString, completion: (data: NSData?, error: NSError?) -> Void){
        let stringURL: NSString = NSString(format: "%@%@", self.domain().root, url)
        let safeURLString: NSString = stringURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        let nsurl:NSURL = NSURL(string: safeURLString as String)!
        print(nsurl)
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        //request.timeoutInterval = 60
        request.HTTPMethod = "GET"
        //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(jsonData, options: nil, error: &err)
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        //print("token = \(self.domain().token)")
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                //print(error?.localizedDescription)
                //print((response as? NSHTTPURLResponse)?.statusCode)
                //print(data)
                if(data != nil ) {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                            /*
                            var jsonParseError: NSError? = nil
                            let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                            print(object)
                            */
                            completion(data: data, error: nil)
                        }else{
                            let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                            completion(data: nil, error: statusError)
                        }
                    }else{
                        //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                        let statusError = NSError(domain:url as String, code:400, userInfo:[NSLocalizedDescriptionKey : NSString(data: data!, encoding: NSUTF8StringEncoding)!])
                        completion(data: nil, error: statusError)
                    }
                }else if error != nil{
                    completion(data: nil, error: error)
                }
            }
        }
        
    }
    
    class func postPhotoAsyncWithCallback(url: NSString, method: String, fieldName: NSString, data: NSData, completion: (data: NSData?, error: NSError?) -> Void){
        let nsurl:NSURL = NSURL(string: (self.domain().root + (url as String)))!
        print(nsurl)
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        
        request.HTTPMethod = method
        
        let boundary = NSString(format: "---------------------------14737809831466499882746641449")
        let contentType = NSString(format: "multipart/form-data; boundary=%@",boundary)
        //  println("Content Type \(contentType)")
        request.addValue(contentType as String, forHTTPHeaderField: "Content-Type")
        
        let body: NSMutableData = NSMutableData()
        
        // Image
        body.appendData(NSString(format: "\r\n--%@\r\n", boundary).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(NSString(format:"Content-Disposition: form-data; name=\"%@\"; filename=\"photo.png\"\\r\n", fieldName).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(data)
        body.appendData(NSString(format: "\r\n--%@\r\n", boundary).dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        
        request.HTTPBody = body
        ///
        
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                if(data != nil ) {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                            /*
                            var jsonParseError: NSError? = nil
                            let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                            print(object)
                            */
                            completion(data: data, error: nil)
                        }else{
                            let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                            completion(data: data, error: statusError)
                        }
                    }
                }else if error != nil{
                    completion(data: nil, error: error)
                }
            }
        }
    }
    
    class func postDataAsyncWithCallback(url: NSString, method: String, data: AnyObject, json: Bool? = true, completion: (data: NSData?, error: NSError?) -> Void){
        //let nsurl:NSURL = NSURL(string: url)!
        let nsurl:NSURL = NSURL(string: (self.domain().root + (url as String)))!
        print(nsurl)
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = method
        
        if json == true{
            let postData: Dictionary = NSDictionary(dictionary: data as! [NSObject : AnyObject]) as Dictionary
            //request.HTTPBody = NSJSONSerialization.dataWithJSONObject(postData, options: nil, error: &err)
            do{
                try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions.init(rawValue: 0))
            }catch let err as NSError{
                print(err)
            }catch{
                print("Unknown error _postDataAsyncWithCallback")
            }
            //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
        }else{
            let postString: NSString = data as! NSString
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        }
        
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                if(data != nil ) {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                            /*
                            var jsonParseError: NSError? = nil
                            let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                            print(object)
                            */
                            completion(data: data, error: nil)
                        }else{
                            let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                            completion(data: data, error: statusError)
                        }
                    }
                }else if error != nil{
                    completion(data: nil, error: error)
                }
            }
        }
        
    }
    
    
    class func postDataWithImageAsyncWithCallback(url: NSString, method: String, data: NSDictionary?, imageObj: NSDictionary?, completion: (data: NSData?, error: NSError?) -> Void){
        //let nsurl:NSURL = NSURL(string: url)!
        let nsurl:NSURL = NSURL(string: (self.domain().root + (url as String)))!
        print(nsurl)
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: nsurl)
        request.HTTPMethod = method
        
        let boundary = DataManager.generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body: NSMutableData = NSMutableData()
        
        if data != nil{
            for (key, value) in data! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        if imageObj != nil{
            let filename: String = imageObj!.objectForKey("filename") as! String
            let fieldname: String = imageObj!.objectForKey("fieldname") as! String
            let mimetype: String = imageObj!.objectForKey("mimetype") as! String
            let imagedata: NSData = imageObj!.objectForKey("data") as! NSData
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(fieldname)\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.appendData(imagedata)
            body.appendString("\r\n")
        }
        
        body.appendString("--\(boundary)--\r\n")
        
        
        request.HTTPBody = body
        
        if self.domain().token != ""{
            request.setValue(self.domain().token, forHTTPHeaderField: "Authorization")
        }
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()){
                if(data != nil ) {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300){
                            /*
                            var jsonParseError: NSError? = nil
                            let object: AnyObject = NSString(data: urlData!, encoding: NSUTF8StringEncoding)!
                            print(object)
                            */
                            completion(data: data, error: nil)
                        }else{
                            let statusError = NSError(domain:url as String, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                            completion(data: data, error: statusError)
                        }
                    }
                }else if error != nil{
                    completion(data: nil, error: error)
                }
            }
        }
    }
    
    class func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().UUIDString)"
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}

extension NSString{
    class func stringFromObject(obj: AnyObject, encoding: UInt) -> NSString{
        var jsonString: NSString = ""
        
        do{
            let jsonData: NSData = try NSJSONSerialization.dataWithJSONObject(obj, options: NSJSONWritingOptions.PrettyPrinted)
            jsonString = NSString(data: jsonData, encoding: encoding)!
        }catch{
            print("NSJSONSerialization error: \((error as! NSError).localizedDescription)")
        }
        return jsonString
    }
}