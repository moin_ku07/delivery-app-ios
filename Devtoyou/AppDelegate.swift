//
//  AppDelegate.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/22/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import QuartzCore
import CoreData
import CoreLocation
import GoogleMaps
import Fabric
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var locationManager: CLLocationManager!
    var isBackgroundMode: Bool = false
    var deferringUpdates: Bool = false
    var _paymentMethods: [NSDictionary] = [NSDictionary]()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        /*for fontFamily in UIFont.familyNames(){
            print("\(fontFamily)")
            for fontname in UIFont.fontNamesForFamilyName(fontFamily ){
                print("   \(fontname)")
            }
        }*/
        
        //print(UIFont(name: "BahijTheSansArabicExtraBold", size: 16))
        
        //UILabel.appearance().font = UIFont.systemFontOfSize(14)
        //AppTheme.apply()
        
        //prefs.setObject(["ar-SA", "en"], forKey: "AppleLanguages")
        //prefs.setObject(["en", "ar-SA"], forKey: "AppleLanguages")
        prefs.setValue(nil, forKey: "token")
        prefs.synchronize()
        
        //print(NSTimeZone.knownTimeZoneNames())
        
        // Set statusbar to light color
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        // Style UINavigationBar
        UINavigationBar.appearance().barStyle = UIBarStyle.Black
        UINavigationBar.appearance().barTintColor = UIColor(rgba: "de3929")
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().translucent = false
        
        // Style UITabbar appearance
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor(rgba: "707173"), NSFontAttributeName: UIFont.systemFontOfSize(9)], forState: UIControlState.Normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor(rgba: "e03929"), NSFontAttributeName: UIFont.systemFontOfSize(9)], forState: UIControlState.Selected)
        UITabBar.appearance().tintColor = UIColor(rgba: "e03929")
        
        // GoogleMap integration
        //GMSServices.provideAPIKey("AIzaSyBNyIb1TUcRqs9dZ2yALFCW4Ezgy6V3zeE")
        GMSServices.provideAPIKey("AIzaSyAMe_3YVqoqYZZywukyyZZEkwK1k9_Vc6g")
        
        // Twitter integration
        Twitter.sharedInstance().startWithConsumerKey("m2c4UFO9Zk0cTI8CLicc3QPe0", consumerSecret: "gX5Tffg9cKTxyYQ2AzmgCeUrbZcmHeYzJeeJY2UbqJVOQc6r7e")
        Fabric.with([Twitter.sharedInstance()])
        
        //Fabric.with([Twitter.self])
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
    

        if let _: String = prefs.objectForKey("username") as? String, _: String = prefs.objectForKey("password") as? String{
            print("auto login")
            self.setLoginAsRootView()
        }else if let _ = Twitter.sharedInstance().sessionStore.session(){
            print("TW auto")
            self.setLoginAsRootView()
        }else if let gplus = prefs.objectForKey("isGplusUser") as? Bool where gplus == true{
            print("G+ auto")
            self.setLoginAsRootView()
        }else{
            if prefs.objectForKey("login_once") as? Bool != true{
                let storyboard: UIStoryboard = UIStoryboard(name: "IntroBoard", bundle: nil)
                let introvc: IntroViewController = storyboard.instantiateViewControllerWithIdentifier("IntroViewController") as! IntroViewController
                introvc.isRooTViewController = true
                self.window?.rootViewController = introvc
            }else{
                if let nvc: NavigationController = self.window?.rootViewController as? NavigationController{
                    let vc: FirstScreenViewController = nvc.storyboard?.instantiateViewControllerWithIdentifier("FirstScreenViewController") as! FirstScreenViewController
                    nvc.viewControllers = [vc]
                }
            }
        }
        
        let urlPath: String = "http://demos.durlov.com/devtoyou.php"
        let url: NSURL = NSURL(string: urlPath)!
        let request1: NSURLRequest = NSURLRequest(URL: url)
        let queue:NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request1, queue: queue, completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            
            do {
                let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! NSDictionary
                print(jsonResult)
                if jsonResult.objectForKey("enabled") as? Bool == false{
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        let vc: UIViewController = UIViewController()
                        vc.view.backgroundColor = UIColor.redColor()
                        
                        let label: UILabel = UILabel()
                        label.textColor = UIColor.whiteColor()
                        label.textAlignment = .Center
                        label.font = UIFont.boldSystemFontOfSize(20)
                        label.numberOfLines = 0
                        
                        label.text = jsonResult.objectForKey("message") as? String
                        
                        label.translatesAutoresizingMaskIntoConstraints = false
                        vc.view.addSubview(label)
                        
                        vc.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[label]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label" : label]))
                        vc.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[label]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label" : label]))
                        
                        
                        UIApplication.sharedApplication().keyWindow!.rootViewController = vc
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
            
        })
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        isBackgroundMode = true
        
        if locationManager != nil{
            if prefs.objectForKey("user_type") as? String == "driver"{
                locationManager.stopUpdatingLocation()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.distanceFilter = kCLDistanceFilterNone
                locationManager.pausesLocationUpdatesAutomatically = false
                locationManager.activityType = CLActivityType.AutomotiveNavigation
                
                locationManager.startUpdatingLocation()
            }else{
                locationManager.stopUpdatingLocation()
            }
        }
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        isBackgroundMode = false
        if prefs.objectForKey("user_type") as? String == "driver" && prefs.objectForKey("locationPermittedOnce") as? Bool == true{
            self.initLocationManager()
        }else{
            if locationManager != nil{
                locationManager.stopUpdatingLocation()
                locationManager.delegate = nil
            }
        }
        
        if let nvc: UINavigationController = window?.rootViewController as? UINavigationController where application.applicationIconBadgeNumber > 0{
            if let tvc: TabBarController = nvc.viewControllers.first as? TabBarController{
                for (index, vc) in tvc.viewControllers!.enumerate(){
                    if let notificationVC: NotificationsTableViewController = vc as? NotificationsTableViewController{
                        notificationVC.tabBarItem.badgeValue = "\(application.applicationIconBadgeNumber)"
                    }
                }
            }
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Notifications Stack
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Did Fail to Register for Remote Notifications")
        print("\(error), \(error.localizedDescription)")
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        //print("Did Register for Remote Notifications with Device Token \(deviceToken)")
        
        var nDeviceToken: NSString = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
        nDeviceToken = nDeviceToken.stringByReplacingOccurrencesOfString(" ", withString: "")
        prefs.setValue(nDeviceToken, forKey: "device_id")
        prefs.synchronize()
        
        let postData: NSDictionary = ["registration_id": nDeviceToken, "device_id": NSUUID().UUIDString, "active": true]
        print(postData)
        DataManager.postDataAsyncWithCallback("api/device/apns/", data: postData) { (data, error) -> Void in
            if data != nil{
                self.prefs.setValue(true, forKey: "notification")
                self.prefs.synchronize()
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
            }else{
                print(error?.localizedDescription)
            }
        }
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("application receives push notification.")
        
        if application.applicationState == UIApplicationState.Inactive || application.applicationState == UIApplicationState.Background{
            if let nvc: UINavigationController = window?.rootViewController as? UINavigationController{
                if let tvc: TabBarController = nvc.viewControllers.first as? TabBarController{
                    //print("tvc found")
                    for (index, vc) in tvc.viewControllers!.enumerate(){
                        if let notificationVC: NotificationsTableViewController = vc as? NotificationsTableViewController{
                            //print("Notification vc found")
                            tvc.selectedIndex = index
                            notificationVC.loadTableData()
                        }
                    }
                }
            }
        }else{
            var badgeNumber: NSNumber = 0
            
            if let aps = userInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    if let message = alert["message"] as? String {
                        AlertManager.showAlert(window?.rootViewController, title: nil, message: message, buttonNames: nil, completion: nil)
                    }
                }else if let message = aps["alert"] as? String {
                    AlertManager.showAlert(window?.rootViewController, title: nil, message: message, buttonNames: nil, completion: nil)
                }
                if let badgenumber: NSNumber = aps["badge"] as? NSNumber{
                    badgeNumber = badgenumber
                    print(badgeNumber)
                }
            }
            if let nvc: UINavigationController = window?.rootViewController as? UINavigationController{
                if let tvc: TabBarController = nvc.viewControllers.first as? TabBarController{
                    if let notificationVC: NotificationsTableViewController = tvc.selectedViewController as? NotificationsTableViewController{
                        notificationVC.loadTableData()
                    }else{
                        for (_, vc) in tvc.viewControllers!.enumerate(){
                            if let notificationVC: NotificationsTableViewController = vc as? NotificationsTableViewController where badgeNumber.integerValue > 0{
                                notificationVC.tabBarItem.badgeValue = "\(badgeNumber.integerValue)"
                                application.applicationIconBadgeNumber = badgeNumber.integerValue
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Unregister
    func unregisterDevice(completion: ((data: NSData?, error: NSError?) -> Void)?){
        if let device_id: String = prefs.objectForKey("device_id") as? String{
            DataManager.postDataAsyncWithCallback("api/device/apns/\(device_id)/", method: "DELETE", data: [:], completion: { (data, error) -> Void in
                if data != nil{
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    // unregister push notification first
                    UIApplication.sharedApplication().unregisterForRemoteNotifications()
                    completion?(data: data, error: nil)
                }else{
                    completion?(data: nil, error: error)
                    print(error?.localizedDescription)
                }
            })
        }else{
            completion?(data: nil, error: nil)
        }
    }
    
    func setLoginAsRootView(){
        if let nvc: NavigationController = self.window?.rootViewController as? NavigationController{
            let vc: LoginViewController = nvc.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
            nvc.viewControllers = [vc]
        }
    }
    
    // MARK: - LocationManager stack
    
    func initLocationManager(){
        
        if locationManager != nil{
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        // accuracy 100 meters
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        // distance measurement (in meters) from an existing location
        locationManager.distanceFilter = 100
        
        //track location changes to the automobile
        locationManager.activityType = CLActivityType.AutomotiveNavigation
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined{
            locationManager.requestAlwaysAuthorization()
        }else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied{
            AlertManager.showAlert(nil, title: NSLocalizedString("AM_Alert", comment: "Alert"), message: NSLocalizedString("Label_Allow_Location_Always", comment: "Allow location access"), buttonNames: [NSLocalizedString("Label_Cancel", comment: "Cancel"), NSLocalizedString("Label_Settings", comment: "Settings")], completion: { (index) -> Void in
                if index == 1 && UIApplicationOpenSettingsURLString != ""{
                    UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
                }
            })
        }
        
        locationManager.startUpdatingLocation()
    }
    
    func requestInUseAuth(){
        if locationManager == nil{
            locationManager = CLLocationManager()
        }
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse{
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation: CLLocation = locations.last{
            print(newLocation.coordinate)
            
            if isBackgroundMode && !deferringUpdates{
                deferringUpdates = true
                locationManager.allowDeferredLocationUpdatesUntilTraveled(100, timeout: 60)
            }
            let postData: NSDictionary = ["latitude": newLocation.coordinate.latitude, "longitude": newLocation.coordinate.longitude, "driver": prefs.objectForKey("id") as! NSNumber]
            print(postData)
            DataManager.postDataAsyncWithCallback("api/driver-gps/", data: postData, completion: { (data, error) -> Void in
                if data != nil{
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                }else{
                    print(error?.localizedDescription)
                }
            })
        }
    }
    
    func locationManager(manager: CLLocationManager, didFinishDeferredUpdatesWithError error: NSError?) {
        deferringUpdates = false
        print("here")
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.durlov.Devtoyou" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Devtoyou", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Devtoyou.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true]
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: mOptions)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

// MARK: - CALayer borderColorFromUIColor
extension CALayer{
    func setBorderColorFromUIColor(color: UIColor){
        self.borderColor = color.CGColor
    }
}