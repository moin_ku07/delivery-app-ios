//
//  FirstScreenViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/22/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import TwitterKit

class FirstScreenViewController: UIViewController {
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let _: String = self.prefs.objectForKey("username") as? String, _: String = self.prefs.objectForKey("password") as? String{
            print("auto login")
            loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }else if let _ = Twitter.sharedInstance().sessionStore.session(){
            print("TW auto")
            loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }else if let gplus = prefs.objectForKey("isGplusUser") as? Bool where gplus == true{
            print("G+ auto")
            loginButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        }
        
        loginButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        loginButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
        
        registerButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        registerButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.doLocalisation()
        }
    }
    
    func doLocalisation(){
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            if UIDevice.currentDevice().systemVersion.hasPrefix("8"){
                loginButton.titleEdgeInsets = UIEdgeInsetsMake(0, -loginButton.imageView!.frame.size.width, 0, loginButton.imageView!.frame.size.width)
                loginButton.imageEdgeInsets = UIEdgeInsetsMake(0, (loginButton.titleLabel!.frame.size.width + 10), 0, -loginButton.titleLabel!.frame.size.width)
                
                registerButton.titleEdgeInsets = UIEdgeInsetsMake(0, -registerButton.imageView!.frame.size.width, 0, registerButton.imageView!.frame.size.width)
                registerButton.imageEdgeInsets = UIEdgeInsetsMake(0, (registerButton.titleLabel!.frame.size.width + 10), 0, -registerButton.titleLabel!.frame.size.width)
            }else{
                loginButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
                registerButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "skipShowTabBar"{
            ProfileTableViewController.doLogout()
        }
    }

}
