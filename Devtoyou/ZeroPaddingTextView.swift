//
//  ZeroPaddingTextView.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/3/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class ZeroPaddingTextView: UITextView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textContainerInset = UIEdgeInsetsZero
        self.textContainer.lineFragmentPadding = 0
    }

}

class PaddedButton: UIButton{
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4)
    }
}
