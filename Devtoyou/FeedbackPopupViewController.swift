//
//  FeedbackPopupViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/15/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import RateView

protocol FeedbackPopupViewControllerDelegate{
    func onFeedbackSubmitSuccess()
}

class FeedbackPopupViewController: UIViewController {
    
    var delegate: FeedbackPopupViewControllerDelegate?

    @IBOutlet var contentView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var rateView: RateView!
    @IBOutlet var feedbackText: SZTextView!
    @IBOutlet var maxHeightConstraint: NSLayoutConstraint!
    
    var titleString: String?
    var ratingValue: Float?
    var feedbackString: String?
    
    
    var apiURL: String?
    var orderID: NSNumber?
    var itemID: NSNumber?
    var storeID: NSNumber?
    var driverID: NSNumber?
    var customerID: NSNumber?
    
    var shouldSubmit: Bool = false
    var activityView: UICustomActivityView!
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rateView.starSize = 15
        rateView.starFillColor = UIColor(rgba: "de3929")
        rateView.starBorderColor = UIColor.grayColor()
        rateView.starNormalColor = UIColor.whiteColor()
        rateView.canRate = true
        
        feedbackText.placeholder = NSLocalizedString("Label_Leave_Feedback", comment: "Leave feedback")
        
        if titleString != nil{
            self.titleLabel.text = titleString
        }
        if ratingValue != nil{
            rateView.rating = ratingValue!
        }
        if feedbackString != nil{
            feedbackText.text = feedbackString
        }
        
        if shouldSubmit{
            self.view.addConstraint(maxHeightConstraint)
            maxHeightConstraint.active = true
        }else{
            maxHeightConstraint.active = false
            self.view.removeConstraint(maxHeightConstraint)
            feedbackText.editable = false
            rateView.canRate = false
        }
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onViewTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
        let closeButton: UIButton = UIButton(frame: CGRectMake(0, 0, 22, 22))
        //closeButton.backgroundColor = UIColor.yellowColor()
        closeButton.setBackgroundImage(UIImage(named: "icon-close"), forState: UIControlState.Normal)
        closeButton.addTarget(self, action: #selector(onCloseTap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(closeButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 22))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 22))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 25))
        self.view.addConstraint(NSLayoutConstraint(item: closeButton, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: -12))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onSubmitTap(sender: UIButton) {
        if apiURL != nil{
            let postData: NSMutableDictionary = ["rating": round(rateView.rating * 100) / 100, "text": feedbackText.text, "user": prefs.objectForKey("id") as! NSNumber]
            
            if orderID != nil{
                postData.setValue(orderID!, forKey: "order")
            }
            if itemID != nil{
                postData.setValue(itemID!, forKey: "item")
            }
            if driverID != nil{
                postData.setValue(driverID!, forKey: "driver")
            }
            if storeID != nil{
                postData.setValue(storeID!, forKey: "store")
            }
            if customerID != nil{
                postData.setValue(customerID!, forKey: "customer")
            }
            print(postData)
            //return
            activityView.showActivityIndicator()
            DataManager.postDataAsyncWithCallback(apiURL!, data: postData, completion: { (data, error) -> Void in
                self.activityView.hideActivityIndicator()
                if data != nil{
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    do{
                        let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSDictionary
                        if json.objectForKey("status") as? Bool == false{
                            AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                        }else{
                            self.dismissViewControllerAnimated(true, completion: { Void in
                                self.delegate?.onFeedbackSubmitSuccess()
                            })
                            print("to do to show rating on detail view")
                        }
                    }catch{
                        print(error)
                        AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                    }
                }else{
                    print(error?.localizedDescription)
                    AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Label_Order_Error_Cart", comment: "try again"), buttonNames: nil, completion: nil)
                }
            })
        }else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func onViewTap(recognizer: UITapGestureRecognizer){
        feedbackText.resignFirstResponder()
    }
    
    // MARK: - onCloseTap
    func onCloseTap(sender: UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
}
