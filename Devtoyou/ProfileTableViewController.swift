//
//  ProfileTableViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 8/29/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit
import TwitterKit
import CoreData
import SDWebImage
import ActionSheetPicker_3_0

class ProfileTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var tableData: NSMutableArray = NSMutableArray()
    
    @IBOutlet var userPic: UIImageView!
    @IBOutlet var addUserPic: UIButton!
    @IBOutlet var fullnameLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var borderView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var creditCashLabel: UILabel!
    @IBOutlet var loyaltyPointLabel: UILabel!
    
    var user_id: String?
    var user_type: String?
    //var userImageData: NSData?
    var activityView: UICustomActivityView!
    var isProfileUpdating: Bool = false
    
    let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    let regex: NSPredicate = NSPredicate(format:"SELF MATCHES %@", "^(facebook|twitter|google\\+)_[a-zA-Z0-9]+$")
    
    var timer: NSTimer!
    
    let numberFormatter: NSNumberFormatter = NSNumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
        
        numberFormatter.locale = NSLocale(localeIdentifier: "ar-SA")
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        
        activityView = UICustomActivityView(activityColor: UIColor.greenColor(), activityBackground: UIColor.whiteColor(), containerColor: UIColor.blackColor().colorWithAlphaComponent(0.3), textColor: UIColor.blackColor())
        activityView.initActivityIndicator(self.navigationController!.view, style: UIActivityIndicatorViewStyle.WhiteLarge, shouldHaveContainer: true, centerPoint: nil)
        
        phoneLabel.text = ""
        emailLabel.text = ""
        
        
        let addUserPicTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onAddUserPicTap(_:)))
        addUserPicTap.numberOfTapsRequired = 1
        addUserPic.addGestureRecognizer(addUserPicTap)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.user_type = prefs.objectForKey("user_type") as? String
        
        if let id: NSNumber = prefs.objectForKey("id") as? NSNumber{
            user_id = id.stringValue
        }
        if let firstname: String = prefs.objectForKey("first_name") as? String where firstname != "N/A" && user_type != "store_owner"{
            fullnameLabel.text = firstname
            if let lastname: String = prefs.objectForKey("last_name") as? String where lastname != "N/A" && user_type != "store_owner"{
                fullnameLabel.text = firstname + " " + lastname
            }
        }else{
            fullnameLabel.text = ""
        }
        if let username: String = prefs.objectForKey("username") as? String where username != "N/A" && !self.regex.evaluateWithObject(username) && user_type != "store_owner"{
            self.phoneLabel.text = username
        }
        
        for constraint in borderView.constraints{
            if constraint.identifier == "borderHeight"{
                constraint.constant = 0.5
            }
        }
        
        if user_type == "customer"{
            tableData = [
                ["title": NSLocalizedString("Label_User_Loyalty", comment: "User Dashboard"), "type": "loyalty"],
                ["title": NSLocalizedString("Label_User_Dashboard", comment: "User Dashboard"), "type": "dashboard"],
                ["title": NSLocalizedString("Label_User_History", comment: "User History"), "type": "history"],
                ["title": NSLocalizedString("Label_View_Address", comment: "View Address"), "type": "address"],
                ["title": NSLocalizedString("Label_Account_Settings", comment: "Account Settings"), "type": "settings"]
            ]
        }else if user_type == "store_owner"{
            tableData = [
                ["title": NSLocalizedString("Label_User_Dashboard", comment: "User Dashboard"), "type": "dashboard"],
                ["title": NSLocalizedString("Label_User_All_Menu_Items", comment: "All Menu Items"), "type": "allitem"],
                ["title": NSLocalizedString("Label_User_Store_History", comment: "Store History"), "type": "history"],
                ["title": NSLocalizedString("Label_View_Address", comment: "View Address"), "type": "address"],
                ["title": NSLocalizedString("Label_Account_Settings", comment: "Account Settings"), "type": "settings"]
            ]
        }else if user_type == "driver"{
            tableData = [
                ["title": NSLocalizedString("Label_User_Dashboard", comment: "User Dashboard"), "type": "dashboard"],
                ["title": NSLocalizedString("Label_User_Driver_History", comment: "Driver History"), "type": "history"],
                ["title": NSLocalizedString("Label_View_Address", comment: "View Address"), "type": "address"],
                ["title": NSLocalizedString("Label_Account_Settings", comment: "Account Settings"), "type": "settings"]
            ]
        }
        if !isProfileUpdating{
            if user_type == "store_owner"{
                tableView.tableHeaderView!.frame.size.height = 262
                let newHeaderView: UIView = tableView.tableHeaderView!
                tableView.tableHeaderView = newHeaderView
                
                DataManager.loadDataAsyncWithCallback("api/stores/" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "/?include_credit=true", completion: { (data, error) -> Void in
                    if data != nil{
                        do{
                            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                            print(json)
                            if let email: String = json.objectForKey("name") as? String where email != "N/A"{
                                self.emailLabel.text = email
                            }
                            if let username: String = json.objectForKey("username") as? String where username != "N/A" && !self.regex.evaluateWithObject(username){
                                self.phoneLabel.text = username
                            }
                            if let photourl: String = json.objectForKey("thumb") as? String{
                                self.userPic.sd_setImageWithURL(NSURL(string: photourl)!)
                            }
                            if let store_balance: NSNumber = json.objectForKey("store_balance") as? NSNumber{
                                self.creditCashLabel.text = self.numberFormatter.stringFromNumber(store_balance)!// + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                            }
                        }catch{
                            print("Unknow error")
                        }
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }else{
                if user_type == "driver"{
                    tableView.tableHeaderView!.frame.size.height = 262
                    let newHeaderView: UIView = tableView.tableHeaderView!
                    tableView.tableHeaderView = newHeaderView
                }
                DataManager.loadDataAsyncWithCallback("auth/profile", completion: { (data, error) -> Void in
                    if data != nil{
                        do{
                            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                            //print(json)
                            if let id: NSNumber = json.objectForKey("id") as? NSNumber{
                                self.user_id = id.stringValue
                                self.prefs.setValue(id, forKey: "id")
                                self.prefs.synchronize()
                            }
                            if let email: String = json.objectForKey("email") as? String where email != "N/A"{
                                self.emailLabel.text = email
                            }
                            if let username: String = json.objectForKey("username") as? String where username != "N/A" && !self.regex.evaluateWithObject(username){
                                self.phoneLabel.text = username
                            }
                            if let photourl: String = json.objectForKey("photo") as? String{
                                self.userPic.sd_setImageWithURL(NSURL(string: photourl)!)
                            }
                            if let loyalty_amount: NSNumber = json.objectForKey("loyalty_amount") as? NSNumber{
                                self.loyaltyPointLabel.text = self.numberFormatter.stringFromNumber(loyalty_amount)!
                            }
                            if let balance: NSNumber = json.objectForKey("balance") as? NSNumber{
                                self.creditCashLabel.text = self.numberFormatter.stringFromNumber(balance)!// + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                            }
                        }catch{
                            print("Unknow error")
                        }
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }
        }
        
        timer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
        
        dispatch_async(dispatch_get_main_queue()){ () -> Void in
            self.doLocalisation()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer.invalidate()
    }
    
    func doLocalisation(){
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            let scalingTransform : CGAffineTransform = CGAffineTransformMakeScale(-1, 1)
            tableView.transform = scalingTransform
            headerView.transform = scalingTransform
            footerView.transform = scalingTransform
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
        
        cell.backgroundColor = UIColor(rgba: "EFECD9")
        cell.contentView.backgroundColor = UIColor(rgba: "EFECD9")
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        
        cell.textLabel?.text = cellData.objectForKey("title") as? String
        cell.textLabel?.font = UIFont.systemFontOfSize(15)
        cell.textLabel?.textColor = UIColor(rgba: "555454")
        
        if (UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar")) && UIDevice.currentDevice().systemVersion.hasPrefix("8"){
            let scalingTransform : CGAffineTransform = CGAffineTransformMakeScale(-1, 1)
            cell.textLabel?.transform = scalingTransform
            cell.textLabel?.textAlignment = .Right
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellData: NSDictionary = tableData[indexPath.row] as! NSDictionary
        if let title: String = cellData.objectForKey("title") as? String, type = cellData.objectForKey("type") as? String{
            self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            if type == "dashboard"{
                let vc: DashboardViewController = self.storyboard!.instantiateViewControllerWithIdentifier("DashboardViewController") as! DashboardViewController
                vc.navigationItem.title = title
                vc.navigationTitle = title
                self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
            }else if type == "history"{
                let vc: UserHistoryTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("UserHistoryTableViewController") as! UserHistoryTableViewController
                vc.navigationItem.title = title
                vc.navigationTitle = title
                self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
            }else if type == "address"{
                let vc: AddressTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("AddressTableViewController") as! AddressTableViewController
                vc.navigationItem.title = title
                vc.navigationTitle = title
                self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
            }else if type == "settings"{
                let vc: AccountSettingsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("AccountSettingsViewController") as! AccountSettingsViewController
                vc.navigationItem.title = title
                vc.navigationTitle = title
                self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
            }else if type == "allitem"{
                let vc: StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsTableViewController") as! StoreItemsTableViewController
                self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                vc.navigationItem.title = title
                vc.navigationTitle = title
                vc.store_id = prefs.objectForKey("store_id") as! NSNumber
                vc.enableAddButton = true
                self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
            }else if type == "loyalty"{
                let vc: WebViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
                self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                vc.navigationItem.title = title
                //vc.fileName = "loyalty.html"
                vc.link = NSURL(string: DataManager.domain().root + "apphtml/loyalty.html")
                self.tabBarController?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - addUserPicTap
    func onAddUserPicTap(recognizer: UITapGestureRecognizer){
        let imagePicker: UIImagePickerController = UIImagePickerController()
        ActionSheetStringPicker.showPickerWithTitle(NSLocalizedString("Label_Select_Photo_Option", comment: "Choose Photo"), rows: [NSLocalizedString("Label_Select_Photo_Library", comment: "Photo Gallery"), NSLocalizedString("Label_Select_Photo_Camera", comment: "Camera")], initialSelection: 0, doneBlock: { (picker: ActionSheetStringPicker!, selectedIndex: Int, object: AnyObject!) -> Void in
            dispatch_async(dispatch_get_main_queue(), { 
                if selectedIndex == 0{
                    imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                }else{
                    imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                }
                
                //imagePicker.allowsEditing = true
                imagePicker.delegate = self
                self.presentViewController(imagePicker, animated: true, completion: nil)
            })
            }, cancelBlock: { (picker: ActionSheetStringPicker!) -> Void in
                print("nothing")
            }, origin: recognizer.view)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let imageInfo = info as NSDictionary
        
        var image: UIImage = imageInfo.objectForKey(UIImagePickerControllerOriginalImage) as! UIImage
        
        let imageSize: CGSize = image.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        if (width != height) {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2;
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0)
            image.drawAtPoint(CGPointMake(-widthOffset, -heightOffset), blendMode: CGBlendMode.Copy, alpha: 1)
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
        }
        
        //scale down image
        let scaledImage = self.scaleImageWith(image, newSize: CGSizeMake(100, 100 / (image.size.width / image.size.height)))
        
        let imageData = UIImagePNGRepresentation(scaledImage)
        //userImageData = imageData
        
        self.userPic.image = UIImage(data: imageData!)
        
        activityView.showActivityIndicator()
        var imageObj: NSMutableDictionary = NSMutableDictionary()
        
        var url: String = "api/"
        if user_type == "customer"{
            url += "customers/" + user_id! + "/"
            imageObj = ["filename": "photo.png", "fieldname": "photo", "mimetype": "image/png", "data": imageData!]
        }else if user_type == "store_owner"{
            url = "api/stores/" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "/"
            imageObj = ["filename": "photo.png", "fieldname": "thumb", "mimetype": "image/png", "data": imageData!]
        }else if user_type == "driver"{
            url += "drivers/" + user_id! + "/"
            imageObj = ["filename": "photo.png", "fieldname": "photo", "mimetype": "image/png", "data": imageData!]
        }
        
        isProfileUpdating = true
        DataManager.postDataWithImageAsyncWithCallback(url, method: "PATCH", data: nil, imageObj: imageObj) { (data, error) -> Void in
            self.activityView.hideActivityIndicator()
            self.isProfileUpdating = false
            if data != nil{
                //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                print("success. Nothing to do atm")
            }else{
                print(error?.localizedDescription)
                AlertManager.showAlert(self, title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Try_Again", comment: "Try again"), buttonNames: nil, completion: nil)
            }
        }
    }
    
    func scaleImageWith( image: UIImage, newSize: CGSize) -> UIImage{
        //println(newSize)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    @IBAction func onLogoutTap(sender: UIButton) {
        ProfileTableViewController.doLogout()
        
        if let nvc: NavigationController = (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? NavigationController{
            nvc.navigationBarHidden = true
            let vc: FirstScreenViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FirstScreenViewController") as! FirstScreenViewController
            nvc.viewControllers = [vc]
        }
    }
    
    // MARK: - doLogout
    class func doLogout(){
        let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).unregisterDevice(nil)
        
        if let social: String = prefs.objectForKey("social") as? String{
            if social == "tw"{
                let store = Twitter.sharedInstance().sessionStore
                
                if let userID = store.session()?.userID {
                    store.logOutUserID(userID)
                }
            }else if social == "google+"{
                GIDSignIn.sharedInstance().signOut()
            }
        }
        DataManager.postDataAsyncWithCallback("/auth/logout/", data: [:], completion: { (data, error) -> Void in
            print(error)
            print(data)
            print("User has logout")
        })
        
        for key in NSUserDefaults.standardUserDefaults().dictionaryRepresentation().keys {
            print(key)
            if key == "login_once"{
                continue
            }
            NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
        }
        /*prefs.setValue(nil, forKey: "username")
        prefs.setValue(nil, forKey: "password")
        prefs.setValue(nil, forKey: "auth_token")
        prefs.setValue(nil, forKey: "first_name")
        prefs.setValue(nil, forKey: "last_name")
        prefs.setValue(nil, forKey: "social")
        prefs.setValue(nil, forKey: "token")
        prefs.setValue(nil, forKey: "id")
        prefs.setValue(nil, forKey: "user_type")
        prefs.setValue(nil, forKey: "isGplusUser")
        prefs.setValue(nil, forKey: "store_id")
        prefs.setValue(nil, forKey: "store_name")*/
        prefs.synchronize()
        
        let cookieJar: NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookie in cookieJar.cookies!{
            cookieJar.deleteCookie(cookie)
        }
        
        // Clear cart
        let moc: NSManagedObjectContext = CoreDataHelper.managedObjectContext()
        let results: NSArray = CoreDataHelper.fetchEntities(NSStringFromClass(Carts), withPredicate: nil, andSorter: nil, managedObjectContext: moc)
        for result in results{
            let cart: Carts = result as! Carts
            print(cart)
            moc.deleteObject(cart)
        }
        do{
            try moc.save()
            print("All record deleted")
        }catch{
            print(error)
        }
    }

    // MARK: - timerTick
    func timerTick(){
        //print("timerTick: \(timer)")
        if !isProfileUpdating{
            if user_type == "store_owner"{
                DataManager.loadDataAsyncWithCallback("api/stores/" + (prefs.objectForKey("store_id") as! NSNumber).stringValue + "/?include_credit=true", completion: { (data, error) -> Void in
                    if data != nil{
                        do{
                            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                            if let store_balance: NSNumber = json.objectForKey("store_balance") as? NSNumber{
                                self.creditCashLabel.text = self.numberFormatter.stringFromNumber(store_balance)!// + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                            }
                        }catch{
                            print("Unknow error")
                        }
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }else{
                DataManager.loadDataAsyncWithCallback("auth/profile", completion: { (data, error) -> Void in
                    if data != nil{
                        do{
                            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                            if let loyalty_amount: NSNumber = json.objectForKey("loyalty_amount") as? NSNumber{
                                self.loyaltyPointLabel.text = self.numberFormatter.stringFromNumber(loyalty_amount)!
                            }
                            if let balance: NSNumber = json.objectForKey("balance") as? NSNumber{
                                self.creditCashLabel.text = self.numberFormatter.stringFromNumber(balance)!// + NSLocalizedString("Label_Currency_Code", comment: "Currency Code")
                            }
                        }catch{
                            print("Unknow error")
                        }
                    }else{
                        print(error?.localizedDescription)
                    }
                })
            }
        }
    }
    
    // MARK: - refresh
    func refresh(refreshControl: UIRefreshControl) {
        timerTick()
        refreshControl.endRefreshing()
    }
}
