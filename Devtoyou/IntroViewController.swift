//
//  IntroViewController.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/31/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, IntroVC3Delegate  {
    
    var pageViewController: UIPageViewController!
    var selectedMenuIndex: Int?
    var totalPage: Int = 3
    
    var isRooTViewController: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("IntroPageViewController") as! UIPageViewController
        pageViewController.view.backgroundColor = self.view.backgroundColor
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        let vc: IntroVC1 = self.storyboard?.instantiateViewControllerWithIdentifier("IntroVC1") as! IntroVC1
        vc.pageIndex = 0
        self.pageViewController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        
        self.addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        pageViewController.didMoveToParentViewController(self)
        
        //var pageViewRect: CGRect = self.view.bounds
        //pageViewRect = CGRectInset(pageViewRect, 10, 10)
        //pageViewController.view.frame = pageViewRect
        
        self.view.gestureRecognizers = pageViewController.gestureRecognizers
        
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor(rgba: "DE3929")
        appearance.backgroundColor = UIColor(rgba: "EFECD9")
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let vc: IntroVC1 = viewController as? IntroVC1{
            if vc.pageIndex > 0{
                return loadPageContentViewController(vc.pageIndex - 1)
            }
        }else if let vc: IntroVC2 = viewController as? IntroVC2{
            if vc.pageIndex > 0{
                return loadPageContentViewController(vc.pageIndex - 1)
            }
        }else if let vc: IntroVC3 = viewController as? IntroVC3{
            if vc.pageIndex > 0{
                return loadPageContentViewController(vc.pageIndex - 1)
            }
        }
        print("prev nil")
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let vc: IntroVC1 = viewController as? IntroVC1{
            if vc.pageIndex + 1 < totalPage{
                return loadPageContentViewController(vc.pageIndex + 1)
            }
        }else if let vc: IntroVC2 = viewController as? IntroVC2{
            if vc.pageIndex + 1 < totalPage{
                return loadPageContentViewController(vc.pageIndex + 1)
            }
        }else if let vc: IntroVC3 = viewController as? IntroVC3{
            if vc.pageIndex + 1 < totalPage{
                return loadPageContentViewController(vc.pageIndex + 1)
            }
        }
        print("after nil")
        return nil
    }
    
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return totalPage
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        if UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft || NSLocale.preferredLanguages()[0].hasPrefix("ar"){
            if UIDevice.currentDevice().systemVersion.hasPrefix("8"){
                return 0
            }
            return totalPage - 1
        }
        return 0
    }
    
    // MARK: - Load PageContentController
    func loadPageContentViewController(index: Int) -> UIViewController?{
        if index < totalPage{
            if index == 0{
                let vc: IntroVC1 = self.storyboard?.instantiateViewControllerWithIdentifier("IntroVC1") as! IntroVC1
                vc.pageIndex = index
                return vc
            }else if index == 1{
                let vc: IntroVC2 = self.storyboard?.instantiateViewControllerWithIdentifier("IntroVC2") as! IntroVC2
                vc.pageIndex = index
                return vc
            }else if index == 2{
                let vc: IntroVC3 = self.storyboard?.instantiateViewControllerWithIdentifier("IntroVC3") as! IntroVC3
                vc.pageIndex = index
                vc.delegate = self
                return vc
            }
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onGotItTap() {
        if isRooTViewController{
            (UIApplication.sharedApplication().delegate as! AppDelegate).window!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("NavigationController") as! NavigationController
            print("here")
        }else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
