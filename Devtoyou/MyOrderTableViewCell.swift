//
//  MyOrderTableViewCell.swift
//  Devtoyou
//
//  Created by Moin Uddin on 10/6/15.
//  Copyright © 2015 Moin Uddin. All rights reserved.
//

import UIKit

protocol MyOrderTableViewCellDelegate{
    func onTrackTap(cell: MyOrderTableViewCell)
    func onAcceptOrderTap(cell: MyOrderTableViewCell)
    func onCancelOrderTap(cell: MyOrderTableViewCell)
}

class MyOrderTableViewCell: UITableViewCell {
    
    var delegate: MyOrderTableViewCellDelegate?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var acceptOrder: UIButton!
    @IBOutlet var trackOrderButton: UIButton!
    @IBOutlet var cancelOrderButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onTrackButtonTap(sender: UIButton) {
        delegate?.onTrackTap(self)
    }

    @IBAction func onAcceptOrderTap(sender: UIButton) {
        delegate?.onAcceptOrderTap(self)
    }
    @IBAction func onCancelOrderTap(sender: UIButton) {
        delegate?.onCancelOrderTap(self)
    }
}
