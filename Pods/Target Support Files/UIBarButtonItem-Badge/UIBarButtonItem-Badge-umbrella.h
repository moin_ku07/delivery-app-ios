#import <UIKit/UIKit.h>

#import "UIBarButtonItem+Badge.h"
#import "UIButton+Badge.h"

FOUNDATION_EXPORT double UIBarButtonItem_BadgeVersionNumber;
FOUNDATION_EXPORT const unsigned char UIBarButtonItem_BadgeVersionString[];

